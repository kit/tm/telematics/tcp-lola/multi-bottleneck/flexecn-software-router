#include "qelems.h"
#include "../rte_include.h"
#include "qlog.h"
#include "macros/common.h"
#include "qdt/lolliq.h"
#include "traces/taildrop_event.h"
#include "makeconf.h"

#define WITH_ETH_OVERHEAD

#ifdef WITH_ETH_OVERHEAD
#define ETHERNET_OVERHEAD (4 + 4 + 8 + 12) // vlan tag + crc + (preamble+sof) + (ifs)
#else
#define ETHERNET_OVERHEAD 0
#endif


static qelem_ops_t qelem_taildrop_ops;

typedef struct __qelem_taildrop {
    qelem_t qelem;

    unsigned byte_limit;

#ifdef WITH_TRACES
    event_log_queue_t* trace_queue;
#endif

    lolliqueue_t queue;
} qelem_taildrop_t __rte_cache_aligned;


//------------------------------------------------------------------------------------------------------

static
qelem_t* qelem_taildrop_alloc ( const  qelem_params_t* common_params, const void* qelem_params )
{
    LOG_INIT ( "allocating qelem_taildrop for port %d \n", common_params->port_id );

    QPARAMSCAST ( taildrop );
    qelem_taildrop_t* qtail = 0;

    // determine queue length

    if ( params->byte_limit > 0 && params->pkt_limit > 0 ) {
        rte_panic ( "qelem_taildrop: please set either pkt_limit or byte_limit, not both\n" );
    }

    unsigned qlen = 0;
    if ( params->pkt_limit > 0 ) {
        qlen = params->pkt_limit;
    } else {
        qlen = params->byte_limit / ( 64 + ETHERNET_OVERHEAD ) ; // in min-sized packets
        qlen = 1 << ( 32 - __builtin_clz ( qlen - 1 ) ); // rounded to the next power of two
    }

    LOG_PARAM ( qlen ,"%u" );

    size_t queue_mem_size = lolliq_entries_sizeof ( qlen );
    QZMALLOC ( qtail, qelem_taildrop_t, queue_mem_size );
    qelem_init ( ( qelem_t* ) qtail, common_params, &qelem_taildrop_ops );
    lolliq_init ( &qtail->queue, qlen );

    if ( params->byte_limit > 0 ) {
        qtail->byte_limit = params->byte_limit;
        LOG_PARAM ( qtail->byte_limit, "%u" );
    }

#ifdef WITH_TRACES
    qtail->trace_queue = common_params->trace_queue;
#endif

    return ( qelem_t* ) qtail;
};

//------------------------------------------------------------------------------------------------------
// optional opts
//------------------------------------------------------------------------------------------------------

static inline unsigned qelem_taildrop_enqueue_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    QCAST ( taildrop );

    // check byte limit

    unsigned num_to_enqueue = num_pkts;

    if ( taildrop->byte_limit > 0 ) {
        unsigned qlen_bytes = lolliq_qlen_bytes ( &taildrop->queue ) + ETHERNET_OVERHEAD * lolliq_count ( &taildrop->queue );
        int diff = ( int ) taildrop->byte_limit - ( int ) qlen_bytes;

        for ( num_to_enqueue = 0 ; num_to_enqueue < num_pkts; num_to_enqueue++ ) {
            diff -= rte_pktmbuf_data_len ( pkts[num_to_enqueue] ) + ETHERNET_OVERHEAD;
            if ( diff < 0 ) {
                break;
            }
        };
    }

    // enqueue packets

    unsigned num_enqueued = lolliq_enqueue_burst ( &taildrop->queue, pkts, num_to_enqueue );

    // add sojourn to packets that did fit in
    unsigned pkt_num;
    for ( pkt_num = 0 ; pkt_num < num_enqueued; pkt_num++ ) {
        timestamp_metadata_t* metadata = MBUF2TS_PTR ( pkts[pkt_num] );
        metadata->timestamp = rte_get_tsc_cycles();
    };

    // drop packets which didn't fit in

//   unsigned pkt_num;
    if ( unlikely ( num_enqueued < num_pkts ) ) {
        taildrop_on_pkts_drop ( taildrop->trace_queue, rte_get_timer_cycles(), num_pkts - num_enqueued );
        for ( pkt_num = num_enqueued ; pkt_num < num_pkts; pkt_num++ ) {
            rte_pktmbuf_free ( pkts[pkt_num] );
        };
    }


    return num_enqueued;
}

static unsigned qelem_taildrop_peek_burst ( qelem_t *qelem, struct rte_mbuf*** burst_ptr )
{
    QCAST ( taildrop );
    return lolliq_peek_burst ( &taildrop->queue, burst_ptr );
}

static void qelem_taildrop_dequeue_peeked_burst ( qelem_t *qelem, struct rte_mbuf** peeked_burst, unsigned num_pkts )
{
    QCAST ( taildrop );
    lolliq_dequeue_peeked_burst ( &taildrop->queue, peeked_burst, num_pkts );
#ifdef WITH_TRACES
    taildrop_on_pkts_dequeue ( taildrop->trace_queue, rte_get_timer_cycles(),
                               lolliq_count ( &taildrop->queue ), lolliq_qlen_bytes ( &taildrop->queue ), num_pkts
                             ,0);
#endif
    LOG_VAR ( taildrop->trace_queue->sampling_index, "%lu" );

}

static unsigned qelem_taildrop_queue_len_bytes ( qelem_t *qelem )
{
    QCAST ( taildrop );
    return lolliq_qlen_bytes ( &taildrop->queue );
}

static unsigned qelem_taildrop_queue_len_pkts ( qelem_t *qelem )
{
    QCAST ( taildrop );
    return lolliq_count ( &taildrop->queue );
}

//------------------------------------------------------------------------------------------------------
// default opts
//------------------------------------------------------------------------------------------------------

static
void qelem_taildrop_tx_flush ( qelem_t *qelem )
{
    QCAST ( taildrop );
    tsc_cycles_t now = rte_get_tsc_cycles();

    struct rte_mbuf** burst;
    unsigned num_pkts = lolliq_peek_burst ( &taildrop->queue, &burst );

    if ( likely ( num_pkts > 0 ) ) {
        unsigned num_tx = rte_eth_tx_burst ( qelem->port_id, qelem->queue_id, burst, num_pkts );
        if ( num_tx > 0 ) {
            lolliq_dequeue_peeked_burst ( &taildrop->queue, burst, num_tx );
	    // get sojourn of last packet and plot it
	    struct rte_mbuf* pkt = burst[num_tx-1];
            tsc_cycles_t pkt_sojourn_time = now - MBUF2TS_PTR(pkt)->timestamp;
            taildrop_on_pkts_dequeue ( taildrop->trace_queue, rte_get_timer_cycles(),
                                       lolliq_count ( &taildrop->queue ), lolliq_qlen_bytes ( &taildrop->queue ), num_tx
                                     ,pkt_sojourn_time);
//       LOG_VAR(taildrop->trace_queue->sampling_index, "%lu");
//       LOG_VAR( (taildrop->trace_queue->sampling_index & (TRACES_QSAMPLING-1)), "%lu");
//      LOG_VAR(pkt->data_len, "%u");
        }
    }
}

static
void qelem_taildrop_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    QCAST ( taildrop );
//    tsc_cycles_t now = rte_get_tsc_cycles();

    // enqueue packets
//    qelem_taildrop_enqueue_burst ( qelem, pkts, num_pkts,now );
    qelem_taildrop_enqueue_burst ( qelem, pkts, num_pkts );

    // if there is at least burst packets in the queue, transmit packets
    if ( likely ( lolliq_count ( &taildrop->queue ) >= taildrop->qelem.min_send_burst ) ) {
        qelem_taildrop_tx_flush ( qelem );
    }
}

static
void qelem_taildrop_free ( qelem_t* qelem )
{
    rte_free ( qelem );
};

//-----------------------------------------------------------------------------------------------------------
// trace log
//-----------------------------------------------------------------------------------------------------------

#ifdef WITH_TRACES
static
void* taildrop_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
    LOG_LINE();
    return trace_event_queue_add ( "taildrop", common_params->trace_queue_size, sizeof ( taildrop_event_t ), taildrop_event_log );
}
#endif

//--------------------------------------------------------------------------------------------------------

qelem_factory_t qelem_taildrop_factory = {
    .qelem_alloc = qelem_taildrop_alloc,
#ifdef WITH_TRACES
    .trace_queue_alloc = taildrop_trace_queue_alloc,
#endif
};

static qelem_ops_t qelem_taildrop_ops = {
    .qelem_tx_burst = qelem_taildrop_tx_burst,
    .qelem_tx_flush = qelem_taildrop_tx_flush,
    .qelem_free = qelem_taildrop_free,

    .enqueue_burst = qelem_taildrop_enqueue_burst,
    .peek_burst = qelem_taildrop_peek_burst,
    .dequeue_peeked_burst = qelem_taildrop_dequeue_peeked_burst,
    .queue_len_bytes = qelem_taildrop_queue_len_bytes,
    .queue_len_pkts = qelem_taildrop_queue_len_pkts,
};

