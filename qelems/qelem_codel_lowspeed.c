#include "qelems.h"
#include "../rte_include.h"
#include "../config/log_macros.h"
#include "qdt/ppqueue.h"
#include "macros/lowspeed.h"

// TODO select sqrt -> approx/double
// TODO select reenter mode - draft,linux,cake,...

#include "qtrl/codel.h"


typedef struct
{
  qelem_t qelem;
  codel_params_t codel_params;
  codel_state_t codel_state;
  ppqueue_t queue;
} qelem_codel_t;


typedef struct
{
  uint64_t timestamp;
} codel_metadata_t;

//-----------------------------------------------------------------------------------------------------------
static qelem_ops_t qelem_codel_lowspeed_ops;

static
qelem_t* qelem_codel_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
  LOG_INIT ( "allocating qelem_codel_lowspeed for port %d \n", common_params->port_id );

  qelem_codel_t* codel = 0;

  QPARAMSCAST ( codel );
  QELEM_INIT ( codel );

  ctrl_codel_init ( &codel->codel_state, &codel->codel_params,
                    params->interval_sec, params->target_sec, params->mtu, TIMESTAMP_HZ() );

  return ( qelem_t* ) codel;
};

//-----------------------------------------------------------------------------------------------------------

static
unsigned qelem_codel_enqueue_burst ( qelem_t*qelem, struct rte_mbuf **pkts,  uint32_t num_pkts , unsigned hw_queue_index )
{
  QCAST ( codel );
  tsc_cycles_t now = TIMESTAMP();

  unsigned num_enq;
  for ( num_enq = 0; num_enq < num_pkts; num_enq++ )
  {
    struct rte_mbuf* pkt = pkts[num_enq];
    codel_metadata_t* metadata = ( codel_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkt , 0 );
    metadata->timestamp = now;

    if ( !ppq_enqueue ( &codel->queue,pkt ) )
    {
      break;
    }
  }

  unsigned pkt_to_drop;
  if ( unlikely ( num_enq < num_pkts ) )
  {
    for ( pkt_to_drop = num_enq ; pkt_to_drop < num_pkts; pkt_to_drop++ )
    {
      rte_pktmbuf_free ( pkts[pkt_to_drop] );
    };
    QELEM_TAIL_DROP_TS ( codel, now, num_pkts-num_enq );
  }

  return num_enq;
}




static struct rte_mbuf* codel_dequeue_one ( qelem_codel_t * codel )
{
  struct rte_mbuf* pkt = 0;
  tsc_cycles_t now;

  while ( 1 )
  {
    // peek packet
    pkt = ppq_peek ( &codel->queue );
    now = TIMESTAMP();

    if ( !pkt )
    {
      ctrl_codel_on_pkt_dequeue ( &codel->codel_state, &codel->codel_params, 0, now, 0 );
      return NULL;
    }
    else
    {
      codel_metadata_t* metadata = ( codel_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkt, 0 );
      boolean_t should_drop = ctrl_codel_on_pkt_dequeue ( &codel->codel_state, &codel->codel_params, metadata->timestamp, now, ppq_len ( &codel->queue ) );
      if ( !should_drop )
      {
        return pkt;
      }
      else
      {
        QELEM_AQM_DROP ( codel );
        ppq_dequeue_peeked ( &codel->queue );
        rte_pktmbuf_free ( pkt );
      }
    }
  }
}

static
void qelem_codel_xmit ( qelem_t *_qelem, unsigned hw_queue_index )
{
  qelem_codel_t* codel = ( qelem_codel_t* ) _qelem;

  struct rte_mbuf* pkt = codel_dequeue_one ( codel );
  if ( !pkt ) { return; }

//   LOG_VAR(ppq_len(&codel->queue), "%u");

  // try to send packet
  boolean_t sent = ( rte_eth_tx_burst ( codel->qelem.port_id, hw_queue_index, &pkt, 1 ) == 1 );
  if ( sent )
  {
    ppq_dequeue_peeked ( &codel->queue );
  }
}

static
void qelem_codel_free ( qelem_t* qelem )
{
  qelem_codel_t* codel = ( qelem_codel_t* ) qelem;

  ppq_destroy ( &codel->queue );
  rte_free ( codel );
};

//--------------------------------------------------------------------------------------------------------

static qelem_ops_t qelem_codel_lowspeed_ops =
{
  .enqueue_burst = qelem_codel_enqueue_burst,
  .xmit = qelem_codel_xmit,
  .qelem_free = qelem_codel_free,
};

qelem_factory_ops_t qelem_codel_lowspeed_factory =
{
  .qelem_alloc = qelem_codel_alloc,
};
