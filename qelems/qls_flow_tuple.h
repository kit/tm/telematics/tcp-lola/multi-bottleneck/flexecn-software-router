#ifndef	__QLS_FLOW_TUPLE__
#define	__QLS_FLOW_TUPLE__

#include <stdint.h>
#include "qls.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

qls_t* qls_flow_tuple_hash_create ( void* qls_params );
qls_t* qls_flow_tuple_rss_create ( void* qls_params );

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
