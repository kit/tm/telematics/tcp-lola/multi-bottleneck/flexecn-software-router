#include "qelem.h"
#include "../rte_include.h"
#include "qlog.h"
#include "macros/common.h"

#ifdef IN_IDE_PARSER
#define MAX_BURST 32
#endif

#ifndef MAX_BURST
#error please define MAX_BURST
#endif

static qelem_ops_t qelem_noop_ops;

typedef struct {
    qelem_t qelem;
    unsigned burst_size;
    unsigned burst_index;
    struct rte_mbuf* burst[];
} qelem_noop_t;

//------------------------------------------------------------------------------------------------------

static
qelem_t* qelem_noop_alloc ( const  qelem_params_t* common_params,const   void* qelem_params )
{
    LOG_INIT ( "allocating qelem_noop for port %d hw_queue %d \n", common_params->port_id, common_params->tx_queue_id );
    
    qelem_noop_t* qelem;
    QZMALLOC ( qelem, qelem_noop_t, sizeof(void*) * MAX_BURST );
    qelem_init ( ( qelem_t* ) qelem, common_params, &qelem_noop_ops );
    qelem->burst_size = common_params->min_send_burst;
    
    LOG_VAR(qelem->burst_size, "%u");

    return ( qelem_t* ) qelem;
};

//------------------------------------------------------------------------------------------------------
// default functions
//------------------------------------------------------------------------------------------------------

static inline
unsigned send_or_drop_burst ( uint8_t port_id, uint16_t queue_id, struct rte_mbuf** burst, uint16_t num_pkts )
{
    unsigned num_tx = rte_eth_tx_burst ( port_id, queue_id, burst, num_pkts );

    if ( unlikely ( num_tx < num_pkts ) ) {
        unsigned pkt_to_drop;
        for ( pkt_to_drop = num_tx ; pkt_to_drop < num_pkts; pkt_to_drop++ ) {
            rte_pktmbuf_free ( burst[pkt_to_drop] );
        };
    }

    return num_tx;
}

static
void qelem_noop_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    QCAST ( noop );
//     QELEM_PKTS_IN ( noop, num_pkts );

    unsigned pkt_num;
    for ( pkt_num = 0; pkt_num < num_pkts; pkt_num++ ) {
        noop->burst[noop->burst_index++] = pkts[pkt_num];

        if ( unlikely ( noop->burst_index == noop->burst_size ) ) {
            unsigned num_tx = send_or_drop_burst ( qelem->port_id, qelem->queue_id, noop->burst, noop->burst_index );
//             QELEM_TAIL_DROP ( noop, noop->burst_index - num_tx );
            noop->burst_index  = 0;
        }
    }
}

static
void qelem_noop_tx_flush ( qelem_t *qelem )
{
    QCAST ( noop );

    if ( noop->burst_index > 0 ) {
        unsigned num_tx = send_or_drop_burst ( qelem->port_id, qelem->queue_id, noop->burst, noop->burst_index );
//         QELEM_TAIL_DROP ( noop, noop->burst_index - num_tx );
        noop->burst_index  = 0;
    }
}

static
void qelem_noop_free ( qelem_t* qelem )
{
    rte_free ( qelem );
};

// qelem_noop doesn't support being elements of another elems -------------------------------------------------------------------------------------

qelem_factory_t qelem_noop_factory = {
    .qelem_alloc  = qelem_noop_alloc,
};

static qelem_ops_t qelem_noop_ops = {
    .qelem_tx_burst = qelem_noop_tx_burst,
    .qelem_tx_flush = qelem_noop_tx_flush,
    .qelem_free = qelem_noop_free,
};



