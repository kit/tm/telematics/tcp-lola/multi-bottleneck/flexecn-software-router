#include "qelem.h"
#include "qelem_fq_codel.h"
#include "qelem_types.h"
#include "codel/codel_control.h"
#include <sys/queue.h>
#include "../rte_include.h"
#include "../config/log_macros.h"
#include "../datapath/packet_metadata.h"

#define BURST_SIZE 32
#define MAX_TAILQ_NUMBER 4

static qelem_ops_t qelem_fq_codel_ops;

//-------------------------------------------------------------------------------------------------------------------------

TAILQ_HEAD ( subq_pkt_head, __fq_codel_metadata );

typedef struct __subqueue {
    // entry for linked list of new/old queues
    TAILQ_ENTRY ( __subqueue ) queues_list_entry;

    // head of the pkt queue
    struct subq_pkt_head subq_pkt_head;

    // queue state variables
    boolean_t is_in_list_of_new_queues;
    boolean_t is_in_list_of_old_queues;
    unsigned bytes_enq;
    int deficit;

    codel_state_t perq_codel_state;

} subqueue_t;

TAILQ_HEAD ( queues_list, __subqueue );

typedef struct {
    qelem_t qelem;

    codel_params_t global_codel_params;

    struct queues_list new_queues;
    struct queues_list old_queues;

    unsigned pkt_limit;
    unsigned curr_pkt_counter;

    unsigned subq_num;
    subqueue_t subqueues[0];

} qelem_fq_codel_t ;

#define QUANTUM 1514


typedef struct __fq_codel_metadata {
    uint64_t timestamp;
    TAILQ_ENTRY ( __fq_codel_metadata ) pkt_queue_entry;
    struct rte_mbuf* parent_pkt;

} fq_codel_metadata_t;

//-------------------------------------------------------------------------------------------------------------------------

static
qelem_t* qelem_fq_codel_alloc ( qelem_params_t* common_params, void* qelem_params )
{
    qelem_fq_codel_params_t* params = ( qelem_fq_codel_params_t* ) qelem_params;
    qelem_fq_codel_t* qelem = rte_zmalloc ( "qelem_fq_codel", sizeof ( qelem_fq_codel_t ) + params->num_subqueues * sizeof ( subqueue_t ), RTE_CACHE_LINE_SIZE );

    qelem->qelem.v_table = &qelem_fq_codel_ops;
    qelem->qelem.port_id = common_params->port_id;
    qelem->subq_num = params->num_subqueues;

    TAILQ_INIT ( &qelem->new_queues );
    TAILQ_INIT ( &qelem->old_queues );

//   subqueue_t** subqueues_ptrs = qelem->subqueues;
    unsigned qnum;
    for ( qnum = 0; qnum < qelem->subq_num; qnum++ ) {
        TAILQ_INIT ( & qelem->subqueues[qnum].subq_pkt_head );
    }

//     qelem->pkt_limit = params->pkt_hard_limit;
    qelem->pkt_limit = 10240 / 2 + 1024 * 1 + 512 + 256;

    double tsc_freq = rte_get_tsc_hz();
//   double timer_freq = rte_get_timer_hz();
    double interval_tsc = ( ( double ) 100 ) * 1e-6 * tsc_freq;
    double target_tsc = ( ( double ) 10 ) * 1e-6 * tsc_freq;

    qelem->global_codel_params.interval = interval_tsc;
    qelem->global_codel_params.target  = target_tsc;

    return ( qelem_t* ) qelem;
};

static
void qelem_fq_codel_free ( qelem_t* qelem )
{
    qelem_fq_codel_t* qelem0 = ( qelem_fq_codel_t* ) qelem;
    rte_free ( qelem0->subqueues );
    rte_free ( qelem0 );
};

//-------------------------------------------------------------------------------------------------------------------------

static
unsigned qelem_fq_codel_enqueue_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts , unsigned hw_queue_index )
{
    qelem_fq_codel_t* fq_codel = ( qelem_fq_codel_t* ) qelem;

    unsigned  pkt_num;
    tsc_cycles_t now = rte_get_tsc_cycles();

    for ( pkt_num = 0; pkt_num < num_pkts; pkt_num++ ) {
        // read hash value from packet metadata
        packet_metadata_t* metadata = ( packet_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkts[pkt_num], 0 );
        uint64_t flow_key_sig = metadata->flow_key_sig;
        uint64_t subq_num = flow_key_sig % fq_codel->subq_num;
        subqueue_t* subq = &fq_codel->subqueues[subq_num];

        // from this point we don't need packet metadata, replace it
        fq_codel_metadata_t* fq_codel_entry = ( fq_codel_metadata_t* ) metadata;
        fq_codel_entry->timestamp = now;
        fq_codel_entry->parent_pkt = pkts[pkt_num];


        TAILQ_INSERT_TAIL ( &subq->subq_pkt_head, fq_codel_entry, pkt_queue_entry );
	subq->bytes_enq += pkts[pkt_num]->data_len;

        if ( !subq->is_in_list_of_new_queues && !subq->is_in_list_of_old_queues ) {
            // if subq is in neither of the lists -> add it to the new subq list
            subq->deficit = QUANTUM;
            subq->is_in_list_of_new_queues = TRUE;
            TAILQ_INSERT_TAIL ( &fq_codel->new_queues, subq, queues_list_entry );
        } else if ( subq->is_in_list_of_new_queues ) {
            // if subq was in the list of new queues, remove it from the list
            // of new queues and add to the list of old queues
            subq->is_in_list_of_new_queues = FALSE;
            subq->is_in_list_of_old_queues = TRUE;
            TAILQ_REMOVE ( &fq_codel->new_queues, subq, queues_list_entry );
            TAILQ_INSERT_TAIL ( &fq_codel->old_queues, subq, queues_list_entry );
        } else {
            // subq is in the list of old queues -> reinsert it at the tail
            TAILQ_REMOVE ( &fq_codel->old_queues, subq, queues_list_entry );
            TAILQ_INSERT_TAIL ( &fq_codel->old_queues, subq, queues_list_entry );
        }

        fq_codel->curr_pkt_counter++;


        if ( fq_codel->curr_pkt_counter >= fq_codel->pkt_limit ) {	  
            subqueue_t* iterq,*victimq = 0;
            unsigned max = 0;
            TAILQ_FOREACH ( iterq, &fq_codel->new_queues, queues_list_entry ) {
                if ( iterq->bytes_enq > max ) {
                    victimq = iterq;
                    max = iterq->bytes_enq;
                }
            }
            TAILQ_FOREACH ( iterq, &fq_codel->old_queues, queues_list_entry ) {
                if ( iterq->bytes_enq > max ) {
                    victimq = iterq;
                    max = iterq->bytes_enq;
                }
            }

            // drop packet from the queue
// this can't happen if we have 10240 packets in the queue if ( victimq && !TAILQ_EMPTY(&victimq->subq_pkt_head)) {
                fq_codel_metadata_t* pktm = TAILQ_FIRST ( &victimq->subq_pkt_head );
                TAILQ_REMOVE ( &victimq->subq_pkt_head , pktm, pkt_queue_entry );
                victimq->bytes_enq-=pktm->parent_pkt->data_len;
                rte_pktmbuf_free ( pktm->parent_pkt );
//             }
        }
    }

    return pkt_num;
};

static subqueue_t* fq_codel_select_subq ( qelem_fq_codel_t* fq_codel, boolean_t* is_new_queue )
{
    subqueue_t* subq = 0;

    // traverse list of new queues
    TAILQ_FOREACH ( subq, &fq_codel->new_queues, queues_list_entry ) {
        if ( subq->deficit < 0 ) {
            subq->deficit += QUANTUM;
            subq->is_in_list_of_new_queues = FALSE;
            subq->is_in_list_of_old_queues = TRUE;
            TAILQ_REMOVE ( &fq_codel->new_queues, subq, queues_list_entry );
            TAILQ_INSERT_TAIL ( &fq_codel->old_queues, subq, queues_list_entry );
        } else {
            *is_new_queue=TRUE;
            return subq;
        }
    }

    // traverse list of old queues
    TAILQ_FOREACH ( subq, &fq_codel->old_queues, queues_list_entry ) {
        if ( subq->deficit < 0 ) {
            subq->deficit += QUANTUM;
            TAILQ_REMOVE ( &fq_codel->old_queues, subq, queues_list_entry );
            TAILQ_INSERT_TAIL ( &fq_codel->old_queues, subq, queues_list_entry );
        } else {
            *is_new_queue=FALSE;
            return subq;
        }
    }

    return 0;
}



static struct rte_mbuf* fq_codel_run_codel_on_queue ( qelem_fq_codel_t* fq_codel, subqueue_t* subq )
{
#define MTU 1518

    while ( 1 ) {
        if ( TAILQ_EMPTY ( & subq->subq_pkt_head ) ) {
            codel_ctrl_exit_congestion_state ( &subq->perq_codel_state );
            return 0;
        }

        // dequeue packet from the queue
        fq_codel_metadata_t* pktm = TAILQ_FIRST ( &subq->subq_pkt_head );
        TAILQ_REMOVE ( &subq->subq_pkt_head , pktm, pkt_queue_entry );
        subq->bytes_enq-=pktm->parent_pkt->data_len;
	fq_codel->curr_pkt_counter--;

        if ( subq->bytes_enq < MTU ) {
	    codel_ctrl_exit_congestion_state ( &subq->perq_codel_state );
            return pktm->parent_pkt;
        }

        tsc_cycles_t now = rte_get_tsc_cycles();

        // consult codel on whether to drop the packet
        boolean_t should_drop = codel_ctrl_on_pkt_dequeue ( &subq->perq_codel_state, &fq_codel->global_codel_params,
                                pktm->timestamp, now );

        if ( should_drop ) {
            rte_pktmbuf_free ( pktm->parent_pkt );
        } else {
            return pktm->parent_pkt;
        }
    }
}

static struct rte_mbuf* fq_codel_dequeue_one ( qelem_fq_codel_t* fq_codel )
{
    while ( 1 ) {
        boolean_t is_new_queue = 0;
        subqueue_t* subq = fq_codel_select_subq ( fq_codel, &is_new_queue );
        if ( !subq ) {
            return 0;
        }

        struct rte_mbuf* pkt = fq_codel_run_codel_on_queue ( fq_codel, subq );
        if ( pkt ) {
            subq->deficit -= pkt->data_len;
            return pkt;
        }

        // if queue is empty
        if ( is_new_queue ) {
            subq->is_in_list_of_new_queues = FALSE;
            subq->is_in_list_of_old_queues = TRUE;
            TAILQ_REMOVE ( &fq_codel->new_queues, subq, queues_list_entry );
            TAILQ_INSERT_TAIL ( &fq_codel->old_queues, subq, queues_list_entry );
        } else {
            // remove queue from the list of active queues
            subq->is_in_list_of_new_queues = FALSE;
            subq->is_in_list_of_old_queues = FALSE;
            TAILQ_REMOVE ( &fq_codel->old_queues, subq, queues_list_entry );
        }
    }
}

static
void qelem_fq_codel_xmit ( qelem_t *_qelem, unsigned hw_queue_index )
{
    qelem_fq_codel_t* fq_codel = ( qelem_fq_codel_t* ) _qelem;


    struct rte_mbuf* burst[BURST_SIZE];
    unsigned  burst_counter;

    unsigned  nb_tx ;
    struct rte_mbuf** burst1;


    // run codel BURST_SIZE times:
    for ( burst_counter = 0; burst_counter < BURST_SIZE; burst_counter++ ) {
        struct rte_mbuf* pkt = fq_codel_dequeue_one ( fq_codel );
        if ( pkt == 0 ) {
            break;
        }

        burst[burst_counter] = pkt;
    }
    
//     LOG_VAR(burst_counter, "%u");

    //now send the burst
    if ( burst_counter == 0 ) {
        return;
    };

    burst1 = &burst[0];

    do {
        nb_tx = rte_eth_tx_burst ( fq_codel->qelem.port_id, hw_queue_index, burst1, burst_counter );
        burst_counter-=nb_tx;
        burst1 = &burst1[nb_tx];
    } while ( burst_counter != 0 );
}

qelem_factory_ops_t qelem_fq_codel_factory = {
    .qelem_alloc = qelem_fq_codel_alloc,
    .qelem_free = qelem_fq_codel_free,
};

static qelem_ops_t qelem_fq_codel_ops = {
    .enqueue_burst = qelem_fq_codel_enqueue_burst,
    .xmit = qelem_fq_codel_xmit,
};

