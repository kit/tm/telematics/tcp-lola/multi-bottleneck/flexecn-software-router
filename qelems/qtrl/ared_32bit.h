#ifndef __QTRL_ARED__
#define __QTRL_ARED__

#include "../qelem_types.h"
#include "../qmath/fastrand.h"
#include "../qmath/qm_types.h"
#include "ared_log.h"
#include <math.h>
// #include <rte_random.h>

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

/**
 * @FILE
 *
 * ared controller
 */

typedef struct
{
  // normal red parameters
//   double w_q; // w_q
  unsigned w_log;
  Q_16_16 min_thresh;
  Q_16_16 max_thresh;
//   double min_thresh;
//   double max_thresh;

  // adaptive red parameters
  tsc_cycles_t update_interval;
  Q_16_16 target_min;
  Q_16_16 target_max;
//   double target_min;
//   double target_max;

//   double alpha_min;
//   double beta;
  Q_0_32 alpha_min;
  Q_0_32 beta;
  double typ_pkt_rate;

} ared_params_t;

typedef struct
{
  // qlen will be parameter passed by qelem
  Q_16_16 avg;
//   double avg; // EWMA average queue len
  Q_16_16 rand;
  int count;
  tsc_cycles_t empty_since;

  // in ARED max_p is recalculated
  Q_0_32 max_p;
  Q_16_16 C0; // max_p / max_th - min_th
//   double max_p;
  tsc_cycles_t last_recalc;
} ared_state_t;

//----------------------------------------------------------------------------------------------------------

static inline void
ctrl_ared_init ( ared_state_t* state, ared_params_t* params, tsc_cycles_t timer_freq );

static inline boolean_t
ctrl_ared_on_pkt_enqueue ( ared_state_t* state, const ared_params_t* params, tsc_cycles_t now, unsigned int qlen_pkts );

static inline void
ctrl_ared_on_pkt_dequeue ( ared_state_t* state, const ared_params_t* params , tsc_cycles_t now, unsigned int qlen_pkts );

static inline void
ctrl_ared_update_if_needed ( ared_state_t* state, const ared_params_t* params, tsc_cycles_t now, unsigned qlen_bytes );


//----------------------------------------------------------------------------------------------------------

static inline
Q_16_16 __ared_new_rand ( void )
{
//   fast_rand();
  uint32_t rand = fast_rand();
  return rand >> 16;
}

static inline void
__ared_update_avg_empty ( ared_state_t* state, const ared_params_t* params, tsc_cycles_t now )
{
  if ( state->avg == 0 ) return;

  tsc_cycles_t delta_t = now - state->empty_since;

//   LOG_VAR ( (double)delta_t, "%f" );
//   LOG_VAR ( params->typ_rate, "%f" );

  double m = params->typ_pkt_rate * delta_t; // TODO: fix this  2) reciprocal scale...
//   double w_q = 0.00390625;

//   LOG_VAR (m, "%f" );
//   double bracket = pow ( 1- w_q  , m ) ;
//   double bracket2 = bracket * state->avg;

//   Q_16_16 pow1 = q_16_16_from_double ( bracket );
  Q_16_16 m1 = q_16_16_from_double ( m );

  // a very grob approx (copied from Linux, if not enough copy from DPDK)
  // (1-w_q)^m = e^(m ln (1-w_q)) = [Taylor e^x] = 1 + m ln (1-w_q) =
  // =[Taylor ln(1+x)] = 1 - m * w_q = 1 - m >> w_log
  // then
  // (1-w_q)^m * avg = avg - avg * m * w_q = avg - m * avg >> w_log;

  Q_16_16 m_avg_w = q_16_16_mul ( m1, state->avg ) >> params->w_log;
//   Q_16_16 m_avg_w1 = (uint32_t) bracket2;

//   LOG_VAR ( bracket, "%f" );
//   LOG_VAR ( bracket2, "%f" );
//   LOG_VAR ( pow1, "%.8x" );
//   LOG_VAR ( m_avg_w, "%.8x" );
//   LOG_VAR ( m_avg_w1, "%.8x" );
//   LOG_VAR ( state->avg, "%.8x" );

  if ( state->avg > m_avg_w )
  {
    state->avg -= m_avg_w;
  }
  else
  {
    state->avg = 0;
  }
}

static inline void
__ared_update_avg_nonempty ( ared_state_t* state, const ared_params_t* params, unsigned qlen_pkts )
{
  // update average
  // avg = (1 - w_q) * avg + w_q * qlen
  // avg = avg + w_q (qlen - avg)
  // avg += (qlen (uint) - avg (Q16.16)) * w_q
  // avg += ((qlen << 16) - avg) >> wlog

  Q_16_16 qlen_pkts1 = q_16_16_from_uint ( qlen_pkts );

  if ( qlen_pkts1 > state->avg )
  {
    state->avg += ( qlen_pkts1 - state->avg ) >> params->w_log;
  }
  else
  {
    state->avg -= ( state->avg - qlen_pkts1 ) >> params->w_log;
  }


//   LOG_VAR ( qlen_pkts1, "%.8x" );
//   LOG_VAR ( state->avg, "%.8x" );

//   state->avg += ( q_16_16_from_uint ( qlen_pkts ) - state->avg ) >> params->w_log;
}



static inline boolean_t
__ared_should_drop ( ared_state_t* state, const ared_params_t* params, tsc_cycles_t now, unsigned int qlen_pkts )
{
  if ( qlen_pkts == 0 )
  {
    __ared_update_avg_empty ( state, params, now );
  }
  else
  {
    __ared_update_avg_nonempty ( state, params, qlen_pkts );
    state->empty_since = 0;
  }

  boolean_t res = FALSE;

  // compare avg with min_th and max_th

  // if avg < min_th don't drop
  if ( state->avg < params->min_thresh )
  {
    state->count = -1;
  }
  // drop packet with some probability
  else if ( state->avg >= params->min_thresh && state->avg < params->max_thresh )
  {
    state->count ++;

//     double C0 = state->max_p / ( params->max_thresh - params->min_thresh );
//     double p_b = C0 * ( state->avg - params->min_thresh );
    Q_16_16 p_b = q_16_16_mul ( state->C0, state->avg - params->min_thresh );


    if ( state->count > 0 )
    {
      // calculate cond: count >= rand/ p_b
      // or p_b * count >= rand
      if ( state->count * p_b >= state->rand )
      {
        state->count = 0;
        state->rand = __ared_new_rand ( );
        res = TRUE;
      }
    }
    else
    {
      state->rand = __ared_new_rand ( );
    };
  }
  // if avg > max_th definitely drop packet
  else if ( state->avg > params->max_thresh )
  {
    state->count = -1;
    res = TRUE;
  }

  ARED_LOG_ON_ENQ ( now, qlen_pkts, state->avg, state->count, res, state->max_p );
  return res;
}

static inline void
__ared_update_maxp ( ared_state_t* state, const ared_params_t* params, unsigned int qlen_pkts , tsc_cycles_t now )
{
// if (avg > target_max and max_p <= 0.5)
//	increase max_p:
//		max_p = max_p + alpha
// elseif (avg < target_min and max_p >= 0.01)
//	decrease max_p:
//		max_p = max_p * beta


  // if queue is empty, then first update avg ( (c) Linux )
  if ( state->empty_since != 0 )
  {
    __ared_update_avg_empty ( state, params, now );
    state->empty_since = now;
  }

  // max_p is Q_0_32
  const Q_0_32 fifty_percents = q_0_32_from_double ( 0.5 );
  const Q_0_32 one_percent = q_0_32_from_double ( 0.01 );

//   LOG_VAR ( fifty_percents, "0x%.8x" );
//   LOG_VAR ( one_percent, "0x%.8x" );
//   LOG_VAR ( state->max_p, "0x%.8x" );
//   LOG_VAR ( state->avg, "0x%.8x" );

  if ( state->avg > params->target_max && state->max_p <= fifty_percents )
  {
    Q_0_32 alpha = state->max_p / 4;
    alpha = ( alpha > params->alpha_min ) ? params->alpha_min : alpha;

    state->max_p += alpha;
  }
  else if ( state->avg < params->target_min && state->max_p >= one_percent )
  {
    state->max_p = q_0_32_mul ( state->max_p , params->beta );
  }
//   else
//   {
//     // no changes to p
//     return;
//   }

  // recalculate C0 for probability calculations
  // C0 = max_p / ( max_th - min_th )
  Q_16_16 delta = params->max_thresh - params->min_thresh;
  Q_0_32 max_p = state->max_p;

  // max_p is 0.aaaa * 2^32 , delta is bb.bb * 2^16
  // max_p / delta = a * 2 ^ 32 / b * 2^16 = a/b * 2^16 which is Q_16_16

  state->C0 = max_p/delta; // looks good :)




}

//----------------------------------------------------------------------------------------------------------

void ctrl_ared_init ( ared_state_t* state, ared_params_t* params, tsc_cycles_t timer_freq )
{
  fast_srand ( timer_freq );

  unsigned min_th = 20; // set to target_delay * C (pkts) / 2
  unsigned max_th = min_th * 3; // according to ARED paper max_th = 3 * min_th
  unsigned delta = max_th - min_th;

  // TODO: convert to Q1616 then divide?
  unsigned target_min = min_th + 2 * delta / 5;
  unsigned target_max = min_th + 3 * delta / 5;

  LOG_VAR ( target_min, "%u" );
  LOG_VAR ( target_max, "%u" );

//   params->w_q = 0.00390625; // (2^-8) // TODO: understand $4.3 from ARED paper
  params->w_log = 8;

  params->min_thresh = q_16_16_from_uint ( min_th );
  params->max_thresh = q_16_16_from_uint ( max_th );

  params->update_interval = 0.5 * timer_freq; // ok, this must be about 5 * RTT, so probably 100ms ?

  params->target_min = 	q_16_16_from_uint ( target_min );
  params->target_max = q_16_16_from_uint ( target_max );

  LOG_VAR ( params->target_min, "0x%.8x" );
  LOG_VAR ( params->target_max, "0x%.8x" );

  params->alpha_min = q_0_32_from_double ( 0.01 );
  params->beta = q_0_32_from_double ( 0.9 );
//   params->typ_rate = 12176e-8 * timer_freq; // this should be time to send 1 pkt
  double typ_pkt_rate_s = 100.0 * 1e6 / ( 1522 * 8 ); // 100Mbit / (MTU * 8)
  params->typ_pkt_rate = typ_pkt_rate_s / timer_freq;

  // assume that state is zalloced
  state->max_p = q_0_32_from_double ( 0.5 );
  state->C0 = state->max_p  / ( params->max_thresh - params->min_thresh );

//   double C0 = 0.01 / (max_th - min_th);

//   LOG_VAR(state->C0, "%u");
//   LOG_VAR(C0, "%f");
}

void ctrl_ared_update_if_needed ( ared_state_t* state, const ared_params_t* params, tsc_cycles_t now, unsigned int qlen_bytes )
{
  if ( now - state->last_recalc > params->update_interval )
  {
//     LOG_LINE();
    __ared_update_maxp ( state, params, qlen_bytes,now );
    state->last_recalc = now;
  }
}


boolean_t ctrl_ared_on_pkt_enqueue ( ared_state_t* state, const ared_params_t* params, tsc_cycles_t now, unsigned int qlen_pkts )
{
  return __ared_should_drop ( state, params, now, qlen_pkts );
}

void ctrl_ared_on_pkt_dequeue ( ared_state_t* state, const ared_params_t* params , tsc_cycles_t now, unsigned int qlen_pkts )
{
  if ( qlen_pkts == 0 )
    state->empty_since = now;
}



#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
