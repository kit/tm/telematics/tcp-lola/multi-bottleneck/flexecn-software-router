#ifndef	__QTRL_CODEL__
#define	__QTRL_CODEL__

// parameters:
// 	-D APPROX_MATH use approximate sqrt root
#define APPROX_MATH

#include "../qelem_types.h"
#ifdef APPROX_MATH
#include "../qmath/approx_reciprocal_sqrt.h"
#endif
#include "../traces/codel_event.h"

#ifdef FQ_CODEL
#define FQ_QUEUE_VAR_DECL , void* fq_queue_ptr
#define FQ_QUEUE_PARAM ,fq_queue_ptr
#else
#define FQ_QUEUE_VAR_DECL
#define FQ_QUEUE_PARAM
#endif

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

/**
 * @FILE
 *
 * CODEL AQM Logic as in Cake (see here: https://github.com/dtaht/sch_cake/blob/master/codel5.h)
 */

typedef struct
{
  tsc_cycles_t interval;
  tsc_cycles_t target;
  uint64_t threshold; // this value is in tsc^2
} codel_params_t;

typedef struct
{
  // estimator
  tsc_cycles_t sojourn_above_since;

  // controller
  boolean_t is_in_dropping_state;
  tsc_cycles_t drop_next;
  uint32_t pkts_dropped;
#ifdef APPROX_MATH
  Q_0_32 approx_inv_sqrt;
#endif

#ifdef WITH_TRACES
  event_log_queue_t* trace_queue;
#endif

} codel_state_t;

//---------------------------------------------------------------------------------------------------
// interface aka public methods
//---------------------------------------------------------------------------------------------------


static inline void ctrl_codel_init ( codel_state_t* codel_state, codel_params_t* codel_params,
                                     double interval_s, double target_s, unsigned mtu_bytes, tsc_cycles_t timer_freq,
                                     boolean_t init_params, void* trace_queue
                                   );

static inline boolean_t ctrl_codel_on_pkt_dequeue ( codel_state_t* codel_state, const codel_params_t* codel_params,
    tsc_cycles_t pkt_enq_time, tsc_cycles_t now, unsigned qlen_bytes FQ_QUEUE_VAR_DECL
                                                  );

//----------------------------------------------------------------------------------------------------------------

#ifdef APPROX_MATH
static inline
uint64_t __codel_control_law ( tsc_cycles_t time, codel_state_t* state, const codel_params_t* params )
{
  return time + int_q_0_32_mul ( params->interval, state->approx_inv_sqrt ); // interval / sqrt ( pkts_dropped );
}
#else
#include <math.h>
static inline
uint64_t __codel_control_law ( tsc_cycles_t time,  codel_state_t* state, const codel_params_t* params )
{
  return time + ( ( double ) params->interval ) / sqrt ( state->pkts_dropped );
}
#endif

static inline
boolean_t __codel_congestion_estimator ( codel_state_t* state, const codel_params_t* params, tsc_cycles_t enqueued_time, tsc_cycles_t now, unsigned qlen_bytes )
{
  tsc_cycles_t sojourn_time = enqueued_time ? now - enqueued_time : 0;

  // exit congestion state when sojourn time falls
  if ( sojourn_time < params->target || qlen_bytes == 0 )
  {
    state->sojourn_above_since = 0;
    return FALSE;
  }
  // if we are dropping, continue dropping
  else if ( state->is_in_dropping_state )
  {
    return TRUE;
  }
  // if it is first packets with sojourn_time > target
  else if ( state->sojourn_above_since ==0 )
  {
    state->sojourn_above_since = now;
    return FALSE;
  }
  // if we were recently dropping, enter dropping state sooner
  else if ( state->pkts_dropped > 1 && now - state->drop_next < 8 * params->interval )
  {
    return ( now - state->sojourn_above_since ) > __codel_control_law ( now, state, params );
  }
  // this is something with slow start -> see Cake mailing list
  else
  {
    return ( now - state->sojourn_above_since ) * ( sojourn_time ) > params->threshold;
  }
}


static inline
void __codel_controller_exit_congestion_state ( codel_state_t* state, tsc_cycles_t now FQ_QUEUE_VAR_DECL )
{
  if ( state->is_in_dropping_state )
  {
    state->is_in_dropping_state = FALSE;
    codel_on_exit_dropping_state ( state->trace_queue, now FQ_QUEUE_PARAM );
  }
}

// this function should only be called when codels enters congestion state
static inline
void __codel_controller_enter_congestion_state ( codel_state_t* state, const codel_params_t* params, tsc_cycles_t now FQ_QUEUE_VAR_DECL )
{
  // enter dropping state
  state->is_in_dropping_state = TRUE;
  codel_on_enter_dropping_state ( state->trace_queue, now FQ_QUEUE_PARAM );

  if ( ( state->pkts_dropped >= 2 ) && ( ( now - state->drop_next ) < 8 * params->interval ) )
  {
    state->pkts_dropped /= 2;
    state->pkts_dropped -=1;
#ifdef APPROX_MATH
    state->approx_inv_sqrt = ( state->approx_inv_sqrt * 92682 ) >> 16; // time interval is multiplied by 1.414 // TODO rewrite in functions
#endif
  }
  else
  {
    state->pkts_dropped = 0;
  }

  state->drop_next = now-1;
}

// this function should only be called if the queue is in persistent queue state
static inline
boolean_t __codel_controller_should_drop_pkt ( codel_state_t* state, const codel_params_t* params, tsc_cycles_t now FQ_QUEUE_VAR_DECL )
{
  if ( !state->is_in_dropping_state )
  {
    __codel_controller_enter_congestion_state ( state,  params, now FQ_QUEUE_PARAM );
  }

// was there a packet that was dropped in the current dropping interval?
  if ( now >= state->drop_next )
  {
    state->pkts_dropped++;
    if ( state->pkts_dropped == 0 )
    {
      state->pkts_dropped = 0xffffffff;
    }

#ifdef APPROX_MATH
    approx_rec_sqrt_next ( &state->approx_inv_sqrt , state->pkts_dropped );
#endif
    state->drop_next = __codel_control_law ( state->drop_next, state, params );
    codel_on_aqm_drop ( state->trace_queue, now,  state->drop_next, state->pkts_dropped FQ_QUEUE_PARAM );
    return TRUE;
  }
  else
  {
    return FALSE;
  }
};

//-------------------------------------------------------------------------------------------------------------

/**
 * initializes codel
 */
void ctrl_codel_init ( codel_state_t* codel_state, codel_params_t* codel_params,
                       double interval_sec, double target_sec, unsigned int mtu_bytes,
                       tsc_cycles_t timer_freq,  boolean_t init_params, void* trace_queue )
{
  if ( init_params )
  {
    double interval_tsc = ( double ) interval_sec  * timer_freq;
    double target_tsc = ( double ) target_sec * timer_freq;

    codel_params->interval = interval_tsc;
    codel_params->target  = target_tsc;
    codel_params->threshold = interval_tsc * target_tsc * 2;
//     codel_params->mtu = mtu_bytes;

    LOG_PARAM ( timer_freq, "%lu" );
    LOG_PARAM ( interval_tsc, "%f" );
    LOG_PARAM ( target_tsc, "%f" );
    LOG_PARAM ( codel_params->interval, "%lu" );
    LOG_PARAM ( codel_params->target, "%lu" );
    LOG_PARAM ( codel_params->threshold, "%lu" );
  }

#ifdef APPROX_MATH
  approx_rec_sqrt_reset ( &codel_state->approx_inv_sqrt );
#endif

#ifdef WITH_TRACES
  codel_state->trace_queue = trace_queue;
  LOG_PARAM ( codel_state->trace_queue, "%p" );
#endif

}

/**
 * return whether the packet should be dropped
 */
boolean_t ctrl_codel_on_pkt_dequeue ( codel_state_t* codel_state, const codel_params_t* codel_params,
                                      tsc_cycles_t pkt_enq_time, tsc_cycles_t now, unsigned qlen_bytes FQ_QUEUE_VAR_DECL )
{
// call estimator
  boolean_t is_in_congestion = __codel_congestion_estimator ( codel_state, codel_params, pkt_enq_time, now, qlen_bytes );

  if ( !is_in_congestion )
  {
    __codel_controller_exit_congestion_state ( codel_state , now FQ_QUEUE_PARAM ); // TODO move to CE
    return FALSE;
  }

// in congestion -> call controller
  return __codel_controller_should_drop_pkt ( codel_state, codel_params, now FQ_QUEUE_PARAM );
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif

