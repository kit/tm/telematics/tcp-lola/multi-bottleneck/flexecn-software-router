#ifndef __QTRL_ARED__
#define __QTRL_ARED__

#include "../qelem_types.h"
#include "../qmath/fastrand.h"
#include "../traces/red_event.h"
#include <math.h>

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

/**
 * @FILE
 *
 * ared controller
 */

typedef struct
{
  // normal red parameters
  double w_q; // w_q
  double min_thresh;
  double max_thresh;

  // adaptive red parameters
  tsc_cycles_t update_interval;
  double target_min;
  double target_max;
  double alpha_min;
  double beta;
  double typ_pkt_rate;

} ared_params_t;

typedef struct
{
  // qlen will be parameter passed by qelem
  double avg; // EWMA average queue len
  double rand;
  int count;
  tsc_cycles_t empty_since;

  // in ARED max_p is recalculated
  double max_p;
  double C0;

#ifdef GENTLE  
  // GRED proportional constant (depends on max_p)
  double G0;
#endif  

  // RNG state
  rand_state_t rand_state;

  tsc_cycles_t last_recalc;

#ifdef WITH_TRACES
  event_log_queue_t* trace_queue;
#endif

} ared_state_t;

//----------------------------------------------------------------------------------------------------------

static inline void
ctrl_ared_init ( ared_state_t* state, ared_params_t* params, tsc_cycles_t timer_freq ,const qelem_ared_params_t* qparams , void* trace_queue );

static inline boolean_t
ctrl_ared_on_pkt_enqueue ( ared_state_t* state, const ared_params_t* params, tsc_cycles_t now, unsigned int qlen_pkts );

static inline void
ctrl_ared_on_pkt_dequeue ( ared_state_t* state, const ared_params_t* params , tsc_cycles_t now, unsigned int qlen_pkts );

static inline void
ctrl_ared_update_if_needed ( ared_state_t* state, const ared_params_t* params, tsc_cycles_t now );


//----------------------------------------------------------------------------------------------------------

static inline
double __ared_new_rand ( ared_state_t* state )
{
  uint32_t rand1 = fast_rand ( &state->rand_state );
  const uint32_t max_int = 0xffffffff;
  return ( ( double ) rand1 / ( double ) max_int );
}

static inline void
__ared_update_avg_empty ( ared_state_t* state, const ared_params_t* params, tsc_cycles_t now )
{
  tsc_cycles_t delta_t = now - state->empty_since;

  double m = params->typ_pkt_rate * delta_t;
  double w_q = params->w_q;

//   LOG_VAR(m, "%f");

  state->avg = state->avg * pow ( ( 1-params->w_q ), m );

}

static inline void
__ared_update_avg_nonempty ( ared_state_t* state, const ared_params_t* params, unsigned qlen_pkts )
{
  // update average
  // avg = (1 - w_q) * avg + w_q * qlen
  state->avg = ( 1 - params->w_q ) * state->avg + params->w_q * qlen_pkts;
}



static inline boolean_t
__ared_should_drop ( ared_state_t* state, const ared_params_t* params, tsc_cycles_t now, unsigned int qlen_pkts )
{
  if ( qlen_pkts == 0 )
  {
    __ared_update_avg_empty ( state, params, now );
  }
  else
  {
    __ared_update_avg_nonempty ( state, params, qlen_pkts );
    state->empty_since = 0;
  }
  
//   LOG_VAR(state->avg, "%f");

  boolean_t res = FALSE;
  double p_b = 0.0;

  // compare avg with min_th and max_th

  // if avg < min_th don't drop
  if ( state->avg < params->min_thresh )
  {
    state->count = -1;
  }
  // drop packet with some probability
  else if ( state->avg >= params->min_thresh && state->avg < params->max_thresh )
  {
    p_b = state->C0 * ( state->avg - params->min_thresh );
  }
#ifdef GENTLE
  else if ( state->avg >= params->max_thresh &&  state->avg < 2 * params->max_thresh )
  {
    p_b = state->G0 * state->avg + 2 * state->max_p -1;
  }
  else if ( state->avg > 2 * params->max_thresh )
#else
  else if ( state->avg > params->max_thresh )
#endif
  {
    state->count = -1;
    res = TRUE;
  }

  // RED's derandomizator(??)
  if ( p_b > 0 )
  {
    state->count++;

    if ( state->count > 0 )
    {
      // calculate cond: count >= rand/ p_b
      if ( state->count * p_b >= state->rand )
      {
        state->count = 0;
        state->rand = __ared_new_rand ( state );
        res = TRUE;
      }
    }
    else
    {
      state->rand = __ared_new_rand ( state );
    };
  }

  red_on_pkt_should_drop(state->trace_queue, now, qlen_pkts, state->avg, p_b, res);  
  return res;
}

static inline void
__ared_update_maxp ( ared_state_t* state, const ared_params_t* params, /*unsigned int qlen_pkts , */tsc_cycles_t now )
{
// if (avg > target_max and max_p <= 0.5)
//	increase max_p:
//		max_p = max_p + alpha
// elseif (avg < target_min and max_p >= 0.01)
//	decrease max_p:
//		max_p = max_p * beta

  double old_max_p = state->max_p;

  // if queue is empty, then first update avg ( (c) Linux )
  if ( state->empty_since != 0 )
  {
    __ared_update_avg_empty ( state, params, now );
    state->empty_since = now;
  }

//   LOG_VAR ( state->max_p, "%f" );
//   LOG_VAR ( state->avg, "%f" );
//   LOG_VAR ( params->target_max, "%f" );
//   LOG_VAR ( params->target_min, "%f" );

  if ( state->avg > params->target_max && state->max_p <= 0.5 )
  {
    double alpha = state->max_p / 4;
    alpha = ( alpha > params->alpha_min ) ? params->alpha_min : alpha;

    state->max_p += alpha;
  }
//   else if ( state->avg < params->target_min && state->max_p >= 0.01 )
  else if ( state->avg < params->target_min && state->max_p >= 0.000001 )
  {
    state->max_p = ( state->max_p * params->beta );
  }

  state->C0 = state->max_p / ( params->max_thresh - params->min_thresh );
#ifdef GENTLE  
  state->G0 = ( 1.0 - state->max_p ) / params->max_thresh;
#endif
  
//   LOG_LINE();
  
  if(state->max_p != old_max_p){
    red_on_update(state->trace_queue, now, state->max_p);
  }
}

//----------------------------------------------------------------------------------------------------------
void ctrl_ared_init ( ared_state_t* state, ared_params_t* params, tsc_cycles_t timer_freq, const qelem_ared_params_t* qparams, void* trace_queue )
{

  // calculate pkt_rate
  if ( qparams->link_rate == 0 || qparams->avg_pkt_size == 0 )
  {
    rte_panic ( "cannot init ARED: please set link rate and avg_pkt_size" );
  }
  
  double typ_pkt_rate_s = qparams->link_rate / ( qparams->avg_pkt_size * 8 ); // C / (MTU * 8)  // pkts/s
  LOG_PARAM ( typ_pkt_rate_s, "%f" );


  // if params->min_thresh != 0 - calculate min_th as in paper.
  if ( qparams->min_thresh == 0 )
  {
    if ( qparams->target_delay == 0 )
    {
      rte_panic ( "cannot init ARED: both qparams->target_delay and qparams->min_thresh are zero .. set one" );
    }

    // min_th = target_delay * C (pkts) / 2
    params->min_thresh = qparams->target_delay * typ_pkt_rate_s / 2;  // set to target_delay * C (pkts) / 2
    if ( params->min_thresh < 5 ) params->min_thresh = 5;
  }
  else
  {
    params->min_thresh = qparams->min_thresh;
  }

  // max_th is 3 * min_th unless specified
  params->max_thresh = ( qparams->min_thresh > 0 ) ? qparams->min_thresh : params->min_thresh * 3;

  LOG_PARAM ( params->min_thresh, "%f" );
  LOG_PARAM ( params->max_thresh, "%f" );

  // calculate w_q
  if ( qparams->w_q == 0 )
  {
    // w_q = 1 - exp (-1 /c)
    params->w_q = 1.0 - exp ( -1.0 / typ_pkt_rate_s );
  }
  else
  {
    params->w_q =  qparams->w_q;
  }

  LOG_PARAM ( params->w_q, "%f" );

  
  // ADAPTIVE part

  params->update_interval = ( qparams->update_interval_s ?  qparams->update_interval_s  : 0.5 ) * timer_freq;

  params->target_min = 	params->min_thresh  + 0.4 * ( params->max_thresh - params->min_thresh );
  params->target_max = 	params->min_thresh  + 0.6 * ( params->max_thresh - params->min_thresh );

  params->alpha_min = ( qparams->alpha ?  qparams->alpha : 0.01 );
  params->beta = ( qparams->beta ?  qparams->beta : 0.9 );

  params->typ_pkt_rate = typ_pkt_rate_s / timer_freq;

  fast_srand ( &state->rand_state, timer_freq );

  // assume that state is zalloced
  state->max_p = 0.000001;
  state->C0 = state->max_p / ( params->max_thresh - params->min_thresh );

#ifdef GENTLE  
  // for gentle RED
  state->G0 = ( 1.0 - state->max_p ) / params->max_thresh;
  
  LOG_PARAM("gentle mode", "%s");
#endif  
  
#ifdef WITH_TRACES
  state->trace_queue = trace_queue;
#endif  
  
}

void ctrl_ared_update_if_needed ( ared_state_t* state, const ared_params_t* params, tsc_cycles_t now)
{
  if ( now - state->last_recalc > params->update_interval )
  {
//     LOG_LINE();
    __ared_update_maxp ( state, params, now );
    state->last_recalc = now;
  }
}

boolean_t ctrl_ared_on_pkt_enqueue ( ared_state_t* state, const ared_params_t* params, tsc_cycles_t now, unsigned int qlen_pkts )
{
  return __ared_should_drop ( state, params, now, qlen_pkts );
}

void ctrl_ared_on_pkt_dequeue ( ared_state_t* state, const ared_params_t* params , tsc_cycles_t now, unsigned int qlen_pkts )
{
#if defined WITH_TRACES && WITH_TRACES == 2
  red_on_pkts_dequeue(state->trace_queue, now, qlen_pkts);
#endif
  
  if ( qlen_pkts == 0 )
    state->empty_since = now;
}



#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
