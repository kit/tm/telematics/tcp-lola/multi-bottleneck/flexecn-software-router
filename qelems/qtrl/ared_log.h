#ifndef	__QTRL_ARED_LOG__
#define	__QTRL_ARED_LOG__

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

#define QELEM_TRACE

#ifdef QELEM_TRACE
#include "../../config/log_macros.h"

#define ARED_LOG_HEADER() \
    do { LOG("#%s,%s,%s,%s,%s,%s\n", "now","qlen_pkts", "avg(Q16.16)", "count" ,"dropped", "max_p(Q0.32)"); } while(0)

      
#ifdef CALC_FLOAT      
#define ARED_LOG_ON_ENQ(now, qlen_pkts, avg, count, dropped, max_p) \
    do { /*if(dropped)*/ LOG("%lu,%u,%f,%d,%u,%f \n", now, qlen_pkts, avg, count, dropped, max_p);} while(0);
#else
#define ARED_LOG_ON_ENQ(now, qlen_pkts, avg, count, dropped, max_p) \
    do { /*if(dropped)*/ LOG("%lu,%u,%u,%d,%u,%u \n", now, qlen_pkts, avg, count, dropped, max_p);} while(0);
#endif
    
#else

#define ARED_LOG_HEADER()
#define ARED_LOG_ON_ENQ(now, qlen_pkts, avg, count, dropped, max_p)

#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif

