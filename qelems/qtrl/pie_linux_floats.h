#pragma message "TODO"

#ifndef	__QTRL_PIE__
#define	__QTRL_PIE__

#include "../qelem_types.h"
#include "../traces/pie_event.h"
#include <rte_random.h>
#include <rte_cycles.h>

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

/**
 * @FILE
 *
 * pie controller - basic version from the paper
 */

typedef struct
{
  // configurable parameters
  double REFERENCE_DELAY;
  tsc_cycles_t MAX_BURST_TIME;

  // possibly configurable parameters
  // for periodic updates
  tsc_cycles_t t_update_tsc;
  double alpha,beta;
  // for depature rate estimation
  unsigned qlen_thresh; // min queue len to estimate
  double epsilon;

  unsigned mtu;

} pie_params_t;

typedef struct
{
  // variables for drop probability calc.
  double p;
  tsc_cycles_t burst_allow_time;

  // variables for periodic updates
  tsc_cycles_t last_update;
  double avg_drain_rate;
  double old_delay;

  // variables for depature rate estimation
  int drained_bytes;
  tsc_cycles_t last_measurement;

#ifdef WITH_TRACES
  event_log_queue_t* trace_queue;
#endif
} pie_state_t;

//---------------------------------------------------------------------------------------------------
static inline void
ctrl_pie_init ( pie_state_t* state, pie_params_t* params,  tsc_cycles_t timer_freq,
                seconds_t ref_delay, seconds_t t_update, double alpha, double beta,
                void* trace_queue );

static inline boolean_t
ctrl_pie_on_pkt_enqueue ( pie_state_t* state, const pie_params_t* params, unsigned int qlen_bytes );

static inline void
ctrl_pie_on_pkt_dequeue ( pie_state_t* state, const pie_params_t* params, tsc_cycles_t now, unsigned qlen_bytes, unsigned pkt_size );

static inline void
ctrl_pie_update_if_needed ( pie_state_t* state, const pie_params_t* params, tsc_cycles_t now, unsigned qlen_bytes );

//---------------------------------------------------------------------------------------------------
#define DRAINED_BYTES_INVALID -1
#define PIE_IN_MEASUREMENT(pie) (pie->drained_bytes != DRAINED_BYTES_INVALID)

static inline boolean_t
__pie_should_drop ( pie_state_t* state, const pie_params_t* params, unsigned int qlen_bytes )
{
  // allow bursts for some time
  if ( state->burst_allow_time > 0 )
  {
    return FALSE;
  }

  // following code is pie on/off from sample code in draft_pie
  if ( ( state->old_delay < params->REFERENCE_DELAY / 2 ) && ( state->p < 0.2 ) )
  {
    return FALSE;
  }

  if ( qlen_bytes < 2 * params->mtu )
  {
    return FALSE;
  }

  // TODO in bytemode p = p * (packet_size / mtu)

  // TODO in the draft there is another probability modus - with 0.085 -> 0.85
  // TODO understand probability in RED

  // drop packet with probability p
  double rand = ( ( float ) rte_rand() ) / ( ( float ) ~0lu );
  double p_local = state->p;

  return rand <= p_local;
}

static void
__pie_update_drop_probability ( pie_state_t* state, const pie_params_t* params, unsigned qlen_bytes, tsc_cycles_t now )
{
  double curr_delay;

  if ( state->avg_drain_rate != 0 )
  {
    curr_delay = ( ( double ) qlen_bytes ) / state->avg_drain_rate;
  }
  else
  {
    curr_delay = 0;
  }

  double alpha = params->alpha;
  double beta = params->beta;
  double p = state->p;
  double new_p;

  // scale factors
  if ( p < 0.001 )
  {
    alpha /= 128;
    beta /= 128;
  }
  else if ( p < 0.01 )
  {
    alpha /=8;
    beta /=8;
  }
  else if ( p < 0.1 )
  {
    alpha /=2;
    beta /=2;
  }

  double delta_p = alpha * ( curr_delay - params->REFERENCE_DELAY ) + beta * ( curr_delay - state->old_delay );
  // LINUX: ensure that p is updated in steps nomore than 2%
  // if delta_p > 2% and p > 10% set delta to 2%
  if ( delta_p > 0.02 && p > 0.1 )
  {
    delta_p = 0.02;
  }


  // LINUX:
  /* Non-linear drop:
   * Tune drop probability to increase quickly for high delays(>= 250ms)
   * 250ms is derived through experiments and provides error protection
   */

// 	if (qdelay > 250ms)
// 		delta += MAX_PROB / (100 / 2); // TODO what is this number%



  // update p
  new_p = p + delta_p;

  if ( new_p < 0 )
  {
    new_p = 0;
  }
  if ( new_p > 1 )
  {
    new_p = 1;
  }

  state->p = new_p;

  /* Non-linear drop in probability: Reduce drop probability quickly if
   * delay is 0 for 2 consecutive Tupdate periods.
  if ((qdelay == 0) && (qdelay_old == 0) && !!!update_prob!!!)
  	vars->prob = (vars->prob * 98) / 100; */
  if ( ( curr_delay == 0 ) && ( state->old_delay == 0 ) && ( qlen_bytes == 0 ) )
  {
    state->p = state->p * 98 / 100 ;
  }

  // update burst allow:
  if ( state->p == 0 && curr_delay <= params->REFERENCE_DELAY / 2 && state->old_delay <= params->REFERENCE_DELAY / 2 && PIE_IN_MEASUREMENT ( state ) )
  {
//     LOG_LINE();
    state->burst_allow_time = params->MAX_BURST_TIME;
    state->drained_bytes = DRAINED_BYTES_INVALID;
    state->avg_drain_rate = 0;
    pie_on_exit_measurement ( state->trace_queue, now, state->burst_allow_time );
  }

  state->old_delay = curr_delay;

  pie_on_update ( state->trace_queue, now, qlen_bytes, curr_delay, state->p, state->burst_allow_time );
}

static void
__pie_calculate_depature_rate ( pie_state_t* state, const pie_params_t* params, tsc_cycles_t now, unsigned qlen_bytes, unsigned pkt_size )
{
  // if queue is big enough and measurement is turned off -> turn measurement on
  if ( qlen_bytes > params->qlen_thresh * 1522   && !PIE_IN_MEASUREMENT ( state ) )
  {
    state->drained_bytes = 0;
    state->last_measurement = now;
    pie_on_enter_measurement ( state->trace_queue, now, state->burst_allow_time );
  }

  if ( PIE_IN_MEASUREMENT ( state ) )
  {
    state->drained_bytes += pkt_size;

    if ( state->drained_bytes >= ( int ) params->qlen_thresh * 1522 )
    {
      tsc_cycles_t drain_interval = now - state->last_measurement;
      double drain_rate = ( ( double ) state->drained_bytes ) / ( double ) drain_interval;

      if ( state->avg_drain_rate == 0 )
      {
        state->avg_drain_rate = drain_rate;
      }
      else
      {
        state->avg_drain_rate = ( 1 - params->epsilon ) * state->avg_drain_rate + params->epsilon * drain_rate;
      }

      // if qlen is too small stop measuring
      // else reset measurement cycle
      if ( qlen_bytes < params->qlen_thresh )
      {
        state->drained_bytes = DRAINED_BYTES_INVALID;
        pie_on_exit_measurement ( state->trace_queue, now, state->burst_allow_time );
      }
      else
      {
        state->last_measurement = now;
        state->drained_bytes = 0;
      }

      // update burst allowance
      if ( state->burst_allow_time != 0 && state->burst_allow_time >= drain_interval )
      {
        state->burst_allow_time -= drain_interval;
      }
      else
      {
        state->burst_allow_time = 0;
      }

      pie_on_new_drain_rate_estimation ( state->trace_queue, now, drain_rate, state->avg_drain_rate, state->burst_allow_time );
    }
  }
}

//---------------------------------------------------------------------------------------------------

void ctrl_pie_init ( pie_state_t* state, pie_params_t* params,  tsc_cycles_t timer_freq,
                     seconds_t ref_delay, seconds_t t_update, double alpha, double beta,
                     void* trace_queue )
{
  // TODO:
  params->REFERENCE_DELAY = ref_delay  * timer_freq;
  params->MAX_BURST_TIME = 100e-3  * timer_freq; // this is hard-coded in linux too

  params->t_update_tsc = t_update * timer_freq;

  params->alpha = alpha / timer_freq;
  params->beta = beta / timer_freq;
  params->epsilon = 0.125; // this is hard-coded

  params->qlen_thresh = 10; // it should be about 10 packets, or something else in byte mode
  params->mtu = 1522; // this we can hardcode ...

  state->drained_bytes = DRAINED_BYTES_INVALID;
  state->avg_drain_rate = 0;
  state->burst_allow_time = params->MAX_BURST_TIME;

#ifdef WITH_TRACES
  state->trace_queue = trace_queue;
#endif
}

void ctrl_pie_update_if_needed ( pie_state_t* state, const pie_params_t* params, tsc_cycles_t now, unsigned int qlen_bytes )
{
  if ( now - state->last_update > params->t_update_tsc )
  {
    __pie_update_drop_probability ( state, params, qlen_bytes,now );
    state->last_update = now;
  }
}

boolean_t ctrl_pie_on_pkt_enqueue ( pie_state_t* state, const pie_params_t* params, unsigned int qlen_bytes )
{
  return __pie_should_drop ( state, params, qlen_bytes );
}

void ctrl_pie_on_pkt_dequeue ( pie_state_t* state, const pie_params_t* params, tsc_cycles_t now, unsigned qlen_bytes, unsigned pkt_size )
{
  __pie_calculate_depature_rate ( state,params, now, qlen_bytes, pkt_size );
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
