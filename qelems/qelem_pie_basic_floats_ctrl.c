#include "qelem.h"
#include "../rte_include.h"
#include "qelem_pie.h"
#include "../config/log_macros.h"
#include "qelem_types.h"
#include "qdt/ppqueue.h"
#include "qtrl/pie_basic_floats.h"


static qelem_ops_t qelem_pie_ops;

typedef struct __qelem_pie
{
  qelem_t qelem;

  pie_params_t pie_params;
  pie_state_t pie_state;

  ppqueue_t queue;

  qelem_stats_t stats;

} qelem_pie_t __rte_cache_aligned;


static
qelem_t* qelem_pie_alloc (const  qelem_params_t* common_params,const  void* qelem_params )
{
  const qelem_pie_params_t* params = (const  qelem_pie_params_t* ) qelem_params;
  qelem_pie_t* qelem = rte_zmalloc ( "qelem_pie", sizeof ( qelem_pie_t ), RTE_CACHE_LINE_SIZE );

  qelem->qelem.v_table = &qelem_pie_ops;
  qelem->qelem.port_id = common_params->port_id;
//   qelem->tailq_number = common_params->mq_queues_count;

  ctrl_pie_init ( &qelem->pie_state, &qelem->pie_params, rte_get_tsc_hz() );
  if ( !ppq_init ( &qelem->queue, 4096 ) )
  {
    rte_panic ( "failed to allocate queue for qelem_codel_lowspeed \n" );
  }

  return ( qelem_t* ) qelem;
};

static
void qelem_pie_free ( qelem_t* qelem )
{
  qelem_pie_t* pie = ( qelem_pie_t* ) qelem;

  ppq_destroy ( &pie->queue );
  rte_free ( pie );
};

static
unsigned qelem_pie_enqueue_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts , unsigned hw_queue_index )
{
  unsigned nb_enc, pkt_num, enq_pkt_num;
  qelem_pie_t* pie = ( qelem_pie_t* ) qelem;

  for ( pkt_num = 0, nb_enc=0; pkt_num < num_pkts; pkt_num++ )
  {
    struct rte_mbuf* pkt = pkts[pkt_num];

    // if there is no space we can't do anything :)
    if ( ppq_free_count ( &pie->queue ) == 0 )
    {
      QELEM_TAIL_DROP ( pie );
      rte_pktmbuf_free ( pkt );
    }

    boolean_t should_drop = ctrl_pie_on_pkt_enqueue ( &pie->pie_state, &pie->pie_params, ppq_len(&pie->queue) );
    if ( should_drop )
    {
      QELEM_AQM_DROP ( pie );
      rte_pktmbuf_free ( pkt );
    }
    else
    {
      ppq_enqueue ( &pie->queue, pkt );
      nb_enc++;
    }
  }

  return nb_enc;
};

static
void qelem_pie_xmit ( qelem_t *_qelem, unsigned hw_queue_index )
{
  qelem_pie_t* pie = ( qelem_pie_t* ) _qelem;

  tsc_cycles_t now = rte_get_tsc_cycles();
  ctrl_pie_update_if_needed(&pie->pie_state, &pie->pie_params, now, ppq_len ( &pie->queue ));

  struct rte_mbuf* pkt = ppq_peek ( &pie->queue );
  
  if ( pkt )
  {
    unsigned pktlen = pkt->data_len;
    boolean_t sent = ( rte_eth_tx_burst ( pie->qelem.port_id, hw_queue_index, &pkt, 1 ) == 1 );
    if ( sent )
    {
      ppq_dequeue_peeked ( &pie->queue );
      ctrl_pie_on_pkt_dequeue ( &pie->pie_state, &pie->pie_params, now, ppq_len ( &pie->queue ) , pktlen );
    }
  }

}

qelem_factory_ops_t qelem_pie_factory =
{
  .qelem_alloc = qelem_pie_alloc,
  .qelem_free = qelem_pie_free,
};

static qelem_ops_t qelem_pie_ops =
{
  .enqueue_burst = qelem_pie_enqueue_burst,
  .xmit = qelem_pie_xmit,
};

