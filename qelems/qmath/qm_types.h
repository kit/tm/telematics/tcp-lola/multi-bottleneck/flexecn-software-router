#ifndef	__QM_TYPES__
#define	__QM_TYPES__

#include <stdint.h>

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

typedef uint32_t Q_0_32; // fixed-point 0.xxx number
typedef uint32_t Q_16_16; // fixed-point 16.16 number

static inline
Q_0_32 q_0_32_mul ( Q_0_32 a, Q_0_32 b )
{
  return (uint32_t)(((uint64_t) a * b) >> 32);
}

static inline
Q_0_32 q_16_16_mul ( Q_16_16 a, Q_16_16 b )
{
  return (uint32_t)(((uint64_t) a * b) >> 16);
}

static inline
uint32_t int_q_0_32_mul ( uint32_t a, Q_0_32 b )
{
  // multiply int by Q0.32 and then approx result 
  // to before the comma
  // uint * Q0.32 is a 64 bit integer with 32 bits after the comma
  // shift cuts the bits after the comma
  return (uint32_t)(((uint64_t) a * b) >> 32);
}

static inline
Q_16_16 q_16_16_from_uint(unsigned a)
{
  // no overflow protection
  return a << 16;
}

static inline
Q_16_16 q_16_16_from_double(double a)
{
  // multiply double by 2^16 and then convert to uint
  return (uint32_t)(a * (1llu << 16));
}

static inline
Q_16_16 q_0_32_from_double(double a)
{
  // multiply double by 2^16 and then convert to uint
  return (uint32_t)(a * (1llu << 32));
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
