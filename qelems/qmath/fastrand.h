#ifndef	__QM_FAST_RAND__
#define	__QM_FAST_RAND__

#include <stdint.h>

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

// simple linear congruent RNG
// copied from the first LCG in http://en.wikipedia.org/wiki/Linear_congruential_generator
  
  
typedef struct
{
  unsigned int g_seed;
} rand_state_t;

static inline void fast_srand ( rand_state_t* rand_state, int seed )
{
  rand_state->g_seed = seed;
}

static inline unsigned fast_rand ( rand_state_t* rand_state )
{
  rand_state->g_seed = ( 1664525lu*rand_state->g_seed+1013904223 ); 
  return rand_state->g_seed;
}


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
