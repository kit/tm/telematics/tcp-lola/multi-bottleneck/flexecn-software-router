#include "qelem.h"
#include "qelem_fq_codel.h"
#include "qelem_types.h"
#include <sys/queue.h>
#include "../rte_include.h"
#include "../config/log_macros.h"
#include "../datapath/packet_metadata.h"

#define BURST_SIZE 32
#define MAX_TAILQ_NUMBER 4

static qelem_ops_t qelem_fq_codel_ops;

//-------------------------------------------------------------------------------------------------------------------------

TAILQ_HEAD ( subq_pkt_head, __fq_codel_metadata );

typedef struct __subqueue {
    // entry for linked list of new/old queues
    TAILQ_ENTRY ( __subqueue ) queues_list_entry;

    // head of the pkt queue
    struct subq_pkt_head subq_pkt_head;

    // queue state variables
    boolean_t is_in_list_of_new_queues;
    boolean_t is_in_list_of_old_queues;
    unsigned bytes_enq;
    int deficit;

    // codel state variables
    // estimator
    tsc_cycles_t persisten_queue_since;

    // controller
    boolean_t is_in_dropping_state;
    tsc_cycles_t drop_next;
    unsigned pkts_dropped;

} subqueue_t;

TAILQ_HEAD ( queues_list, __subqueue );

typedef struct {
    qelem_t qelem;

    // parameters
    tsc_cycles_t INTERVAL;
    tsc_cycles_t TARGET;

    struct queues_list new_queues;
    struct queues_list old_queues;

    unsigned pkt_limit;
    unsigned curr_pkt_counter;

    unsigned subq_num;
    subqueue_t* subqueues[0];

} qelem_fq_codel_t ;
#define QUANTUM 1514


typedef struct __fq_codel_metadata {
    uint64_t timestamp;
    TAILQ_ENTRY ( __fq_codel_metadata ) pkt_queue_entry;
    struct rte_mbuf* parent_pkt;

} fq_codel_metadata_t;

//-------------------------------------------------------------------------------------------------------------------------

static
qelem_t* qelem_fq_codel_alloc ( qelem_params_t* common_params, void* qelem_params )
{
    qelem_fq_codel_params_t* params = ( qelem_fq_codel_params_t* ) qelem_params;
    qelem_fq_codel_t* qelem = rte_zmalloc ( "qelem_fq_codel", sizeof ( qelem_fq_codel_t ) + params->num_subqueues * sizeof ( subqueue_t* ), RTE_CACHE_LINE_SIZE );
    subqueue_t* subqueues = rte_zmalloc ( "qelem_fq_codel_subq", params->num_subqueues * sizeof ( subqueue_t ), RTE_CACHE_LINE_SIZE );

    qelem->qelem.v_table = &qelem_fq_codel_ops;
    qelem->qelem.port_id = common_params->port_id;
    qelem->subq_num = params->num_subqueues;

    TAILQ_INIT ( &qelem->new_queues );
    TAILQ_INIT ( &qelem->old_queues );

    subqueue_t** subqueues_ptrs = qelem->subqueues;
    unsigned qnum;
    for ( qnum = 0; qnum < qelem->subq_num; qnum++ ) {
        subqueues_ptrs[qnum] = &subqueues[qnum];
        TAILQ_INIT ( & subqueues[qnum].subq_pkt_head );
    }

    qelem->pkt_limit = params->pkt_hard_limit;
    LOG_VAR ( qelem->pkt_limit, "%u" );
    LOG_VAR ( qelem->curr_pkt_counter, "%u" );

    double tsc_freq = rte_get_tsc_hz();
//   double timer_freq = rte_get_timer_hz();
    double interval_tsc = ( ( double ) 100 ) * 1e-6 * tsc_freq;
    double target_tsc = ( ( double ) 5 ) * 1e-6 * tsc_freq;

    qelem->INTERVAL = interval_tsc;
    qelem->TARGET  = target_tsc;

    return ( qelem_t* ) qelem;
};

static
void qelem_fq_codel_free ( qelem_t* qelem )
{
    qelem_fq_codel_t* qelem0 = ( qelem_fq_codel_t* ) qelem;
    rte_free ( qelem0->subqueues );
    rte_free ( qelem0 );
};

//-------------------------------------------------------------------------------------------------------------------------

static
unsigned qelem_fq_codel_enqueue_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts , unsigned hw_queue_index )
{
    qelem_fq_codel_t* fq_codel = ( qelem_fq_codel_t* ) qelem;

    unsigned  pkt_num;
    tsc_cycles_t now = rte_get_tsc_cycles();

    for ( pkt_num = 0; pkt_num < num_pkts; pkt_num++ ) {
        // read hash value from packet metadata
        packet_metadata_t* metadata = ( packet_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkts[pkt_num], 0 );
        uint64_t flow_key_sig = metadata->flow_key_sig;
        uint64_t subq_num = flow_key_sig % fq_codel->subq_num;
        subqueue_t* subq = fq_codel->subqueues[subq_num];

        // from this point we don't need packet metadata, replace it
        fq_codel_metadata_t* fq_codel_entry = ( fq_codel_metadata_t* ) metadata;
        fq_codel_entry->timestamp = now;
        fq_codel_entry->parent_pkt = pkts[pkt_num];


        TAILQ_INSERT_TAIL ( &subq->subq_pkt_head, fq_codel_entry, pkt_queue_entry );
        subq->bytes_enq += pkts[pkt_num]->data_len;

        if ( !subq->is_in_list_of_new_queues && !subq->is_in_list_of_old_queues ) {
            // if subq is in neither of the lists -> add it to the new subq list
            subq->deficit = QUANTUM;
            subq->is_in_list_of_new_queues = TRUE;
            TAILQ_INSERT_TAIL ( &fq_codel->new_queues, subq, queues_list_entry );
        } else if ( subq->is_in_list_of_new_queues ) {
            // if subq was in the list of new queues, remove it from the list
            // of new queues and add to the list of old queues
            subq->is_in_list_of_new_queues = FALSE;
            subq->is_in_list_of_old_queues = TRUE;
            TAILQ_REMOVE ( &fq_codel->new_queues, subq, queues_list_entry );
            TAILQ_INSERT_TAIL ( &fq_codel->old_queues, subq, queues_list_entry );
        } else {
            // subq is in the list of old queues -> reinsert it at the tail
            TAILQ_REMOVE ( &fq_codel->old_queues, subq, queues_list_entry );
            TAILQ_INSERT_TAIL ( &fq_codel->old_queues, subq, queues_list_entry );
        }

        fq_codel->curr_pkt_counter++;

        if ( fq_codel->curr_pkt_counter >= fq_codel->pkt_limit ) {
            subqueue_t* iterq,*victimq = 0;
            unsigned max = 0;
            TAILQ_FOREACH ( iterq, &fq_codel->new_queues, queues_list_entry ) {
                if ( iterq->bytes_enq > max ) {
                    victimq = iterq;
                    max = iterq->bytes_enq;
                }
            }
            TAILQ_FOREACH ( iterq, &fq_codel->old_queues, queues_list_entry ) {
                if ( iterq->bytes_enq > max ) {
                    victimq = iterq;
                    max = iterq->bytes_enq;
                }
            }

            // drop packet from the queue
            fq_codel_metadata_t* pktm = TAILQ_FIRST ( &victimq->subq_pkt_head );
            TAILQ_REMOVE ( &victimq->subq_pkt_head , pktm, pkt_queue_entry );
            victimq->bytes_enq-=pktm->parent_pkt->data_len;
            rte_pktmbuf_free ( pktm->parent_pkt );
        }
    }

    return pkt_num;
};

static subqueue_t* fq_codel_select_subq ( qelem_fq_codel_t* fq_codel, boolean_t* is_new_queue )
{
    subqueue_t* subq = 0;

    // traverse list of new queues
    TAILQ_FOREACH ( subq, &fq_codel->new_queues, queues_list_entry ) {
        if ( subq->deficit < 0 ) {
            subq->deficit += QUANTUM;
            subq->is_in_list_of_new_queues = FALSE;
            subq->is_in_list_of_old_queues = TRUE;
            TAILQ_REMOVE ( &fq_codel->new_queues, subq, queues_list_entry );
            TAILQ_INSERT_TAIL ( &fq_codel->old_queues, subq, queues_list_entry );
        } else {
            *is_new_queue=TRUE;
            return subq;
        }
    }

    // traverse list of old queues
    TAILQ_FOREACH ( subq, &fq_codel->old_queues, queues_list_entry ) {
        if ( subq->deficit < 0 ) {
            subq->deficit += QUANTUM;
            TAILQ_REMOVE ( &fq_codel->old_queues, subq, queues_list_entry );
            TAILQ_INSERT_TAIL ( &fq_codel->old_queues, subq, queues_list_entry );
        } else {
            *is_new_queue=FALSE;
            return subq;
        }
    }

    return 0;
}

#include <math.h>

static uint64_t fq_codel_control_law ( qelem_fq_codel_t* fq_codel, subqueue_t* codel, tsc_cycles_t time_tsc )
{
    return time_tsc + fq_codel->INTERVAL / sqrt ( codel->pkts_dropped );
}

static struct rte_mbuf* fq_codel_run_codel_on_queue ( qelem_fq_codel_t* fq_codel, subqueue_t* subq )
{
#define MTU 1518

    while ( 1 ) {
        // dequeue packet
        struct rte_mbuf* pkt;
        fq_codel_metadata_t* metadata;
        boolean_t was_dequeued = TRUE;
        boolean_t is_in_persistent_queue_state = FALSE;

        if ( TAILQ_EMPTY ( &subq->subq_pkt_head ) ) {
            was_dequeued = FALSE;
        } else {
            metadata = TAILQ_FIRST ( &subq->subq_pkt_head );
            TAILQ_REMOVE ( &subq->subq_pkt_head, metadata, pkt_queue_entry );
            pkt = metadata->parent_pkt;
            subq->bytes_enq -= metadata->parent_pkt->data_len;
            fq_codel->curr_pkt_counter--;
        }

        //
        // first we run estimator and decide whether we are in "persisten_queue_state"
        // we are in "persisten_queue_state" if sojourn_time is above the target for at
        // least an interval
        //

        if ( was_dequeued == FALSE ) {
            subq->persisten_queue_since = 0;
            subq->is_in_dropping_state = FALSE;
            return 0;
        }

        tsc_cycles_t now = rte_get_tsc_cycles();

        // calculate packet sojourn time

        tsc_cycles_t sojourn_time = now - metadata->timestamp;

        // is sojourn_time above the target
        // and there are at least MTU bytes in queue
        if ( sojourn_time > fq_codel->TARGET && subq->bytes_enq > MTU ) {
            if ( subq->persisten_queue_since == 0 ) {
                // it there was no persistent queue... indicate that there is
                subq->persisten_queue_since = now + fq_codel->INTERVAL;
            } else {
                // if there was persistent queue ...
                if ( now > subq->persisten_queue_since ) {
                    // we are in persistent_queue_state
                    is_in_persistent_queue_state = TRUE;
                }
            }
        } else {
            // if we are in persisten_queue_state, exit it
            is_in_persistent_queue_state = FALSE; // i hope gcc is smart enough not to execute this statement :)
// 	    subq->persisten_queue_since = 0;
// 	    subq->is_in_dropping_state = FALSE;
            // and return dequeued packet
//             return pkt;
        }

        //
        // run controller
        // if we are in persisten_queue_state then controller should take action
        // if we are not in persisten_queue_state controller needs to be reset
        //

        if ( is_in_persistent_queue_state ) {
            // if we were not in dropping state, enter dropping state
            // also make opt. when dropping state is entered/exited too often
            if ( !subq->is_in_dropping_state ) {
                subq->is_in_dropping_state = TRUE;
                subq->pkts_dropped = ( subq->pkts_dropped >= 2 ) && ( ( now - subq->drop_next ) < 8 * fq_codel->INTERVAL )
                                     ? subq->pkts_dropped - 2 : 0;
            }

            // we entered dropping state
            // was there a packet that was dropped in the current dropping interval?
            if ( now >= subq->drop_next ) {
                subq->bytes_enq -= pkt->data_len;
                rte_pktmbuf_free ( pkt );
                subq->pkts_dropped++;
                subq->drop_next = fq_codel_control_law ( fq_codel, subq, subq->drop_next );
                continue;
            } else {
                return pkt;
            }
        } else {
            subq->persisten_queue_since = 0;
            subq->is_in_dropping_state = FALSE;
            return pkt;
        }
    }
}

static struct rte_mbuf* fq_codel_dequeue_one ( qelem_fq_codel_t* fq_codel )
{
    while ( 1 ) {
        boolean_t is_new_queue = 0;
        subqueue_t* subq = fq_codel_select_subq ( fq_codel, &is_new_queue );
        if ( !subq ) {
            return 0;
        }

        // apply codel algorithm to the subq
        struct rte_mbuf* pkt = fq_codel_run_codel_on_queue ( fq_codel, subq );
        if ( pkt ) {
            subq->deficit -= pkt->data_len;
            return pkt;
        }

        // if queue is empty
        if ( is_new_queue ) {
            subq->is_in_list_of_new_queues = FALSE;
            subq->is_in_list_of_old_queues = TRUE;
            TAILQ_REMOVE ( &fq_codel->new_queues, subq, queues_list_entry );
            TAILQ_INSERT_TAIL ( &fq_codel->old_queues, subq, queues_list_entry );
        } else {
            // remove queue from the list of active queues
            subq->is_in_list_of_new_queues = FALSE;
            subq->is_in_list_of_old_queues = FALSE;
            TAILQ_REMOVE ( &fq_codel->old_queues, subq, queues_list_entry );
        }
    }
}

static
void qelem_fq_codel_xmit ( qelem_t *_qelem, unsigned hw_queue_index )
{
    qelem_fq_codel_t* fq_codel = ( qelem_fq_codel_t* ) _qelem;


    struct rte_mbuf* burst[BURST_SIZE];
    unsigned  burst_counter;

    unsigned  nb_tx ;
    struct rte_mbuf** burst1;


    // run codel BURST_SIZE times:
    for ( burst_counter = 0; burst_counter < BURST_SIZE; burst_counter++ ) {
        struct rte_mbuf* pkt = fq_codel_dequeue_one ( fq_codel );
        if ( pkt == 0 ) {
            break;
        }

        burst[burst_counter] = pkt;
    }

    //now send the burst
    if ( burst_counter == 0 ) {
        return;
    };

    burst1 = &burst[0];

    do {
        nb_tx = rte_eth_tx_burst ( fq_codel->qelem.port_id, hw_queue_index, burst1, burst_counter );
        burst_counter-=nb_tx;
        burst1 = &burst1[nb_tx];
    } while ( burst_counter != 0 );
}

qelem_factory_ops_t qelem_fq_codel_factory = {
    .qelem_alloc = qelem_fq_codel_alloc,
    .qelem_free = qelem_fq_codel_free,
};

static qelem_ops_t qelem_fq_codel_ops = {
    .enqueue_burst = qelem_fq_codel_enqueue_burst,
    .xmit = qelem_fq_codel_xmit,
};


