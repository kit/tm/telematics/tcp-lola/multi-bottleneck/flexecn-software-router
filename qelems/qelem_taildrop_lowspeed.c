#include "qelems.h"
#include "../rte_include.h"
#include "../config/log_macros.h"
#include "macros/lowspeed.h"
#include "qdt/ppqueue.h"


typedef struct __qelem_taildrop
{
  qelem_t qelem;
  unsigned byte_limit;
  ppqueue_t queue;
} qelem_taildrop_t __rte_cache_aligned;

static qelem_ops_t qelem_taildrop_lowspeed_ops;

static
qelem_t* qelem_taildrop_alloc ( const  qelem_params_t* common_params, const void* qelem_params )
{
  LOG_INIT ( "allocating qelem_taildrop for port %d \n", common_params->port_id );

  qelem_taildrop_t* taildrop = 0;
  QPARAMSCAST ( taildrop );
  QELEM_INIT0 ( taildrop );

  if ( params->byte_limit > 0 && params->pkt_limit > 0 )
  {
    rte_panic ( "qelem_taildrop: please set either pkt_limit or byte_limit, not both\n" );
  }

  unsigned qlen = 0;
  if ( params->pkt_limit > 0 )
  {
    qlen = params->pkt_limit;
    taildrop->byte_limit = ~0l;
  }
  else
  {
    qlen = params->byte_limit / 64 ; // in min-sized packets
    qlen = 1 << ( 32 - __builtin_clz ( qlen - 1 ) ); // rounded to the next power of two
    taildrop->byte_limit = params->byte_limit;
  }

  PPQ_INIT ( taildrop, qlen );

  return ( qelem_t* ) taildrop;
};


static
unsigned qelem_taildrop_enqueue_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts , unsigned hw_queue_index )
{
  QCAST ( taildrop );

  unsigned num_enq;
  for ( num_enq = 0; num_enq < num_pkts; num_enq++ )
  {
    struct rte_mbuf* pkt = pkts[num_enq];

    if (taildrop->byte_limit - ppq_len ( &taildrop->queue ) < pkt->pkt_len )
    {
      // if byte_limit is zero then (0 - unsigned) will be a very big number
      break;
    }

    if ( !ppq_enqueue ( &taildrop->queue, pkt ) )
    {
      break;
    }
  }

  unsigned pkt_to_drop;
  if ( unlikely ( num_enq < num_pkts ) )
  {
    for ( pkt_to_drop = num_enq ; pkt_to_drop < num_pkts; pkt_to_drop++ )
    {
      rte_pktmbuf_free ( pkts[pkt_to_drop] );
    };
    QELEM_TAIL_DROP ( taildrop , num_pkts - num_enq );
  }

  return num_enq;

};

static
void qelem_taildrop_xmit ( qelem_t *_qelem, unsigned hw_queue_index )
{
  qelem_taildrop_t* qelem = ( qelem_taildrop_t* ) _qelem;

  struct rte_mbuf* pkt = ppq_peek ( &qelem->queue );

  if ( !pkt )
  {
    return;
  }
  
  // try to send packet
  boolean_t sent = ( rte_eth_tx_burst ( qelem->qelem.port_id, hw_queue_index, &pkt, 1 ) == 1 );
  if ( sent )
  {
    ppq_dequeue_peeked ( &qelem->queue );
  }
}

static
void qelem_taildrop_free ( qelem_t* qelem )
{
  qelem_taildrop_t* qelem0 = ( qelem_taildrop_t* ) qelem;

  ppq_destroy ( &qelem0->queue );
  rte_free ( qelem0 );
};

qelem_factory_ops_t qelem_taildrop_lowspeed_factory =
{
  .qelem_alloc = qelem_taildrop_alloc,
};

static qelem_ops_t qelem_taildrop_lowspeed_ops =
{
  .enqueue_burst = qelem_taildrop_enqueue_burst,
  .xmit = qelem_taildrop_xmit,
  .qelem_free = qelem_taildrop_free,
};

