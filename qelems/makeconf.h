#ifndef	__QELEMS__MAKECONF__
#define	__QELEMS__MAKECONF__

/**
 * @file
 * 
 * a summary of all compile-time macros
 */

// traces: (selected in Makefile)
// #define WITH_TRACES 2

// taildrop: -----------------------------------------------------------------------------------------------

#define WITH_ETH_OVERHEAD

// CoDel: --------------------------------------------------------------------------------------------------

/// use approximate sqrt or sqrt from math library (toggle by un/commenting)
#define APPROX_MATH 

#define FLAVOR_DRAFT 0 
#define FLAVOR_LINUX 1
#define FLAVOR_CAKE 2 
/// select Codel version: select from above
#define FLAVOR FLAVOR_DRAFT

// PIE: --------------------------------------------------------------------------------------------------
// ARED: --------------------------------------------------------------------------------------------------



#endif