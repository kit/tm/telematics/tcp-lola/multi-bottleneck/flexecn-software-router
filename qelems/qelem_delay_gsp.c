#include "qelems.h"
#include "../rte_include.h"
#include "qlog.h"
#include "macros/common.h"

#define NO_BYTES_COUNTER
#include "qdt/lolliq.h"

#define DELAY_GSP
#include "qtrl/gsp.h"

static qelem_ops_t qelem_delay_gsp_ops;

typedef struct __qelem_delay_gsp {
    qelem_t qelem;

    gsp_params_t params;
    gsp_state_t state;

    lolliqueue_t queue;
} qelem_delay_gsp_t __rte_cache_aligned;

// typedef struct {
//     tsc_cycles_t timestamp;
// } pkt_metadata_t;

//------------------------------------------------------------------------------------------------------

static
qelem_t* qelem_delay_gsp_alloc ( const  qelem_params_t* common_params, const void* qelem_params )
{
    LOG_INIT ( "allocating qelem_delay_gsp for port %d \n", common_params->port_id );

    QPARAMSCAST ( gsp );
    qelem_delay_gsp_t* gsp = 0;

    // determine queue length
    unsigned qlen = params->qlen_bytes / ( 512 ) ; // in min-sized packets
//   unsigned qlen = params->qlen_bytes / (64) ; // in min-sized packets
    qlen = 1 << ( 32 - __builtin_clz ( qlen - 1 ) ); // rounded to the next power of two
    LOG_PARAM ( qlen ,"%u" );

    size_t queue_mem_size = lolliq_entries_sizeof ( qlen );
    QZMALLOC ( gsp, qelem_delay_gsp_t, queue_mem_size );
    qelem_init ( ( qelem_t* ) gsp, common_params, &qelem_delay_gsp_ops );
    lolliq_init ( &gsp->queue, qlen );

    ctrl_gsp_init ( &gsp->state, &gsp->params, params, rte_get_tsc_hz(), rte_get_tsc_cycles(),
                    common_params->trace_queue );

    return ( qelem_t* ) gsp;
};

//------------------------------------------------------------------------------------------------------
// optional opts
//------------------------------------------------------------------------------------------------------

static inline unsigned qelem_delay_gsp_enqueue_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    QCAST ( delay_gsp );
    tsc_cycles_t now = rte_get_timer_cycles();

    unsigned num_taildrops = 0;
    unsigned num_aqm_drops = 0;

    unsigned pkt_num;

    if ( lolliq_count ( &delay_gsp->queue ) == 0 ) {
        delay_gsp->state.timeout_expiry = now;

        // reset overflow state
        if ( delay_gsp->state.astate == OVERFLOW ) {
            delay_gsp->state.astate = DRAIN;
        }
    }

    // enqueue all packets before last, if we hit taildrop -> bad
    if ( num_pkts > 1 ) {
        unsigned num_enq = lolliq_enqueue_burst ( &delay_gsp->queue, pkts, num_pkts-1 );

        // timestamp all enqueued packets
        for ( pkt_num = 0 ; pkt_num < num_enq; pkt_num++ ) {
            struct rte_mbuf* pkt = pkts[pkt_num];
            timestamp_metadata_t* metadata = MBUF2TS_PTR(pkt);
            metadata->timestamp = now;
        };

        if ( unlikely ( num_enq < num_pkts - 1 ) ) {
            // on taildrops enter overflow state
            delay_gsp->state.astate = OVERFLOW;

            for ( pkt_num = num_enq ; pkt_num < num_pkts-1; pkt_num++ ) {
                rte_pktmbuf_free ( pkts[pkt_num] );
            };

            num_taildrops = ( num_pkts - 1 ) - num_enq;

            // TODO: somehow return from here
            // TODO: drop last pkt
        }
    }

    // gsp operation for last pkt
    struct rte_mbuf* pkt = pkts[num_pkts-1];

    if ( delay_gsp->state.last_sojourn > delay_gsp->params.threshold_tsc && now > delay_gsp->state.timeout_expiry ) {
        //drop packet:
        rte_pktmbuf_free ( pkt );
	
//         recalculate interval:
//         delay_gsp->state.interval = ( delay_gsp->params.initial_interval * delay_gsp->params.tau ) / ( delay_gsp->params.tau + delay_gsp->state.cummulative_time );
        gsp_on_adaptation ( delay_gsp->state.trace_queue, now, delay_gsp->state.cummulative_time, delay_gsp->state.interval, delay_gsp->state.astate );
	
	// set next timeout
        delay_gsp->state.timeout_expiry = now + delay_gsp->state.interval;

        num_aqm_drops = 1;
    } else {
        if ( !lolliq_enqueue_pkt ( &delay_gsp->queue, pkt ) ) {
            delay_gsp->state.astate = OVERFLOW;
            rte_pktmbuf_free ( pkt );

            num_taildrops++;
        } else {
            timestamp_metadata_t* metadata = MBUF2TS_PTR(pkt);
            metadata->timestamp = now;
        }
    }

    // update state
    if ( delay_gsp->state.astate == DRAIN && delay_gsp->state.last_sojourn > delay_gsp->params.threshold_tsc ) {
        delay_gsp->state.astate = CLEAR;
    }

    gsp_on_pkts_enqueue ( delay_gsp->state.trace_queue, now, num_aqm_drops, num_taildrops );
    return num_pkts;
}

// static unsigned qelem_delay_gsp_peek_burst ( qelem_t *qelem, struct rte_mbuf*** burst_ptr )
// {
//   QCAST ( gsp );
//   return lolliq_peek_burst ( &gsp->queue, burst_ptr );
// }
//
// static void qelem_delay_gsp_dequeue_peeked_burst ( qelem_t *qelem, struct rte_mbuf** peeked_burst, unsigned num_pkts )
// {
//   QCAST ( gsp );
//   lolliq_dequeue_peeked_burst ( &gsp->queue, peeked_burst, num_pkts );
// #ifdef WITH_TRACES
//   gsp_on_pkts_dequeue ( gsp->trace_queue, rte_get_timer_cycles(),
//                         lolliq_count ( &gsp->queue ), lolliq_qlen_bytes ( &gsp->queue ), num_pkts
//                       );
// #endif
//
// }
//
// static unsigned qelem_delay_gsp_queue_len_bytes ( qelem_t *qelem )
// {
//   QCAST ( gsp );
//   return lolliq_qlen_bytes ( &gsp->queue );
// }
//
// static unsigned qelem_delay_gsp_queue_len_pkts ( qelem_t *qelem )
// {
//   QCAST ( gsp );
//   return lolliq_count ( &gsp->queue );
// }

//------------------------------------------------------------------------------------------------------
// default opts
//------------------------------------------------------------------------------------------------------

static
void qelem_delay_gsp_tx_flush ( qelem_t *qelem )
{
    QCAST ( delay_gsp );
    tsc_cycles_t now = rte_get_timer_cycles();

    struct rte_mbuf** burst;
    unsigned num_pkts = lolliq_peek_burst ( &delay_gsp->queue, &burst );

    if ( likely ( num_pkts > 0 ) ) {
        unsigned num_tx = rte_eth_tx_burst ( qelem->port_id, qelem->queue_id, burst, num_pkts );
        if ( num_tx > 0 ) {
            // by specification we are supposed to use sojourn time of last dequeued packet on enqueue
            // so get sojourn time of last sent packet
//             pkt_metadata_t* metadata = ( pkt_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( burst[num_tx-1] , 0 );
            struct rte_mbuf* pkt = burst[num_tx-1];
            timestamp_metadata_t* metadata = MBUF2TS_PTR(pkt);
            tsc_cycles_t pkt_sojourn_time = now - metadata->timestamp;

//      if(pkt_sojourn_time > 2412000)
//      {
//	LOG_VAR(pkt_sojourn_time, "%lu");
//      }

            ctrl_gsp_on_pkts_dequeue ( &delay_gsp->state, &delay_gsp->params, now, pkt_sojourn_time );

            lolliq_dequeue_peeked_burst ( &delay_gsp->queue, burst, num_tx );

#if defined WITH_TRACES && WITH_TRACES == 2
            gsp_on_pkts_dequeue ( delay_gsp->state.trace_queue, now, pkt_sojourn_time );
#endif
        }
    }
}

static
void qelem_delay_gsp_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    QCAST ( delay_gsp );

    // enqueue packets
    qelem_delay_gsp_enqueue_burst ( qelem, pkts, num_pkts );

    // if there is at least burst packets in the queue, transmit packets
    if ( likely ( lolliq_count ( &delay_gsp->queue ) >= delay_gsp->qelem.min_send_burst ) ) {
        qelem_delay_gsp_tx_flush ( qelem );
    }
}

static
void qelem_delay_gsp_free ( qelem_t* qelem )
{
    rte_free ( qelem );
};

//-----------------------------------------------------------------------------------------------------------
// trace log
//-----------------------------------------------------------------------------------------------------------

#ifdef WITH_TRACES
static
void* gsp_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
    LOG_LINE();
    return trace_event_queue_add ( "delay_gsp", common_params->trace_queue_size, sizeof ( gsp_event_t ), gsp_event_log );
}
#endif

//--------------------------------------------------------------------------------------------------------

qelem_factory_t qelem_delay_gsp_factory = {
    .qelem_alloc = qelem_delay_gsp_alloc,
#ifdef WITH_TRACES
    .trace_queue_alloc = gsp_trace_queue_alloc,
#endif
};

static qelem_ops_t qelem_delay_gsp_ops = {
    .qelem_tx_burst = qelem_delay_gsp_tx_burst,
    .qelem_tx_flush = qelem_delay_gsp_tx_flush,
    .qelem_free = qelem_delay_gsp_free,

//   .enqueue_burst = qelem_delay_gsp_enqueue_burst,
//   .peek_burst = qelem_delay_gsp_peek_burst,
//   .dequeue_peeked_burst = qelem_delay_gsp_dequeue_peeked_burst,
//   .queue_len_bytes = qelem_delay_gsp_queue_len_bytes,
//   .queue_len_pkts = qelem_delay_gsp_queue_len_pkts,
};

