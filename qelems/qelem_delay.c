#include "qelems.h"
#include "../rte_include.h"
#include "qlog.h"
#include "macros/common.h"

#define NO_BYTES_COUNTER
#include "qdt/tsq.h"

typedef struct  {
    qelem_t qelem;
    tsc_cycles_t delay;
    tsqueue_t queue;
} qelem_delay_t __rte_cache_aligned;

typedef struct {
    uint64_t timestamp;
} delay_metadata_t;

static qelem_ops_t qelem_delay_ops;

//---------------------------------------------------------------------------------------------------------------

static
qelem_t* qelem_delay_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
    LOG_INIT ( "allocating qelem_delay for port %d \n", common_params->port_id );

    QPARAMSCAST ( delay );
    qelem_delay_t* qdelay = 0;

    // calculate queue length

    double qlen1 = ( params->delay_sec * params->link_speed_bit_s ) / ( params->min_pkt_size * 8 );
    unsigned qlen = qlen1;

    //round qlen to the next power of two
//     qlen = 1 << ( 32 - __builtin_clz ( qlen - 1 ) );
    // TODO this is probably processor diff ?
    qlen =  1 << (32 - __builtin_clz ( qlen - 1 ) );
    LOG_VAR ( qlen, "%u" );

    // init queue

    size_t queue_mem_size = tsq_entries_sizeof ( qlen);
    QZMALLOC ( qdelay, qelem_delay_t, queue_mem_size );
    qelem_init ( ( qelem_t* ) qdelay, common_params, &qelem_delay_ops );
    tsq_init ( &qdelay->queue, qlen);

    // set parameters
    qdelay->delay = params->delay_sec * TIMESTAMP_HZ();

    return ( qelem_t* ) qdelay;
};

//------------------------------------------------------------------------------------------------------
// optional ops
//------------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------------
// default opts
//------------------------------------------------------------------------------------------------------

static
void qelem_delay_tx_flush ( qelem_t *qelem )
{
    QCAST ( delay );
    tsc_cycles_t now = rte_get_tsc_cycles();

    struct rte_mbuf** burst = 0;
    unsigned num_deq = tsq_peek_burst ( &delay->queue, &burst );

    // check whether each packet was in queue long enough
    unsigned num_to_send;
    for ( num_to_send = 0; num_to_send < num_deq; num_to_send++ ) {
//         delay_metadata_t* metadata = ( delay_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( burst[num_to_send] , 0 );
//         tsc_cycles_t sojourn = now - metadata->timestamp;
        tsc_cycles_t sojourn = now - tsq_get_timestamp(&delay->queue, num_to_send);
        if ( sojourn < delay->delay ) {
            break;
        }
    }

    unsigned num_tx = rte_eth_tx_burst ( delay->qelem.port_id, delay->qelem.queue_id, burst, num_to_send );
    if ( num_tx > 0 ) {
        tsq_dequeue_peeked_burst ( &delay->queue, burst, num_tx );
    }
}

static
void qelem_delay_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    QCAST ( delay );
    QELEM_PKTS_IN ( delay, num_pkts );
    tsc_cycles_t now = rte_get_tsc_cycles();

    //
    // enqueue part
    //
    {
        // enqueue as many packets as possible
        unsigned num_enq = tsq_enqueue_burst ( &delay->queue, pkts, num_pkts , now);

        // timestamp all enqueued packets
        unsigned pkt_num;
//         for ( pkt_num = 0 ; pkt_num < num_enq; pkt_num++ ) {
//             delay_metadata_t* metadata = ( delay_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkts[pkt_num] , 0 );
//             metadata->timestamp = now;
//         };

        // free all non-enqueued packets
        if ( unlikely ( num_enq < num_pkts ) ) {
//             LOG_VAR( num_pkts-num_enq, "%u");
            for ( pkt_num = num_enq ; pkt_num < num_pkts; pkt_num++ ) {
                rte_pktmbuf_free ( pkts[pkt_num] );
            };
//             QELEM_TAIL_DROP ( delay, num_pkts-num_enq );
//             LOG_VAR(num_pkts-num_enq, "%u");
        }
    }

    //
    // dequeue part
    //
    {
        qelem_delay_tx_flush(qelem);
//         if (lolliq_count( &delay->queue )  < delay->qelem.min_send_burst) {
//             return;
//         }
//
//         struct rte_mbuf** burst = 0;
//         unsigned num_deq= lolliq_peek_burst ( &delay->queue, &burst );
//
//         // check whether each packet was in queue long enough
//         unsigned pkt_num;
//         for ( pkt_num = 0; pkt_num < num_deq; pkt_num++ ) {
//             delay_metadata_t* metadata = ( delay_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( burst[pkt_num] , 0 );
//             tsc_cycles_t sojourn = now - metadata->timestamp;
//             if ( sojourn < delay->delay ) {
//                 break;
//             }
//         }
//
//         // if there are less than burst packets, wait for a burst
//         if ( pkt_num < num_deq ) {
//             return ;
//         }
//
//         unsigned num_tx = rte_eth_tx_burst ( delay->qelem.port_id, delay->qelem.queue_id, burst, num_deq );
//         if ( num_tx > 0 ) {
//             lolliq_dequeue_peeked_burst ( &delay->queue, burst, num_tx );
//         }
    }

}

// static
// unsigned qelem_delay_enqueue_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts , unsigned hw_queue_index )
// {
//     qelem_delay_t* qelem0 = ( qelem_delay_t* ) qelem;
//     tsc_cycles_t now = TIMESTAMP();
//
//     unsigned num_enq = sq_enqueue_burst ( &qelem0->queue, pkts, num_pkts );
//
//     unsigned pkt_num;
//     for ( pkt_num = 0 ; pkt_num < num_enq; pkt_num++ ) {
//         delay_metadata_t* metadata = ( delay_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkts[pkt_num] , 0 );
//         metadata->timestamp = now;
//     };
//
//     // free packets
//     if ( unlikely ( num_enq < num_pkts ) ) {
//         for ( pkt_num = num_enq ; pkt_num < num_pkts; pkt_num++ ) {
//             rte_pktmbuf_free ( pkts[pkt_num] );
//         };
//         QELEM_TAIL_DROP ( qelem0, num_pkts-num_enq );
//     }
//
//     return num_enq;
// };
//
// static
// void qelem_delay_xmit ( qelem_t *_qelem, unsigned hw_queue_index )
// {
//     qelem_delay_t* qelem = ( qelem_delay_t* ) _qelem;
//
//     struct rte_mbuf** burst;
//     uint64_t now = rte_get_tsc_cycles();
//
//     // check head - if head's sojourn is too low, don't do anything
//     struct rte_mbuf* pkt =  sq_head ( &qelem->queue );
//     if ( !pkt ) {
//         return;
//     }
//     delay_metadata_t* metadata = ( delay_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkt , 0 );
//     tsc_cycles_t sojourn = now - metadata->timestamp;
//     if ( sojourn < qelem->delay ) {
//         return;
//     }
//
//     unsigned num_pkts = sq_peek ( &qelem->queue, &burst );
//
//     // check whether each packet was in queue long enough
//     unsigned pkt_num;
//     for ( pkt_num = 0; pkt_num < num_pkts; pkt_num++ ) {
//         delay_metadata_t* metadata = ( delay_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( burst[pkt_num] , 0 );
//         tsc_cycles_t sojourn = now - metadata->timestamp;
//         if ( sojourn < qelem->delay ) {
//             break;
//         }
//     }
//
//     unsigned num_to_send = pkt_num;
//
//     if ( num_to_send > 0 ) {
//         unsigned num_tx = rte_eth_tx_burst ( qelem->qelem.port_id, hw_queue_index, burst, num_to_send );
//         if ( num_tx > 0 ) {
//             sq_dequeue_peeked ( &qelem->queue, num_tx );
//         }
//     }
// }

static
void qelem_delay_free ( qelem_t* qelem )
{
    rte_free ( qelem );
};

//-----------------------------------------------------------------------------------------------------------

qelem_factory_t qelem_delay_factory = {
    .qelem_alloc = qelem_delay_alloc,
};

static qelem_ops_t qelem_delay_ops = {
    .qelem_tx_burst = qelem_delay_tx_burst,
    .qelem_tx_flush = qelem_delay_tx_flush,
    .qelem_free = qelem_delay_free,
};


