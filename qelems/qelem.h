#ifndef	QELEM
#define	QELEM

#include <stdint.h>
#include "qelem_stats.h"
#include "qelem_types.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

struct rte_mbuf;
struct __qelem;
typedef struct __qelem qelem_t;


typedef struct __qelem_params {
    unsigned port_id;
    unsigned tx_queue_id;
    unsigned min_send_burst;
// #ifdef WITH_TRACES    
    long unsigned trace_queue_size;
    void* trace_queue; 
// #endif
} qelem_params_t;

typedef struct __qelem_factory_ops {
    qelem_t* ( * qelem_alloc ) ( const qelem_params_t* common_params, const void* qelem_params );
#ifdef WITH_TRACES
    void* (*trace_queue_alloc)(const qelem_params_t* common_params, const void* qelem_params);
#endif
} qelem_factory_t;

typedef struct __qelem_ops {
    void ( * qelem_free ) ( qelem_t* );

    // default functions
    void ( * qelem_tx_burst ) ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts );
    void ( * qelem_tx_flush ) ( qelem_t *qelem );

    // optional functions to use qelems as elements of other qelems
    unsigned ( *enqueue_burst ) ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts); // return what?
    unsigned ( *peek_burst ) ( qelem_t *qelem, struct rte_mbuf*** burst_ptr );
    void ( *dequeue_peeked_burst ) ( qelem_t *qelem, struct rte_mbuf** burst_ptr, unsigned num_pkts );
    unsigned ( *queue_len_bytes ) ( qelem_t *qelem );
    unsigned ( *queue_len_pkts ) ( qelem_t *qelem );

    // deprecated

    // transmits packets to the device
    // this function is not allowed to drop packets
    // (unless we have headrop queue!)
    void ( *xmit ) ( qelem_t *qelem, unsigned hw_queue_index );
} qelem_ops_t;

struct __qelem {
    qelem_ops_t* v_table;
    uint8_t port_id;
    uint16_t queue_id; 
    uint32_t min_send_burst;
    qelem_stats_t stats;
};

static inline 
void qelem_init(qelem_t *qelem, const qelem_params_t* common_params,  qelem_ops_t* v_table){
    qelem->port_id = common_params->port_id;
    qelem->queue_id = common_params->tx_queue_id;
    qelem->min_send_burst = common_params->min_send_burst;
    qelem->v_table = v_table;
}

static inline
unsigned qelem_enqueue_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    return qelem->v_table->enqueue_burst ( qelem, pkts, num_pkts );
}

static inline
unsigned qelem_peek_burst (qelem_t *qelem, struct rte_mbuf*** burst_ptr )
{
    return qelem->v_table->peek_burst ( qelem, burst_ptr );
}

static inline
void qelem_dequeue_peeked_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    qelem->v_table->dequeue_peeked_burst ( qelem, pkts, num_pkts );
}

static inline
unsigned qelem_queue_len_bytes ( qelem_t *qelem)
{
    return qelem->v_table->queue_len_bytes (qelem);
}

static inline
unsigned qelem_queue_len_pkts ( qelem_t *qelem )
{
    return qelem->v_table->queue_len_pkts (qelem);
}

static inline
void qelem_xmit ( qelem_t *qelem, unsigned hw_queue_index )
{
    return qelem->v_table->xmit ( qelem, hw_queue_index );
}

static inline
void qelem_free ( qelem_t *qelem )
{
    return qelem->v_table->qelem_free ( qelem );
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
