#ifndef	__QELEMS__ECN__
#define	__QELEMS__ECN__

#include "../rte_include.h"
#include "qelem_types.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

enum {
  ECN_CE_MARK = 0x3,
  ECN_MASK = 0x3,
};

// basic implementation of ECN marking: only sets CE bits in ECT marked packets.

// boolean (ecn_mark_pkt);

static
boolean_t ecn_mark_ipv4_pkt ( struct ipv4_hdr* ip_hdr );

static
boolean_t ecn_mark_ipv6_pkt ( struct ipv6_hdr* ip_hdr );

static
boolean_t ecn_mark_pkt ( struct rte_mbuf* pkt )
{
  const uint16_t swapped_ETHER_TYPE_IPv4 = rte_constant_bswap16 ( ETHER_TYPE_IPv4 );
  const uint16_t swapped_ETHER_TYPE_IPv6 = rte_constant_bswap16 ( ETHER_TYPE_IPv6 );

  struct ether_hdr* eth_hdr = rte_pktmbuf_mtod ( pkt, struct ether_hdr* );

  // can't use switch/case because values are not exactly bswapped;
  if ( eth_hdr->ether_type == swapped_ETHER_TYPE_IPv4 )
  {
    return ecn_mark_ipv4_pkt ( ( struct ipv4_hdr* ) &eth_hdr[1] );
  }
  else if ( eth_hdr->ether_type == swapped_ETHER_TYPE_IPv6 )
  {
    return ecn_mark_ipv6_pkt ( ( struct ipv6_hdr* ) &eth_hdr[1] );
  }
  else
  {
    return FALSE;
  }
}

boolean_t ecn_mark_ipv4_pkt ( struct ipv4_hdr* ip_hdr )
{

  uint8_t tos = ip_hdr->type_of_service;

  // check if packet supports ecn and if not return
  boolean_t ecn_enabled = (tos & ECN_MASK) > 0;
  if ( !ecn_enabled )
  {
    return FALSE;
  }

  // set CE and recalculate checksum
  ip_hdr->type_of_service |= ECN_CE_MARK;

  // update checksum
  //   HC' = { HC - 1     HC > 0
  //         { 0xFFFE     HC = 0

  uint16_t checksum = ip_hdr->hdr_checksum;
  checksum =  rte_be_to_cpu_16 ( checksum );
  checksum = checksum != 0 ? checksum - 1 : 0xFFFE;
  ip_hdr->hdr_checksum = rte_cpu_to_be_16 ( checksum );

  return TRUE;
}

boolean_t ecn_mark_ipv6_pkt ( struct ipv6_hdr* ip_hdr )
{

  uint32_t line = rte_be_to_cpu_32 ( ip_hdr->vtc_flow );

  // check if packet supports ecn and if not return
  boolean_t ecn_enabled = ( ( line >> 20 ) & ECN_MASK ) > 0;
  if ( !ecn_enabled )
  {
    return FALSE;
  }

  // set CE
  ip_hdr->vtc_flow |=  rte_cpu_to_be_32 ( ECN_CE_MARK << 20 );
  return TRUE;
}




#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
