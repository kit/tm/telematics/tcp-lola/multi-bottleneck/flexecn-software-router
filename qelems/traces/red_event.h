#ifndef	__RED_EVENT__
#define	__RED_EVENT__

#ifdef IN_IDE_PARSER
#define WITH_TRACES 2
#endif

#ifdef WITH_TRACES

#include <stdint.h>
#include "trace_event_queue.h"

enum RED_EVENT {RED_PKT_SHOULD_DROP, RED_PKTS_ENQ, RED_PKTS_DEQ, RED_UPDATE};

typedef struct red_event
{
  uint64_t now;
  uint32_t event_type;
  uint32_t qlen_pkts_taildrops;
  uint32_t aqm_drops;
  double avg_maxp;
  double p_b;
} red_event_t;

#include "../macros/qdbg.h"

static inline void red_on_pkt_should_drop ( event_log_queue_t* queue, uint64_t now, unsigned qlen_pkts, double avg_qlen, double p_b, unsigned is_dropped )
{
  if ( ( queue->writer_index < queue->size ) )
  {
    red_event_t* event = & ( ( ( red_event_t* ) queue->events ) [queue->writer_index] );
    event->event_type = RED_PKT_SHOULD_DROP;
    event->now = now;
    event->qlen_pkts_taildrops = qlen_pkts;
    event->avg_maxp = avg_qlen;
    event->p_b = p_b;
    event->aqm_drops = is_dropped;
    COMPILER_BARRIER();
    queue->writer_index++;
  }
}

static inline void red_on_pkts_enqueue ( event_log_queue_t* queue, uint64_t now, unsigned num_aqm_drop, unsigned num_taildrop )
{
  if ( ( num_aqm_drop > 0 || num_taildrop > 0 ) && ( queue->writer_index < queue->size ) )
  {
    red_event_t* event = & ( ( ( red_event_t* ) queue->events ) [queue->writer_index] );
    event->event_type = RED_PKTS_ENQ;
    event->now = now;
    event->qlen_pkts_taildrops = num_taildrop;
    event->aqm_drops = num_aqm_drop;
    COMPILER_BARRIER();
    queue->writer_index++;
  }
}

static inline void red_on_pkts_dequeue ( event_log_queue_t* queue, uint64_t now, unsigned qlen_pkts )
{
  if ( queue->writer_index < queue->size )
  {
    red_event_t* event = & ( ( ( red_event_t* ) queue->events ) [queue->writer_index] );
    event->event_type = RED_PKTS_DEQ;
    event->now = now;
    event->qlen_pkts_taildrops = qlen_pkts;
    COMPILER_BARRIER();
    queue->writer_index++;
  }
}

static inline void red_on_update ( event_log_queue_t* queue, uint64_t now, double max_p )
{
  if ( ( queue->writer_index < queue->size ) )
  {
    red_event_t* event = & ( ( ( red_event_t* ) queue->events ) [queue->writer_index] );
    event->event_type = RED_UPDATE;
    event->now = now;
    event->avg_maxp = max_p;
    COMPILER_BARRIER();
    queue->writer_index++;
  }
}

#else

#define red_on_pkt_should_drop(queue,now,qlen_pkts,avg_qlen,p_b,is_dropped)
#define red_on_pkts_enqueue(queue,now,num_aqm_drop,num_taildrop)
#define red_on_update(queue,now,max_p)


#endif // ifdef WITH_TRACES


#endif // ifndef __QTRL_TAILDROP_LOG__



