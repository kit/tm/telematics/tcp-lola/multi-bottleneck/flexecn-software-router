#ifndef	__GSP_EVENT__
#define	__GSP_EVENT__

#ifdef IN_IDE_PARSER
#define WITH_TRACES 2
#define TRACES_QSAMPLING 8
#endif

#ifdef WITH_TRACES

#include <stdint.h>
#include "trace_event_queue.h"

enum GSP_EVENT {GSP_PKT_DROP, GSP_DEQUEUE, GSP_ADAPT};

typedef struct gsp_event {
    uint64_t now;
    uint32_t event_type;
    uint64_t aqmdrops_ctime;
    uint64_t taildrops_interval;
    uint64_t qlen_astate;
} gsp_event_t;

static inline void gsp_on_pkts_enqueue ( event_log_queue_t* queue, uint64_t now, uint32_t num_aqm_drops, uint32_t num_tail_drops)
{
    if ( (num_aqm_drops > 0 || num_tail_drops > 0)  && ( queue->writer_index < queue->size ) ) {
        gsp_event_t* event = & ( ( ( gsp_event_t* ) queue->events ) [queue->writer_index] );
        event->event_type = GSP_PKT_DROP;
        event->now = now;
        event->aqmdrops_ctime = num_aqm_drops;
        event->taildrops_interval = num_tail_drops;
        COMPILER_BARRIER();
        queue->writer_index++;
    }
}

static inline void gsp_on_pkts_dequeue ( event_log_queue_t* queue, uint64_t now, uint64_t qlen_bytes)
{
    if ( queue->writer_index < queue->size ) {
      //FIXME: in current version dealy gsp will on every packet dequeue
      // first perform adaptation and then log queue state
      // as so we increase sampling index in gsp_on_adaptation and save
      // both events when sampling % TRACES_QSAMPLING = 0
       queue->sampling_index++;
       if( (queue->sampling_index & (TRACES_QSAMPLING-1)) == 0){
        gsp_event_t* event = & ( ( ( gsp_event_t* ) queue->events ) [queue->writer_index] );
        event->event_type = GSP_DEQUEUE;
        event->now = now;
        event->qlen_astate = qlen_bytes;
        COMPILER_BARRIER();
        queue->writer_index++;
	}
    }
}

static inline void gsp_on_adaptation ( event_log_queue_t* queue, uint64_t now, uint64_t cumm_time, uint64_t interval, unsigned state)
{
    if ( queue->writer_index < queue->size ) {
//       queue->sampling_index++;
//       if( (queue->sampling_index & (TRACES_QSAMPLING-1)) == 0){
        gsp_event_t* event = & ( ( ( gsp_event_t* ) queue->events ) [queue->writer_index] );
        event->event_type = GSP_ADAPT;
        event->now = now;
        event->aqmdrops_ctime = cumm_time;
        event->taildrops_interval = interval;
	event->qlen_astate = state;
        COMPILER_BARRIER();
        queue->writer_index++;
//       }
    }
}

#else

#define gsp_on_pkts_enqueue(queue, now, num_aqm_drops,num_tail_drops)
#define gsp_on_pkts_dequeue(queue, now, qlen_bytes)
#define gsp_on_adaptation(queue,now, cumm_time, interval,state)

#endif // ifdef WITH_TRACES


#endif // ifndef __QTRL_TAILDROP_LOG__



