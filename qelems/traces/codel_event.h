#ifndef	__CODEL_EVENT__
#define	__CODEL_EVENT__

#ifdef IN_IDE_PARSER
#define WITH_TRACES 2
#endif

#ifdef WITH_TRACES

#include <stdint.h>
#include "trace_event_queue.h"

enum CODEL_EVENT {CODEL_PKT_DEQUEUE_LOG, CODEL_TAILDROP, CODEL_INOUT_DROPPING_STATE, CODEL_PKT_DROP, FQ_CODEL_ON_ENQUEUE, FQ_CODEL_OVERFLOW};

typedef struct __codel_event {
    uint64_t now;
    uint64_t sojourn_dropnext;
    uint32_t event_type;
    uint32_t qlenbytes_count_droppingstate_taildrops;
} codel_event_t;

typedef struct __fq_codel_event {
    uint64_t now;
    uint64_t sojourn_dropnext;
    uint32_t event_type;
    uint32_t qlenbytes_count_droppingstate_taildrops;
    uint64_t fq_queue_addr;
} fq_codel_event_t;

#ifdef FQ_CODEL
#define FQ_QUEUE_VAR , void* fq_queue_ptr
#define FQ_QUEUE_ASSIGN  (event)->fq_queue_addr = (uint64_t) fq_queue_ptr
#define codel_event_t fq_codel_event_t
#else
#define FQ_QUEUE_VAR 
#define FQ_QUEUE_ASSIGN
#endif

static inline void codel_on_pkt_dequeue ( event_log_queue_t* queue, uint64_t now, uint64_t sojourn_time, uint32_t qlen_bytes FQ_QUEUE_VAR )
{
    if ( ( queue->writer_index < queue->size ) ) {
	queue->sampling_index++;
	if( (queue->sampling_index & (TRACES_QSAMPLING-1)) == 0){
	  codel_event_t* event = & ( ( ( codel_event_t* ) queue->events ) [queue->writer_index] );
	  event->now = now;
	  event->sojourn_dropnext = sojourn_time;
	  event->event_type = CODEL_PKT_DEQUEUE_LOG;
	  event->qlenbytes_count_droppingstate_taildrops = qlen_bytes;
	  FQ_QUEUE_ASSIGN;
	  COMPILER_BARRIER();
	  queue->writer_index++;
      }
    }
}

static inline void codel_on_taildrop ( event_log_queue_t* queue, uint64_t now, uint32_t num_pkts_dropped )
{
    if ( num_pkts_dropped != 0 ) {
        if ( ( queue->writer_index < queue->size ) ) {
            codel_event_t* event = & ( ( ( codel_event_t* ) queue->events ) [queue->writer_index] );
            event->now = now;
            event->event_type = CODEL_TAILDROP;
            event->qlenbytes_count_droppingstate_taildrops = num_pkts_dropped;
            COMPILER_BARRIER();
            queue->writer_index++;
        }
    }
}

static inline void codel_on_aqm_drop ( event_log_queue_t* queue, uint64_t now, uint64_t drop_next, uint32_t pkts_dropped FQ_QUEUE_VAR)
{
    if ( ( queue->writer_index < queue->size ) ) {
        codel_event_t* event = & ( ( ( codel_event_t* ) queue->events ) [queue->writer_index] );
        event->now = now;
        event->sojourn_dropnext = drop_next;
        event->event_type = CODEL_PKT_DROP;
        event->qlenbytes_count_droppingstate_taildrops = pkts_dropped;
	FQ_QUEUE_ASSIGN;
        COMPILER_BARRIER();
        queue->writer_index++;
    }
}


static inline void codel_on_enter_dropping_state ( event_log_queue_t* queue, uint64_t now FQ_QUEUE_VAR)
{
    if ( ( queue->writer_index < queue->size ) ) {
        codel_event_t* event = & ( ( ( codel_event_t* ) queue->events ) [queue->writer_index] );
        event->now = now;
        event->event_type = CODEL_INOUT_DROPPING_STATE;
        event->qlenbytes_count_droppingstate_taildrops = 1;
	FQ_QUEUE_ASSIGN;
        COMPILER_BARRIER();
        queue->writer_index++;
    }
}

static inline void codel_on_exit_dropping_state ( event_log_queue_t* queue, uint64_t now FQ_QUEUE_VAR)
{
    if ( ( queue->writer_index < queue->size ) ) {
        codel_event_t* event = & ( ( ( codel_event_t* ) queue->events ) [queue->writer_index] );
        event->now = now;
        event->event_type = CODEL_INOUT_DROPPING_STATE;
        event->qlenbytes_count_droppingstate_taildrops = 0;
	FQ_QUEUE_ASSIGN;
        COMPILER_BARRIER();
        queue->writer_index++;
    }
}

static inline void fq_codel_on_pkt_enqueue ( event_log_queue_t* queue, uint64_t now, unsigned global_pkt_counter )
{
#if WITH_TRACES == 2  
    if ( ( queue->writer_index < queue->size ) ) {
        fq_codel_event_t* event = & ( ( ( fq_codel_event_t* ) queue->events ) [queue->writer_index] );
        event->now = now;
        event->event_type = FQ_CODEL_ON_ENQUEUE;
        event->qlenbytes_count_droppingstate_taildrops = global_pkt_counter;
        COMPILER_BARRIER();
        queue->writer_index++;
    }
#endif    
}

static inline void fq_codel_on_pkt_overflow ( event_log_queue_t* queue, uint64_t now, void* flow_queue_ptr )
{
    if ( ( queue->writer_index < queue->size ) ) {
        fq_codel_event_t* event = & ( ( ( fq_codel_event_t* ) queue->events ) [queue->writer_index] );
        event->now = now;
        event->event_type = FQ_CODEL_OVERFLOW;
        event->fq_queue_addr = (uint64_t) flow_queue_ptr;
        COMPILER_BARRIER();
        queue->writer_index++;
    }
}

#else

#define codel_on_pkt_dequeue(queue, now, sojourn_time, qlen_bytes)
#define codel_on_taildrop(queue, now, num_pkts_dropped)
#define codel_on_aqm_drop(queue, now, drop_next, count)
#define codel_on_enter_dropping_state(queue, now)
#define codel_on_exit_dropping_state(queue, now)
#define fq_codel_on_pkt_enqueue(queue, now, global_pkt_counter)
#define fq_codel_on_pkt_overflow(queue, now, flow_queue_ptr)


#endif // ifdef WITH_TRACES


#endif // ifndef __QTRL_CODEL_LOG__



