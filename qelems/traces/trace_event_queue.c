#include "trace_event_queue.h"
#include "taildrop_event.h"
#include "codel_event.h"
#include "pie_event.h"
#include "red_event.h"
#include "gsp_event.h"
#include "curvyred_event.h"
#include "../qlog.h"
#include "../../rte_include.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_FILE_NAME 64

trace_event_queues_t trace_event_queues_list;

void trace_event_init ( void )
{
  LIST_INIT ( &trace_event_queues_list );
}

// "codel"

event_log_queue_t* trace_event_queue_add ( const char* log_file, unsigned long num_entries, __SIZE_TYPE__ size_of_entry, logger_func logger )
{
  // get time string
  time_t raw_time;
  struct tm *tm;

  time ( &raw_time );
  tm = localtime ( &raw_time );

  char time_buffer[20];
  strftime ( time_buffer,20,"%FT%X", tm );

  // open tracefile for writing:
  char filename[MAX_FILE_NAME];
  char filename_binary[MAX_FILE_NAME];
  sprintf ( ( char* ) &filename, "%s_%s.log", log_file , time_buffer );
  sprintf ( ( char* ) &filename_binary, "%s_%s.qlm", log_file , time_buffer );
  FILE* fd = fopen ( filename, "w" );
//   FILE* fd_bin = fopen ( filename_binary, "wb" );

  if ( !fd )
  {
    rte_panic ( "error opening file '%s' for writing: %s\n", filename, strerror ( errno ) );
  }

//   if ( !fd_bin )
//   {
//     rte_panic ( "error opening file '%s' for writing: %s\n", filename_binary, strerror ( errno ) );
//   }

  LOG_PARAM ( num_entries, "%lu" );
  LOG_PARAM ( __builtin_clz ( num_entries - 1 ), "%d" );
  LOG_PARAM ( __builtin_clz ( ( unsigned ) num_entries - 1 ), "%d" );
  LOG_PARAM ( __builtin_clzl ( num_entries - 1 ), "%d" );
  LOG_PARAM ( __builtin_clzll ( num_entries - 1 ), "%d" );

  // allocate memory for event queue
  size_t entries =  1lu << ( 64 - __builtin_clzl ( num_entries - 1 ) );
  LOG_PARAM ( entries, "%lu" );


  event_log_queue_t* queue = rte_zmalloc_socket ( "eventqueue", sizeof ( event_log_queue_t ) + entries * size_of_entry, RTE_CACHE_LINE_SIZE , rte_socket_id() );

  if ( !queue )
  {
    rte_panic ( "allocating event queue failed" );
  }

  queue->writer_index = 0;
  queue->size = entries;
  queue->events = ( void* ) &queue[1];

  // create entry
  trace_event_queue_t* entry = rte_malloc ( "trace_entry_t", sizeof ( trace_event_queue_t ), 0 );
  entry->logger = logger;
  entry->fd = fd;
//   entry->fd_bin = fd_bin;
  entry->queue = queue;
  entry->reader_index = 0;

  LIST_INSERT_HEAD ( &trace_event_queues_list, entry, l_entry );
  return queue;
}

//--------------------------------------------------------------------------------------
// per-aqm event loggers
//--------------------------------------------------------------------------------------

#ifdef WITH_TRACES


void taildrop_event_log ( trace_event_queue_t* queue )
{
//   LOG_LINE();
  FILE* fd = queue->fd;
  taildrop_event_t* event = & ( ( ( taildrop_event_t* ) queue->queue->events ) [queue->reader_index] );
  switch ( event->event_type )
  {
    // TODO why are tail_drops always 0!
  case TAILDROP_PKT_DROP:
    fprintf ( fd, "pktdrop_log,%lu,%u\n", event->now, event->taildrops );
    break;
  case TAILDROP_PKT_DEQUEUE:
    fprintf ( fd, "dequeue_log,%lu,%u,%u,%lu\n", event->now, event->qlen_pkts, event->qlen_bytes,event->last_sojourn );
    break;
  }

//   LOG_LINE();
//   fwrite(event, sizeof(taildrop_event_t),1,queue->fd_bin);
//   LOG_LINE();
}

/**
 * codel headers
 *
 * dequeue_log: timestamp, pkt_sojourn_time, qlen_bytes
 * taildrop: timestamp, num_dropped_packets
 * codeldrop: timestamp, codel_count, codel_dropnext
 * dropping_state: timestamp, in/out
 */
void codel_event_log ( trace_event_queue_t* queue )
{
  FILE* fd = queue->fd;
  codel_event_t* event = & ( ( ( codel_event_t* ) queue->queue->events ) [queue->reader_index] );
  switch ( event->event_type )
  {
  case CODEL_PKT_DEQUEUE_LOG:
    fprintf ( fd, "dequeue_log,%lu,%lu,%u\n", event->now, event->sojourn_dropnext, event->qlenbytes_count_droppingstate_taildrops );
    break;
  case CODEL_TAILDROP:
    fprintf ( fd, "taildrop,%lu,%u\n", event->now, event->qlenbytes_count_droppingstate_taildrops );
    break;
  case CODEL_PKT_DROP:
    fprintf ( fd, "codeldrop,%lu,%u,%lu\n", event->now, event->qlenbytes_count_droppingstate_taildrops, event->sojourn_dropnext );
    break;
  case CODEL_INOUT_DROPPING_STATE:
    fprintf ( fd, "dropping_state,%lu,%u\n", event->now, event->qlenbytes_count_droppingstate_taildrops );
    break;
  }
}

/**
 * codel headers
 *
 * dequeue_log: timestamp, pkt_sojourn_time, qlen_bytes
 * taildrop: timestamp, num_dropped_packets
 * codeldrop: timestamp, codel_count, codel_dropnext
 * dropping_state: timestamp, in/out
 */
void fq_codel_event_log ( trace_event_queue_t* queue )
{
  FILE* fd = queue->fd;
  fq_codel_event_t* event = & ( ( ( fq_codel_event_t* ) queue->queue->events ) [queue->reader_index] );
  switch ( event->event_type )
  {
  case CODEL_PKT_DEQUEUE_LOG:
    fprintf ( fd, "dequeue_log,%lu, %lu,%lu,%u\n", event->fq_queue_addr, event->now, event->sojourn_dropnext, event->qlenbytes_count_droppingstate_taildrops );
    break;
  case FQ_CODEL_ON_ENQUEUE:
    fprintf ( fd, "fq-enqueue,%lu,%u\n",event->now, event->qlenbytes_count_droppingstate_taildrops );
    break;
  case FQ_CODEL_OVERFLOW:
    fprintf ( fd, "fq-overflow,%lu,%lu\n", event->fq_queue_addr, event->now );
    break;
  case CODEL_PKT_DROP:
    fprintf ( fd, "codeldrop,%lu,%lu,%u,%lu\n", event->fq_queue_addr, event->now, event->qlenbytes_count_droppingstate_taildrops, event->sojourn_dropnext );
    break;
  case CODEL_INOUT_DROPPING_STATE:
    fprintf ( fd, "dropping_state,%lu,%lu,%u\n",event->fq_queue_addr,  event->now, event->qlenbytes_count_droppingstate_taildrops );
    break;
  }
}

/**
 * pie headers
 *
 * pie_enqueue_log: now, qlen_bytes, aqm_drops, tail_drops
 * pie_dequeue_log: now, qlen_bytes
 * pie_update_log: now, qlen_bytes, curr_delay, p, burst_allow (?)
 * pie_inout_measurement: now, in_out, burst_allow
 * pie_new_measurement: now, curr_drain_rate, avg_drain_rate, burst_allow
 */
void pie_event_log ( trace_event_queue_t* queue )
{
  FILE* fd = queue->fd;
  pie_event_t* event = & ( ( ( pie_event_t* ) queue->queue->events ) [queue->reader_index] );
  switch ( event->event_type )
  {
  case PIE_DROP:
    fprintf ( fd, "drops,%lu,%u,%u\n", event->now,
              event->aqmdrops_measurement, ( uint32_t ) event->burstallow_taildrops );
    break;
  case PIE_QUEUE_SOJOURN:
    fprintf ( fd, "queue_sojourn,%lu,%lu\n", event->now,event->qlen_bytes__sojourn );
    break;
  case PIE_UPDATE:
    fprintf ( fd, "update_log,%lu,%f,%f,%lu\n", event->now,
              event->curr_drainrate_delay,  event->avgdrainrate_p, event->burstallow_taildrops );
    break;
  case PIE_BALLOW_RESET:
    fprintf ( fd, "ballow_reset,%lu,%lu\n", event->now, event->burstallow_taildrops );
    break;

//   case PIE_MEASUREMENT:
//     fprintf ( fd, "inout_measurement_log,%lu,%u,%lu\n", event->now,event->aqmdrops_measurement, event->burstallow_taildrops );
//     break;
//   case PIE_DRAIN_RATE_EST:
//     fprintf ( fd, "new_measurement,%lu,%f,%f,%lu\n", event->now, event->curr_drainrate_delay,  event->avgdrainrate_p, event->burstallow_taildrops );
//     break;
  }
}


/**
 * red headers
 *
 * red_should_drop_log: now, qlen_pkts, avg, p_b, should_drop
 * red_enqueue_log: now, aqm_drops, tail_drops
 * red_update_log: now, max_p
 */
void red_event_log ( trace_event_queue_t* queue )
{
  FILE* fd = queue->fd;
  red_event_t* event = & ( ( ( red_event_t* ) queue->queue->events ) [queue->reader_index] );
  switch ( event->event_type )
  {
  case RED_PKT_SHOULD_DROP:
    fprintf ( fd, "red_should_drop,%lu,%u,%f,%f,%u\n", event->now, event->qlen_pkts_taildrops,
              event->avg_maxp, event->p_b, event->aqm_drops );
    break;
  case RED_PKTS_ENQ:
    fprintf ( fd, "red_enqueue,%lu,%u,%u\n", event->now, event->aqm_drops, event->qlen_pkts_taildrops );
    break;
  case RED_UPDATE:
    fprintf ( fd, "red_update,%lu,%f\n", event->now,event->avg_maxp );
    break;
  }
}

/**
 * gsp headers
 *
 * gsp_drops_log: now, aqm_drops, tail_drops
 * gsp_qlen_log: now, qlen_bytes
 * gsp_adapt_log: now, cumm_time, interval, state
 */
void gsp_event_log ( trace_event_queue_t* queue )
{
  FILE* fd = queue->fd;
  gsp_event_t* event = & ( ( ( gsp_event_t* ) queue->queue->events ) [queue->reader_index] );
  switch ( event->event_type )
  {
  case GSP_PKT_DROP:
    fprintf ( fd, "gsp_drops_log,%lu,%lu,%lu\n", event->now, event->aqmdrops_ctime,
              event->taildrops_interval );
    break;
  case GSP_DEQUEUE:
    fprintf ( fd, "gsp_qlen_log,%lu,%lu\n", event->now, event->qlen_astate );
    break;
  case GSP_ADAPT:
    fprintf ( fd, "gsp_adapt_log,%lu,%lu,%lu,%lu\n",
              event->now,event->aqmdrops_ctime, event->taildrops_interval, event->qlen_astate );
    break;
  }
//   fwrite(event, sizeof(gsp_event_t), 1, queue->fd_bin);
}

/**
 * curvyred headers
 *
 * cr_taildrop: now, qlen_pkts, avg, p_b, should_drop
 * cr_dequeue: now, aqm_drops, tail_drops
 * red_update_log: now, max_p
 */
void curvyred_event_log ( trace_event_queue_t* queue )
{
  FILE* fd = queue->fd;
  curvyred_event_t* event = & ( ( ( curvyred_event_t* ) queue->queue->events ) [queue->reader_index] );
  switch ( event->event_type )
  {
  case CURVYRED_PKTS_TAILDROP:
    fprintf ( fd, "cr_taildrop,%lu,%u\n", event->now, event->aqm_drops_taildrops );
    break;
  case RED_PKTS_ENQ:
    fprintf ( fd, "cr_dequeue,%lu,%lu,%f,%u\n", event->now, event->curr_sojourn,
              event->avg_sojourn, event->aqm_drops_taildrops );
    break;
  }
}

#endif //ifdef WITH_TRACES

