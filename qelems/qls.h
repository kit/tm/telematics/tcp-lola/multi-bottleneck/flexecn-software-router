#ifndef	__QLS__
#define	__QLS__

#include <stdint.h>

/**
 * @file 
 * 
 * basic interface for classifiers
 */

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

struct rte_mbuf;
struct __qls;
typedef struct __qls qls_t;

typedef qls_t* (*qls_create_op) (void* qls_params);

struct __qls {
    uint32_t (*qlassify) (qls_t* qls, struct rte_mbuf* pkt);
};

static inline 
uint32_t qls_qlassify (qls_t* qls, struct rte_mbuf* pkt){
  return qls->qlassify(qls, pkt);
}

// static inline
// qls_t* qls_create();

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
