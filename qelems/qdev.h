#ifndef	__QDEV__
#define	__QDEV__

#include "qelem.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

typedef struct {
    qelem_t** qelems;
} qdev_port_t;

extern qdev_port_t qdev_ports[];

struct rte_eth_txconf;

void qdev_port_init ( uint8_t port_id, uint16_t num_tx_queues, int socket_id );
int qdev_queue_setup ( uint8_t port_id, uint16_t tx_queue_id, uint16_t nb_tx_desc,
		       unsigned int socket_id, const struct rte_eth_txconf* tx_conf, 
		       unsigned int min_send_burst, qelem_factory_t* qelem_factory, const void* qelem_params
    );

static void qdev_tx_burst ( uint8_t port_id, uint16_t queue_id, struct rte_mbuf **tx_pkts, uint16_t nb_pkts );
static void qdev_tx_flush ( uint8_t port_id, uint16_t queue_id );


static
void qdev_tx_burst ( uint8_t port_id, uint16_t queue_id, struct rte_mbuf** tx_pkts, uint16_t nb_pkts )
{
    qelem_t* qelem = qdev_ports[port_id].qelems[queue_id];
    qelem->v_table->qelem_tx_burst ( qelem, tx_pkts, nb_pkts );
}

static
void qdev_tx_flush ( uint8_t port_id, uint16_t queue_id )
{
    qelem_t* qelem = qdev_ports[port_id].qelems[queue_id];
    qelem->v_table->qelem_tx_flush ( qelem );
}

static
qelem_stats_t qdev_queue_stats_get ( uint8_t port_id, uint16_t queue_id )
{
    qelem_t* qelem = qdev_ports[port_id].qelems[queue_id];
    return qelem->stats;
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif

