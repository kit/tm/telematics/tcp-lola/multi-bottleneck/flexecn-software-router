#include "qelems.h"
#include "../rte_include.h"
#include "qlog.h"
#include "macros/common.h"
#include "qdt/lolliq.h"
#include "qtrl/gsp.h"

static qelem_ops_t qelem_gsp_ops;

typedef struct __qelem_gsp
{
  qelem_t qelem;

  gsp_params_t params;
  gsp_state_t state;


//   //params
//   double interval; // 2 * RTT
//   double threshold; // 1/2 buffer size
//
//   double tau; // parameter, controlling the adaptation speed: 5 * interval
//   double initial_interval; // 2 * RTT
//   double cummulative_time;
//   qstate_t state;
//
//   // adapt
//   tsc_cycles_t time_above_thresh;
//   tsc_cycles_t time_below_thresh;
//   tsc_cycles_t last_timestamp;
//   double alpha;
//
//   //state
//   tsc_cycles_t timeout_expiry;


  lolliqueue_t queue;
} qelem_gsp_t __rte_cache_aligned;


//------------------------------------------------------------------------------------------------------

static
qelem_t* qelem_gsp_alloc ( const  qelem_params_t* common_params, const void* qelem_params )
{
  LOG_INIT ( "allocating qelem_gsp for port %d \n", common_params->port_id );

  QPARAMSCAST ( gsp );
  qelem_gsp_t* gsp = 0;

  // determine queue length
  unsigned qlen = params->qlen_bytes / (64) ; // in min-sized packets
  qlen = 1 << ( 32 - __builtin_clz ( qlen - 1 ) ); // rounded to the next power of two
  LOG_PARAM ( qlen ,"%u" );

  size_t queue_mem_size = lolliq_entries_sizeof (qlen);
  QZMALLOC ( gsp, qelem_gsp_t, queue_mem_size );
  qelem_init ( ( qelem_t* ) gsp, common_params, &qelem_gsp_ops );
  lolliq_init ( &gsp->queue, qlen);

  ctrl_gsp_init ( &gsp->state, &gsp->params, params, rte_get_timer_hz(), rte_get_timer_cycles(), common_params->trace_queue );

  return ( qelem_t* ) gsp;
};

//------------------------------------------------------------------------------------------------------
// optional opts
//------------------------------------------------------------------------------------------------------

static inline unsigned qelem_gsp_enqueue_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
  QCAST ( gsp );
  tsc_cycles_t now = rte_get_timer_cycles();
  
  unsigned num_taildrops = 0;
  unsigned num_aqm_drops = 0;

  if ( lolliq_qlen_bytes ( &gsp->queue ) == 0 )
  {
    gsp->state.timeout_expiry = now;
    
    // reset overflow state
    if ( gsp->state.astate == OVERFLOW )
    {
      gsp->state.astate = DRAIN;
    }
  }

  // enqueue all packets before last, if we hit taildrop -> bad
  if ( num_pkts > 1 )
  {
    unsigned num_enq = lolliq_enqueue_burst ( &gsp->queue, pkts, num_pkts-1 );
    if ( unlikely ( num_enq < num_pkts - 1 ) )
    {
      // on taildrops enter overflow state
      gsp->state.astate = OVERFLOW;

      unsigned pkt_num;
      for ( pkt_num = num_enq ; pkt_num < num_pkts-1; pkt_num++ )
      {
        rte_pktmbuf_free ( pkts[pkt_num] );
      };
      
      num_taildrops = (num_pkts - 1) - num_enq;

      // TODO: somehow return from here
      // TODO: drop last pkt
    }
  }

  // gsp operation for last pkt
  struct rte_mbuf* pkt = pkts[num_pkts-1];

  if ( lolliq_qlen_bytes ( &gsp->queue ) > gsp->params.threshold && now > gsp->state.timeout_expiry )
  {
    rte_pktmbuf_free ( pkt );
    gsp->state.timeout_expiry = now + gsp->state.interval;
    
    num_aqm_drops = 1;
  }
  else
  {
    if ( !lolliq_enqueue_pkt ( &gsp->queue, pkt ) )
    {
      gsp->state.astate = OVERFLOW;
      rte_pktmbuf_free ( pkt );
      
      num_taildrops++;
    }
  }

  // update state
  if ( gsp->state.astate == DRAIN && lolliq_qlen_bytes ( &gsp->queue ) > gsp->params.threshold )
  {
    gsp->state.astate = CLEAR;
  }
  
  gsp_on_pkts_enqueue(gsp->state.trace_queue, now, num_aqm_drops, num_taildrops);
  return num_pkts;
}

// static unsigned qelem_gsp_peek_burst ( qelem_t *qelem, struct rte_mbuf*** burst_ptr )
// {
//   QCAST ( gsp );
//   return lolliq_peek_burst ( &gsp->queue, burst_ptr );
// }
//
// static void qelem_gsp_dequeue_peeked_burst ( qelem_t *qelem, struct rte_mbuf** peeked_burst, unsigned num_pkts )
// {
//   QCAST ( gsp );
//   lolliq_dequeue_peeked_burst ( &gsp->queue, peeked_burst, num_pkts );
// #ifdef WITH_TRACES
//   gsp_on_pkts_dequeue ( gsp->trace_queue, rte_get_timer_cycles(),
//                         lolliq_count ( &gsp->queue ), lolliq_qlen_bytes ( &gsp->queue ), num_pkts
//                       );
// #endif
// 
// }
// 
// static unsigned qelem_gsp_queue_len_bytes ( qelem_t *qelem )
// {
//   QCAST ( gsp );
//   return lolliq_qlen_bytes ( &gsp->queue );
// }
// 
// static unsigned qelem_gsp_queue_len_pkts ( qelem_t *qelem )
// {
//   QCAST ( gsp );
//   return lolliq_count ( &gsp->queue );
// }

//------------------------------------------------------------------------------------------------------
// default opts
//------------------------------------------------------------------------------------------------------

static
void qelem_gsp_tx_flush ( qelem_t *qelem )
{
  QCAST ( gsp );
  tsc_cycles_t now = rte_get_timer_cycles();

  struct rte_mbuf** burst;
  unsigned num_pkts = lolliq_peek_burst ( &gsp->queue, &burst );

  if ( likely ( num_pkts > 0 ) )
  {
    unsigned num_tx = rte_eth_tx_burst ( qelem->port_id, qelem->queue_id, burst, num_pkts );
    if ( num_tx > 0 )
    {
      unsigned qlen_bytes_before = lolliq_qlen_bytes ( &gsp->queue );
      ctrl_gsp_on_pkts_dequeue(&gsp->state,&gsp->params, now, qlen_bytes_before );
      lolliq_dequeue_peeked_burst ( &gsp->queue, burst, num_tx );
      
#if defined WITH_TRACES && WITH_TRACES == 2
      gsp_on_pkts_dequeue(gsp->state.trace_queue, now, lolliq_qlen_bytes ( &gsp->queue ));
#endif      
    }
  }
}

static
void qelem_gsp_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
  QCAST ( gsp );

  // enqueue packets
  qelem_gsp_enqueue_burst ( qelem, pkts, num_pkts );

  // if there is at least burst packets in the queue, transmit packets
  if ( likely ( lolliq_count ( &gsp->queue ) >= gsp->qelem.min_send_burst ) )
  {
    qelem_gsp_tx_flush ( qelem );
  }
}

static
void qelem_gsp_free ( qelem_t* qelem )
{
  rte_free ( qelem );
};

//-----------------------------------------------------------------------------------------------------------
// trace log
//-----------------------------------------------------------------------------------------------------------

#ifdef WITH_TRACES
static
void* gsp_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
  LOG_LINE();
  return trace_event_queue_add ( "gsp", common_params->trace_queue_size, sizeof ( gsp_event_t ), gsp_event_log );
}
#endif

//--------------------------------------------------------------------------------------------------------

qelem_factory_t qelem_gsp_factory =
{
  .qelem_alloc = qelem_gsp_alloc,
#ifdef WITH_TRACES
  .trace_queue_alloc = gsp_trace_queue_alloc,
#endif
};

static qelem_ops_t qelem_gsp_ops =
{
  .qelem_tx_burst = qelem_gsp_tx_burst,
  .qelem_tx_flush = qelem_gsp_tx_flush,
  .qelem_free = qelem_gsp_free,

//   .enqueue_burst = qelem_gsp_enqueue_burst,
//   .peek_burst = qelem_gsp_peek_burst,
//   .dequeue_peeked_burst = qelem_gsp_dequeue_peeked_burst,
//   .queue_len_bytes = qelem_gsp_queue_len_bytes,
//   .queue_len_pkts = qelem_gsp_queue_len_pkts,
};

