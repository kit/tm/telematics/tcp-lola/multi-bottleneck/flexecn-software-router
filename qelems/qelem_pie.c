#include "qelems.h"
#include "../rte_include.h"
#include "qlog.h"
#include "macros/common.h"
#include "qdt/lolliq.h"

//-----------------------------------------------------------
// params: basic/extened ; use delay or drain rate estimation

#define PIE_USE_SOJOURN
#define PIE_USE_EXTENDED

//-----------------------------------------------------------

#ifdef PIE_USE_EXTENDED
#include "qtrl/pie_extended_floats.h"
#else
#include "qtrl/pie_basic_floats.h"
#endif



static qelem_ops_t qelem_pie_ops;

typedef struct __qelem_pie
{
  qelem_t qelem;

  pie_params_t pie_params;
  pie_state_t pie_state;

  lolliqueue_t queue;
} qelem_pie_t __rte_cache_aligned;


static
qelem_t* qelem_pie_alloc ( const  qelem_params_t* common_params, const  void* qelem_params )
{
  LOG_INIT ( "allocating qelem_pie for port %d queue %d \n", common_params->port_id, common_params->tx_queue_id );

  QPARAMSCAST ( pie );
  qelem_pie_t* pie = 0;

  size_t queue_mem_size = lolliq_entries_sizeof ( params->qlen_pkts );
  QZMALLOC ( pie, qelem_pie_t, queue_mem_size );

  qelem_init ( ( qelem_t* ) pie, common_params, &qelem_pie_ops );
  lolliq_init ( &pie->queue, params->qlen_pkts );

  ctrl_pie_init ( &pie->pie_state, &pie->pie_params, rte_get_tsc_hz(), params->ref_delay, params->t_update,
                  params->alpha, params->beta, common_params->trace_queue );

  return ( qelem_t* ) pie;
};

//-----------------------------------------------------------------------------------------------------------
// helpers opts
//-----------------------------------------------------------------------------------------------------------

static inline
unsigned pie_enqueue_burst ( qelem_pie_t* pie, struct rte_mbuf **pkts,  uint32_t num_pkts, uint64_t now )
{
  unsigned pkt_num, num_enq = 0, num_taildrop = 0,num_aqm_drop = 0;
  for ( pkt_num = 0; pkt_num < num_pkts; pkt_num++ )
  {
    struct rte_mbuf* pkt = pkts[pkt_num];

    // if there is no space we can't do anything :)
    if ( unlikely ( lolliq_free_count ( &pie->queue ) == 0 ) )
    {
      rte_pktmbuf_free ( pkt );
      num_taildrop++;
      ctrl_pie_on_taildrop ( &pie->pie_state, &pie->pie_params );
      continue;
    }

    boolean_t should_drop = ctrl_pie_on_pkt_enqueue ( &pie->pie_state, &pie->pie_params, now,
                            lolliq_qlen_bytes ( &pie->queue ) );
    if ( unlikely ( should_drop ) )
    {
      rte_pktmbuf_free ( pkt );
      num_aqm_drop++;
    }
    else
    {
      MBUF2TS_PTR(pkt)->timestamp = now;
      lolliq_enqueue_pkt ( &pie->queue, pkt );
      num_enq++;
    }
  }
#ifdef PIE_USE_SOJOURN
  pie_sojourn_on_pkts_enqueue(pie->pie_state.trace_queue, now, num_aqm_drop, num_taildrop);
#else
  pie_on_pkts_enqueue ( pie->pie_state.trace_queue, now, lolliq_qlen_bytes ( &pie->queue ), num_aqm_drop, num_taildrop );
#endif
  
  return num_enq;
}

static inline
void pie_send_burst ( qelem_pie_t* pie, tsc_cycles_t now )
{
  struct rte_mbuf** burst;
  unsigned num_pkts = lolliq_peek_burst ( &pie->queue, &burst );

  if ( num_pkts > 0 )
  {

    unsigned num_tx = rte_eth_tx_burst ( pie->qelem.port_id, pie->qelem.queue_id, burst, num_pkts );

    if ( num_tx > 0 )
    {
#ifdef PIE_USE_SOJOURN
      // get sojourn of last sent packet (seems to conform to pie spec)
      timestamp_metadata_t* metadata = MBUF2TS_PTR ( burst[num_tx-1] );
      tsc_cycles_t pkt_sojourn_time = now - metadata->timestamp;
      ctrl_pie_on_pkt_dequeue ( &pie->pie_state, &pie->pie_params, pkt_sojourn_time );

      // dequeue packets
      lolliq_dequeue_peeked_burst ( &pie->queue, burst, num_tx );

      // add trace event
      pie_sojourn_on_pkts_dequeue ( pie->pie_state.trace_queue, now, pkt_sojourn_time );

#else
      // get delta in queue length
      unsigned old_queue_len = lolliq_qlen_bytes ( &pie->queue );
      lolliq_dequeue_peeked_burst ( &pie->queue, burst, num_tx );
      unsigned new_queue_len = lolliq_qlen_bytes ( &pie->queue );

      // update pie and generate trace point
      ctrl_pie_on_pkt_dequeue ( &pie->pie_state, &pie->pie_params, now, new_queue_len, old_queue_len - new_queue_len );
      pie_on_pkts_dequeue ( pie->pie_state.trace_queue, now, new_queue_len );
#endif


    }
  }
}

//-----------------------------------------------------------------------------------------------------------
// default opts
//-----------------------------------------------------------------------------------------------------------

static
void qelem_pie_tx_flush ( qelem_t *qelem )
{
  QCAST ( pie );
  tsc_cycles_t now = rte_get_timer_cycles();

  // flush queue
  if ( lolliq_count ( &pie->queue ) > 0 )
  {
    pie_send_burst ( pie, now );
  } 
  else {
    ctrl_pie_on_pkt_dequeue ( &pie->pie_state, &pie->pie_params, 0 );
  }

  // update pie
  ctrl_pie_update_if_needed ( &pie->pie_state, &pie->pie_params, now, lolliq_qlen_bytes ( &pie->queue ) );
}

static
void qelem_pie_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
  QCAST ( pie );

// #if defined  WITH_TRACES && WITH_TRACES == 2 
  tsc_cycles_t now = rte_get_timer_cycles();
// #else
//   tsc_cycles_t now = 0;
// #endif

  // enqueu packets
  pie_enqueue_burst ( pie, pkts, num_pkts, now );

  // if there are enough packet to send -> send them
  if ( likely ( lolliq_count ( &pie->queue ) >= pie->qelem.min_send_burst ) )
  {
// #if !(defined WITH_TRACES) || WITH_TRACES == 1
//     now = rte_get_timer_cycles();
// #endif
    pie_send_burst ( pie, now );
  }
}

static
void qelem_pie_free ( qelem_t* qelem )
{
  qelem_pie_t* pie = ( qelem_pie_t* ) qelem;
  rte_free ( pie );
};

//-----------------------------------------------------------------------------------------------------------
// optional opts
//-----------------------------------------------------------------------------------------------------------

static
unsigned qelem_pie_enqueue_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
  QCAST ( pie );

#if defined  WITH_TRACES && WITH_TRACES == 2
  tsc_cycles_t now = rte_get_timer_cycles();
#else
  tsc_cycles_t now = 0;
#endif

  // TODO very big question here - if there are no packets , tb will not call for drop probability updates ...
  // probably need it here

  return pie_enqueue_burst ( pie, pkts, num_pkts, now );
};

static
unsigned qelem_pie_peek_burst ( qelem_t *qelem, struct rte_mbuf*** burst_ptr )
{
  QCAST ( pie );
  return lolliq_peek_burst ( &pie->queue, burst_ptr );
}

// static
// void qelem_pie_dequeue_peeked_burst ( qelem_t *qelem, struct rte_mbuf** burst_ptr, unsigned num_pkts )
// {
//   QCAST ( pie );
// 
//   if ( likely ( num_pkts > 0 ) )
//   {
//     tsc_cycles_t now = rte_get_timer_cycles();
// 
//     unsigned old_queue_len = lolliq_qlen_bytes ( &pie->queue );
//     lolliq_dequeue_peeked_burst ( &pie->queue, burst_ptr, num_pkts );
//     unsigned new_queue_len = lolliq_qlen_bytes ( &pie->queue );
// 
//     // TODO 3rd arg -> new_queue_len or old_queue_len;
// 
//     ctrl_pie_on_pkt_dequeue ( &pie->pie_state, &pie->pie_params, now, new_queue_len, old_queue_len - new_queue_len );
//     ctrl_pie_update_if_needed ( &pie->pie_state, &pie->pie_params, now, new_queue_len );
//   }
// }


static
unsigned qelem_pie_queue_len_bytes ( qelem_t * qelem )
{
  QCAST ( pie );
  return lolliq_qlen_bytes ( &pie->queue );
}

static
unsigned qelem_pie_queue_len_pkts ( qelem_t * qelem )
{
  QCAST ( pie );
  return lolliq_count ( &pie->queue );
}

//-----------------------------------------------------------------------------------------------------------
// trace log
//-----------------------------------------------------------------------------------------------------------

#ifdef WITH_TRACES
static
void* pie_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
  return trace_event_queue_add ( "pie", common_params->trace_queue_size, sizeof ( pie_event_t ), pie_event_log );
}
#endif

//---------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------

qelem_factory_t qelem_pie_factory =
{
  .qelem_alloc = qelem_pie_alloc,
#ifdef WITH_TRACES
  .trace_queue_alloc = pie_trace_queue_alloc,
#endif
};

static qelem_ops_t qelem_pie_ops =
{
  .qelem_tx_burst = qelem_pie_tx_burst,
  .qelem_tx_flush = qelem_pie_tx_flush,
  .qelem_free = qelem_pie_free,

  .enqueue_burst = qelem_pie_enqueue_burst,
  .peek_burst = qelem_pie_peek_burst,
//   .dequeue_peeked_burst = qelem_pie_dequeue_peeked_burst,
  .queue_len_bytes = qelem_pie_queue_len_bytes,
  .queue_len_pkts = qelem_pie_queue_len_pkts,
};

