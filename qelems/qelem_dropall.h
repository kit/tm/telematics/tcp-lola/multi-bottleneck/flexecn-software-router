#ifndef	QELEM_DROPALL
#define	QELEM_DROPALL

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */
  
typedef struct {

} qelem_dropall_params_t;  
  
extern qelem_factory_t qelem_dropall_factory;    

  
#ifdef __cplusplus
}
#endif /* __cplusplus */  

#endif