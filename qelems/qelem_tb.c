#include "qelems.h"
#include "../rte_include.h"
#include "qlog.h"
#include "macros/helpers.h"

#define ETHERNET_OVERHEAD (4 + 4 + 8 + 12) // vlan tag + crc + (preamble+sof) + (ifs)

static qelem_ops_t qelem_tb_ops;

typedef struct __qelem_tb {
    qelem_t qelem;

    double max_tokens;
    double tokens;
    unsigned min_send_burst;
    double rate_tsc;
    tsc_cycles_t last_update;

    qelem_t* child;

} qelem_tb_t __rte_cache_aligned;

//---------------------------------------------------------------------------------------------------------------

static
qelem_t* qelem_tb_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
    LOG_INIT ( "allocating qelem_tb for port %d \n", common_params->port_id );

    QPARAMSCAST ( tb );
    qelem_tb_t* tb = 0;

    // create child, nothig can go wrong with this one
    qelem_t* child = params->qelem_child_factory->qelem_alloc ( common_params, params->qelem_child_params );
    if ( !child->v_table->enqueue_burst || !child->v_table->peek_burst
            || !child->v_table->dequeue_peeked_burst || !child->v_table->queue_len_bytes
            || !child->v_table->queue_len_pkts
       ) {
        rte_panic ( "child qelem doesn't implement optional ops -- can't use it" );
    }


    QZMALLOC ( tb, qelem_tb_t, 0 );
    qelem_init ( ( qelem_t* ) tb, common_params, &qelem_tb_ops );
    tb->child = child;
    double rate_bytes_s = params->rate_bits_s / 8.0;
    tb->rate_tsc = rate_bytes_s / rte_get_tsc_hz();
    tb->max_tokens = params->max_burst;
    tb->tokens = tb->max_tokens;
    tb->min_send_burst = common_params->min_send_burst;


    return ( qelem_t* ) tb;
};

//------------------------------------------------------------------------------------------------------
// default opts
//------------------------------------------------------------------------------------------------------

static
void qelem_tokenbucket_tx_flush ( qelem_t *qelem )
{

    qelem_tb_t* tokenbucket = ( qelem_tb_t* ) qelem;

    // note - codel drops packets in front of the queue
    // that is qelem_peek_burst will drop packets from the queue
    // and they should not be considered in the tocket budget
    //   keep this in mind when calculating queue lengthes


    struct rte_mbuf** burst;
    unsigned max_num_pkts =  qelem_peek_burst ( tokenbucket->child, &burst );
    if ( max_num_pkts == 0 ) {
        return;
    }

//     LOG_VAR ( max_num_pkts, "%u" );


    if ( tokenbucket->tokens < tokenbucket->max_tokens ) {
        // update tokens
        tsc_cycles_t now = rte_get_tsc_cycles();
        tsc_cycles_t delta = now - tokenbucket->last_update;

        tokenbucket->tokens += delta * tokenbucket->rate_tsc;
        if ( tokenbucket->tokens > tokenbucket->max_tokens ) {
            tokenbucket->tokens = tokenbucket->max_tokens;
        }

        tokenbucket->last_update = now;
    }

//     LOG_VAR(tokenbucket->tokens, "%f");

    if ( tokenbucket->tokens > 0 ) {

        unsigned num_pkts;
        int num_tokens = tokenbucket->tokens;
        for ( num_pkts = 0; num_pkts < max_num_pkts; num_pkts++ ) {
            num_tokens -= burst[num_pkts]->data_len + ETHERNET_OVERHEAD ;
            if ( num_tokens < 0 ) {
                break;
            }
        }

        if ( num_pkts > 0 ) {

//             LOG_VAR ( burst[0], "%p" );
//             LOG_VAR ( burst[0]->buf_addr, "%p" );
//             LOG_VAR ( num_pkts, "%u" );

            unsigned num_tx = rte_eth_tx_burst ( tokenbucket->qelem.port_id, tokenbucket->qelem.queue_id, burst, num_pkts );
            if ( num_tx > 0 ) {
                unsigned qlen_before = qelem_queue_len_bytes ( tokenbucket->child );
                qelem_dequeue_peeked_burst ( tokenbucket->child, burst, num_tx );
                unsigned qlen_after = qelem_queue_len_bytes ( tokenbucket->child );
                unsigned delta_len = qlen_before - qlen_after;

                tokenbucket->tokens -= ( int ) delta_len;

//                 LOG_VAR ( qelem->tokens, "%f" );
            }
        }
    }
}

static
void qelem_tokenbucket_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
//     LOG_LINE();

    qelem_tb_t* tokenbucket = ( qelem_tb_t* ) qelem;
    QELEM_PKTS_IN ( tokenbucket, num_pkts );

    unsigned num_enq = qelem_enqueue_burst ( tokenbucket->child, pkts, num_pkts );
    QELEM_TAIL_DROP ( tokenbucket, num_pkts - num_enq );

    if ( qelem_queue_len_pkts ( tokenbucket->child ) > tokenbucket->min_send_burst ) {
        qelem_tokenbucket_tx_flush ( qelem );
    }
}


/*
//---------------------------------------------------------------------------------------------------------------
static
unsigned qelem_tb_enqueue_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts , unsigned hw_queue_index )
{
    qelem_tb_t* qelem0 = ( qelem_tb_t* ) qelem;
    return qelem_enqueue_burst ( qelem0->child, pkts, num_pkts, hw_queue_index );
};

static
void qelem_tb_xmit ( qelem_t *_qelem, unsigned hw_queue_index )
{
    qelem_tb_t* qelem = ( qelem_tb_t* ) _qelem;

    // if queue is empty do nothing
    unsigned qlen_before = qelem->child->v_table->queue_len_bytes ( qelem->child );
    if ( qlen_before == 0 ) {
        return;
    }

    // if queue is full and there are less tokens than max rate
    // update tokens

    if ( qelem->tokens < qelem->max_tokens ) {
        // update tokens
        tsc_cycles_t now = rte_get_tsc_cycles();
        tsc_cycles_t delta = now - qelem->last_update;

        qelem->tokens += delta * qelem->rate_tsc;
        if ( qelem->tokens > qelem->max_tokens ) {
            qelem->tokens = qelem->max_tokens;
        }

        qelem->last_update = now;
    }

//     LOG_VAR(qelem->tokens, "%d");

    // if token size is poritive -> enqueue burst
    if ( qelem->tokens > 0 ) {

        struct rte_mbuf** burst;
        unsigned max_num_pkts = qelem->child->v_table->peek_burst ( qelem->child, &burst );

        unsigned num_pkts;
        int num_tokens = qelem->tokens;
        for ( num_pkts = 0; num_pkts < max_num_pkts; num_pkts++ ) {
            num_tokens -= burst[num_pkts]->pkt_len + ETHERNET_OVERHEAD ;
            if ( num_tokens < 0 ) {
                break;
            }
        }

        if ( num_pkts > 0 ) {

            unsigned num_tx = rte_eth_tx_burst ( qelem->qelem.port_id, hw_queue_index, burst, num_pkts );
            if ( num_tx > 0 ) {
                qelem->child->v_table->dequeue_peeked_burst ( qelem->child, num_tx );

                unsigned qlen_after = qelem->child->v_table->queue_len_bytes ( qelem->child );
                unsigned delta_len = qlen_before - qlen_after;

//                 LOG_VAR ( delta_len, "%u" );

                qelem->tokens -= ( int ) delta_len;

//                 LOG_VAR ( qelem->tokens, "%f" );
            }
        }

    }
}*/

static
void qelem_tb_free ( qelem_t* qelem )
{
    qelem_tb_t* qelem0 = ( qelem_tb_t* ) qelem;

    qelem_free ( qelem0->child );
    rte_free ( qelem0 );
};

//---------------------------------------------------------------------------------------------------------------
#ifdef WITH_TRACES
#include "traces/trace_event_queue.h"

static
void* tb_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
    QPARAMSCAST ( tb );

    if ( !params->qelem_child_factory->trace_queue_alloc ) {
        return 0;
    }

    return params->qelem_child_factory->trace_queue_alloc ( common_params, params->qelem_child_params );
}

#endif

//---------------------------------------------------------------------------------------------------------------

qelem_factory_t qelem_tb_factory = {
    .qelem_alloc = qelem_tb_alloc,
#ifdef WITH_TRACES
    .trace_queue_alloc = tb_trace_queue_alloc,
#endif
};

static qelem_ops_t qelem_tb_ops = {
    .qelem_tx_burst = qelem_tokenbucket_tx_burst,
    .qelem_tx_flush = qelem_tokenbucket_tx_flush,
    .qelem_free = qelem_tb_free,
};

