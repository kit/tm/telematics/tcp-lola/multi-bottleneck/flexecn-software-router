#include "qelem.h"
#include "../rte_include.h"
#include "qelem_dropall.h"

#define BURST_SIZE 32

typedef struct __qelem_dropall
{
  qelem_t qelem;
} qelem_dropall_t;

static qelem_ops_t qelem_dropall_ops;    

static
qelem_t* qelem_dropall_alloc (const qelem_params_t* common_params, const void* qelem_params )
{

  qelem_dropall_t* qelem = rte_zmalloc ( "qelem_dropall", sizeof ( qelem_dropall_t ), RTE_CACHE_LINE_SIZE );

  qelem->qelem.v_table = &qelem_dropall_ops;
  return ( qelem_t* ) qelem;
};

static
void qelem_dropall_free ( qelem_t* qelem )
{
  rte_free ( qelem );
};

static
unsigned qelem_dropall_enqueue_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts , unsigned hw_queue_index )
{
  unsigned index;
  for ( index = 0; index < num_pkts; index++ )
  {
    rte_pktmbuf_free ( pkts[index] );
  }
  
  return num_pkts;
};

// static
// int qelem_dropall_enqueue ( qelem_t *qelem, struct rte_mbuf *pkt , unsigned hw_queue_index )
// {
//   // TODO: do this in bursts?
//   rte_pktmbuf_free(pkt);
//   return 1;
// 
// };

static
void qelem_dropall_xmit ( qelem_t *qelem, unsigned hw_queue_index ){
  // do nothing
}


qelem_factory_ops_t qelem_dropall_factory = {
  .qelem_alloc = qelem_dropall_alloc,
  .qelem_free = qelem_dropall_free,
};

static qelem_ops_t qelem_dropall_ops =
{
//   .enqueue = qelem_dropall_enqueue,
  .enqueue_burst = qelem_dropall_enqueue_burst,
  .xmit = qelem_dropall_xmit,
};
