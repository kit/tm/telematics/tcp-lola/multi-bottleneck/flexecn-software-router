#include "qdev.h"
#include "../rte_include.h"
#include "qlog.h"
#include "traces/trace_event_queue.h"

qdev_port_t qdev_ports[RTE_MAX_ETHPORTS];

void qdev_port_init ( uint8_t port_id, uint16_t num_tx_queues, int socket_id )
{
    qdev_ports[port_id].qelems = ( qelem_t** ) rte_zmalloc_socket ( 0, sizeof ( qelem_t* ) * num_tx_queues, 0, socket_id );
}

int qdev_queue_setup ( uint8_t port_id, uint16_t tx_queue_id, uint16_t nb_tx_desc,
                       unsigned int socket_id, const struct rte_eth_txconf* tx_conf,
                       unsigned min_send_burst, qelem_factory_t* qelem_factory, const void* qelem_params )
{
    int ret;

    // setup queue on ethdev
    ret = rte_eth_tx_queue_setup ( port_id, tx_queue_id, nb_tx_desc, rte_eth_dev_socket_id ( port_id ), tx_conf );
    if ( ret < 0 ) {
        return ret;
    }

    // create qelem
    qelem_params_t params;
    params.port_id = port_id;
    params.tx_queue_id = tx_queue_id;
    params.min_send_burst = min_send_burst;

#ifdef WITH_TRACES
//     if (port_id == 3) { // FIXME config
        params.trace_queue_size = 30 * 1e6;

        if ( qelem_factory->trace_queue_alloc ) {
            trace_event_queue_t* queue = qelem_factory->trace_queue_alloc( &params, qelem_params );
	    LOG_VAR(queue, "%p");
            if ( !queue ) {
                rte_panic ( "allocating trace_queue failed\n" );
            }
            params.trace_queue = queue;
	    LOG_VAR(params.trace_queue, "%p");
        }
//     }
#endif
    qelem_t* qelem = qelem_factory->qelem_alloc ( &params, qelem_params );

    if ( !qelem ) {
        return -1;
    }

    qdev_ports[port_id].qelems[tx_queue_id] = qelem;

    return 0;
}

