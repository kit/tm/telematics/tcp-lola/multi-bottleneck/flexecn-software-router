#include "qelems.h"
#include "../rte_include.h"
#include "qlog.h"
#include "macros/common.h"
#include "qdt/lolliq.h"

// TODO: macro - selector for 

#include "qtrl/codel.h"
// #include "qtrl/codel_cake.h"

static qelem_ops_t qelem_codel_ops;

typedef struct {
    qelem_t qelem;

    codel_params_t codel_params;
    codel_state_t codel_state;

    lolliqueue_t queue;
} qelem_codel_t;

typedef struct {
    tsc_cycles_t timestamp;
} codel_metadata_t;


//-----------------------------------------------------------------------------------------------------------

static
qelem_t* qelem_codel_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
    LOG_INIT ( "allocating qelem_codel for port %d \n", common_params->port_id );

    QPARAMSCAST ( codel );
    qelem_codel_t* codel = 0;

    size_t queue_mem_size = lolliq_entries_sizeof ( params->qlen_pkts);
    QZMALLOC ( codel, qelem_codel_t, queue_mem_size );

    qelem_init ( ( qelem_t* ) codel, common_params, &qelem_codel_ops );
    lolliq_init ( &codel->queue, params->qlen_pkts);
    
    LOG_PARAM ( common_params->trace_queue, "%p" );

    ctrl_codel_init ( &codel->codel_state, &codel->codel_params,
                      params->interval_sec, params->target_sec,
                      params->mtu * common_params->min_send_burst , rte_get_tsc_hz(), TRUE,
                      common_params->trace_queue
                    );

    return ( qelem_t* ) codel;
};

//-----------------------------------------------------------------------------------------------------------
// helpers opts
//-----------------------------------------------------------------------------------------------------------

static inline
unsigned codel_enqueue_burst ( qelem_codel_t *codel, struct rte_mbuf **pkts,  uint32_t num_pkts , tsc_cycles_t now )
{
    unsigned num_enq = lolliq_enqueue_burst ( &codel->queue, pkts, num_pkts );

    unsigned pkt_num;
    for ( pkt_num = 0 ; pkt_num < num_enq; pkt_num++ ) {
//         codel_metadata_t* metadata = ( codel_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkts[pkt_num] , 0 );
      codel_metadata_t* metadata = ( codel_metadata_t* ) (pkts[pkt_num]->buf_addr);  
      metadata->timestamp = now;
    };

    if ( unlikely ( num_enq < num_pkts ) ) {
        for ( pkt_num = num_enq; pkt_num < num_pkts; pkt_num++ ) {
            struct rte_mbuf* pkt = pkts[pkt_num];
            rte_pktmbuf_free ( pkt );
        };
    }
    
    codel_on_taildrop(codel->codel_state.trace_queue, now, num_pkts-num_enq);

    return num_enq;
}

static inline
void codel_drop_packets ( qelem_codel_t * codel, tsc_cycles_t now )
{
    while ( 1 ) {
        struct rte_mbuf* pkt =  lolliq_head ( &codel->queue );

        if ( !pkt ) {
            ctrl_codel_on_pkt_dequeue ( &codel->codel_state, &codel->codel_params, 0, now, 0 );
            return;
        } else {
//             codel_metadata_t* metadata = ( codel_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkt, 0 );
	  codel_metadata_t* metadata = ( codel_metadata_t* ) (pkt->buf_addr); 
	  boolean_t should_drop = ctrl_codel_on_pkt_dequeue ( &codel->codel_state, &codel->codel_params,
                                    metadata->timestamp, now,
                                    lolliq_qlen_bytes ( &codel->queue ) - pkt->data_len);
            if ( !should_drop ) {
                return;
            } else {
// #if defined WITH_TRACES && WITH_TRACES == 2
//                 unsigned sojourn_time = now - metadata->timestamp;
//                 codel_on_pkt_dequeue ( codel->codel_state.trace_queue, now, sojourn_time, lolliq_qlen_bytes ( &codel->queue ) );
// #e -ndif
                lolliq_drop_head ( &codel->queue );
            }
        }
    }
}

static inline
void codel_send_burst ( qelem_codel_t * codel, tsc_cycles_t now )
{
//     LOG_LINE();
    struct rte_mbuf** burst;
    unsigned num_deq =  lolliq_peek_burst ( &codel->queue, &burst );
    if ( num_deq > 0 ) {
        unsigned num_tx = rte_eth_tx_burst ( codel->qelem.port_id, codel->qelem.queue_id, burst, num_deq ) ;
        if ( num_tx > 0 ) {
#if defined WITH_TRACES && WITH_TRACES == 2
	  // only log first packet, otherwise toooooooooo many points
	  codel_metadata_t* metadata = ( codel_metadata_t* ) (burst[0]->buf_addr); 
	  unsigned sojourn_time = now - metadata->timestamp;
	  codel_on_pkt_dequeue ( codel->codel_state.trace_queue, now, sojourn_time, lolliq_qlen_bytes ( &codel->queue ) );
#endif
            lolliq_dequeue_peeked_burst ( &codel->queue, burst, num_tx );
        }
    }
}

//-----------------------------------------------------------------------------------------------------------
// default opts
//-----------------------------------------------------------------------------------------------------------

static
void qelem_codel_tx_flush ( qelem_t *qelem )
{
    QCAST ( codel );
    tsc_cycles_t now = rte_get_tsc_cycles();

    codel_drop_packets ( codel , now );

    codel_send_burst ( codel, now );
}

static
void qelem_codel_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    QCAST ( codel );
    tsc_cycles_t now = rte_get_tsc_cycles();

    // enqueue part
    unsigned num_enq = codel_enqueue_burst ( codel, pkts, num_pkts, now );
//     QELEM_TAIL_DROP ( codel, num_pkts-num_enq );

    // dequeue part
    codel_drop_packets ( codel , now );

    if ( unlikely (lolliq_count ( &codel->queue ) < codel->qelem.min_send_burst ) ) {
        return;
    }

    codel_send_burst ( codel, now );
}

//-----------------------------------------------------------------------------------------------------------
// optional opts
//-----------------------------------------------------------------------------------------------------------


static
unsigned qelem_codel_enqueue_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    QCAST ( codel );
    return codel_enqueue_burst ( codel, pkts, num_pkts, rte_get_tsc_cycles() );
};

static
unsigned qelem_codel_peek_burst ( qelem_t *qelem, struct rte_mbuf*** burst_ptr )
{
    QCAST ( codel );
    tsc_cycles_t now = rte_get_tsc_cycles();
    codel_drop_packets ( codel , now );

    // TODO - do I need to run codel on all to-be-sent packets? probably yes, but then,
    // since i don't have an undo there might be a problem ..

    return lolliq_peek_burst ( &codel->queue, burst_ptr );
}

static
void qelem_codel_dequeue_peeked_burst ( qelem_t *qelem, struct rte_mbuf** burst_ptr, unsigned num_pkts )
{
    QCAST ( codel );

#if defined WITH_TRACES && WITH_TRACES == 2
    tsc_cycles_t now = rte_get_timer_cycles();
    unsigned pkt_num;
    for ( pkt_num = 0; pkt_num < num_pkts; pkt_num++ ) {
//         codel_metadata_t* metadata = ( codel_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( burst_ptr[pkt_num], 0 );
        codel_metadata_t* metadata = ( codel_metadata_t* ) ( burst_ptr[pkt_num]->buf_addr );
        unsigned sojourn_time = now - metadata->timestamp;
        codel_on_pkt_dequeue ( codel->codel_state.trace_queue, now, sojourn_time, lolliq_qlen_bytes ( &codel->queue ) );
    }
#endif

    return lolliq_dequeue_peeked_burst ( &codel->queue, burst_ptr, num_pkts );
}


// v2 that makes live reeeeeally easy -> test

//     struct rte_mbuf** burst1 = &burst[0];
//     unsigned num_sent, burst_counter = num_pkts;
//
//     do {
//         num_sent = rte_eth_tx_burst ( codel->qelem.port_id, hw_queue_index, burst1, burst_counter );
// // 	 LOG_VAR(num_sent, "%u");
//         burst_counter-=num_sent;
//         burst1 = &burst1[num_sent];
//     } while ( burst_counter != 0 );
// //
//     tsq_dequeue_peeked ( &codel->queue, ( struct rte_mbuf** ) &burst, num_pkts, num_pkts );
// }



static
unsigned qelem_codel_queue_len_bytes ( qelem_t * qelem )
{
    QCAST ( codel );
    return lolliq_qlen_bytes ( &codel->queue );
}

static
unsigned qelem_codel_queue_len_pkts ( qelem_t * qelem )
{
    QCAST ( codel );
    return lolliq_count ( &codel->queue );
}

static
void qelem_codel_free ( qelem_t* qelem )
{
    qelem_codel_t* codel = ( qelem_codel_t* ) qelem;
    rte_free ( codel );
};

//-----------------------------------------------------------------------------------------------------------
// trace log
//-----------------------------------------------------------------------------------------------------------

#ifdef WITH_TRACES
static
void* codel_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
    return trace_event_queue_add("codel", common_params->trace_queue_size, sizeof(codel_event_t), codel_event_log);
}
#endif
//---------------------------------------------------------------------------------------------------------------

qelem_factory_t qelem_codel_factory = {
    .qelem_alloc = qelem_codel_alloc,
#ifdef WITH_TRACES
    .trace_queue_alloc = codel_trace_queue_alloc,
#endif
};

static qelem_ops_t qelem_codel_ops = {
    .qelem_tx_burst = qelem_codel_tx_burst,
    .qelem_tx_flush = qelem_codel_tx_flush,
    .qelem_free = qelem_codel_free,

    .enqueue_burst = qelem_codel_enqueue_burst,
    .peek_burst = qelem_codel_peek_burst,
    .dequeue_peeked_burst = qelem_codel_dequeue_peeked_burst,
    .queue_len_bytes = qelem_codel_queue_len_bytes,
    .queue_len_pkts = qelem_codel_queue_len_pkts,
};


