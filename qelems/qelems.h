#ifndef	__QELEMS__
#define	__QELEMS__

#include "qelem.h"
//TODO: include "makeconf.h"


#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

/**
 * @file
 * header to include all supported qelems
 */

//-----------------------------------------------------------------------------------------------
// noop aka taildrop with very small buffer
//------------------------------------------------------------------------------------------------

typedef struct
{
  // there are no parameters for qelem_noop
} qelem_noop_params_t;

extern qelem_factory_t qelem_noop_factory;

//-----------------------------------------------------------------------------------------------
// taildrop
//------------------------------------------------------------------------------------------------

typedef struct
{
  unsigned pkt_limit; // one of them must be set
  unsigned byte_limit;
} qelem_taildrop_params_t;

extern qelem_factory_t qelem_taildrop_factory;
extern qelem_factory_t qelem_taildrop_lowspeed_factory;

//-----------------------------------------------------------------------------------------------
// codel
//------------------------------------------------------------------------------------------------

typedef struct
{
  unsigned qlen_pkts;
  seconds_t interval_sec;
  seconds_t target_sec; // default 0.05 * interval_sec
  unsigned mtu; // DEFAULT 1518
} qelem_codel_params_t;

static const qelem_codel_params_t codel_default_params =
{
  .qlen_pkts = 1024,
  .interval_sec = 1000e-3, // 20ms
  .target_sec = 1e-3, // 1 ms = 5% of 20ms
  .mtu = 1518,
};

extern qelem_factory_t qelem_codel_factory;

// extern qelem_factory_t qelem_codel_lowspeed_factory;

// experemental codels
// extern qelem_factory_t qelem_codel_exp1_factory;
// extern qelem_factory_t qelem_codel_exp2_factory;

//-----------------------------------------------------------------------------------------------
// pie
//-----------------------------------------------------------------------------------------------

typedef struct
{
  unsigned qlen_pkts;
  seconds_t ref_delay;
  seconds_t t_update;
  double alpha, beta;
} qelem_pie_params_t;

static const qelem_pie_params_t pie_default_params =
{
  .qlen_pkts = 4096,
  .ref_delay = 20 * 1e-3, // 20ms
  .t_update = 32 * 1e-3, // 32ms - linux
  .alpha = 2, .beta = 20,
};


extern qelem_factory_t qelem_pie_factory;

//-----------------------------------------------------------------------------------------------
// ared
//-----------------------------------------------------------------------------------------------

typedef struct
{
  unsigned qlen_pkts;

  double target_delay; //sec
  double link_rate; // bit/s
  double avg_pkt_size; // bytes
  double w_q; // if w_q == 0  w_q is recalculated according to formula in ARED paper

  double alpha, beta ; // if 0 -> take default

  double min_thresh; // non-default values
  double max_thresh;

  double update_interval_s; // default 0.5

} qelem_ared_params_t;

static const qelem_ared_params_t ared_default_params =
{
  .qlen_pkts = 0, // calculate from max_thresh
  .target_delay = 5e-3, // 5ms from RED paper
  .avg_pkt_size = 1514, // eth mtu
  .w_q = 0,
  .alpha = 0, .beta = 0,
  .min_thresh = 0, .max_thresh = 0,
};

extern qelem_factory_t qelem_ared_factory;

//-----------------------------------------------------------------------------------------------
// FQ-Codel
//-----------------------------------------------------------------------------------------------
#include "qls.h"

typedef struct
{
  unsigned num_flow_queues;
  unsigned quantum;
  unsigned pkt_hard_limit;
  double interval_sec;
  double target_sec;
  unsigned mtu;

  void* qls_params; // parameters for the classifier
  qls_create_op qls_create; // classifier initializer
} qelem_fq_codel_params_t;

static const qelem_fq_codel_params_t fq_codel_default_params =
{
  .num_flow_queues = 1024,
  .quantum = 1518,
  .pkt_hard_limit = 10240,
  .interval_sec = 20e-3, // 20ms
  .target_sec = 1e-3, // 1 ms = 5% of 20ms
  .mtu = 1522,
};

extern qelem_factory_t qelem_fq_codel_factory;

//-----------------------------------------------------------------------------------------------
// gsp (global synchronization protection)
//-----------------------------------------------------------------------------------------------

typedef struct
{
  unsigned qlen_bytes;
  double threshold_ratio; 
  double interval_s;
  unsigned alpha;
  unsigned tau;
} qelem_gsp_params_t;

/*
 * for queue-length based gsp: threshold =  threshold_ratio * qlen_bytes
 * for delay-based based gsp: threshold =  threshold_ratio in secodns
 */

static const qelem_gsp_params_t gsp_default_params = {
  .interval_s = 2 * 100e-3, // 100ms
  .alpha = 2,
  .tau = 5
  
};

extern qelem_factory_t qelem_gsp_factory;
extern qelem_factory_t qelem_delay_gsp_factory;

//-----------------------------------------------------------------------------------------------
// simple token bucket rate limiter
//-----------------------------------------------------------------------------------------------

typedef struct
{
  double rate_bits_s;
  double max_burst;
//   unsigned min_send_burst;

  const void* qelem_child_params;
  qelem_factory_t* qelem_child_factory;
} qelem_tb_params_t;

extern qelem_factory_t qelem_tb_factory;

//-----------------------------------------------------------------------------------------------
// simple delay emulator
//-----------------------------------------------------------------------------------------------

typedef struct
{
  seconds_t delay_sec;
  unsigned min_pkt_size;
  double link_speed_bit_s;
} qelem_delay_params_t;

extern qelem_factory_t qelem_delay_factory;

//-----------------------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
