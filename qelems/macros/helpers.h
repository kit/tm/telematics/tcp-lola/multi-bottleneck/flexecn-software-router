#pragma message "this file is deprecated!"

#include "common.h"

#define SQ_INIT(QELEM,SIZE) \
do { \
    if ( !sq_init ( &QELEM->queue, SIZE ) ) \
    { \
    rte_panic ( "failed to allocate queue for qelem_" #QELEM " \n" ); \
    }\
} while (0)

#define QELEM_INIT0(QNAME) \
do { \
  QNAME = rte_zmalloc ( "qelem_"#QNAME, sizeof ( qelem_##QNAME##_t ), RTE_CACHE_LINE_SIZE ); \
  QNAME->qelem.v_table = &qelem_##QNAME##_ops; \
  QNAME->qelem.port_id = common_params->port_id; \
} while(0)

#define QELEM_INIT(QNAME) \
do { \
  QELEM_INIT0(QNAME); \
  SQ_INIT(QNAME,params->qlen_pkts); \
} while(0)
