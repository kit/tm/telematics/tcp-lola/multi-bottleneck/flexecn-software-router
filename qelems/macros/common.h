
// #define DQTYPE(QNAME) qelem_## QNAME ##_t
// #define DQENQ(QNAME) qelem_## QNAME ##_enqueue_burst

#define TIMESTAMP()  rte_get_tsc_cycles()
#define TIMESTAMP_HZ()  rte_get_tsc_hz()

#define QPARAMSCAST(QNAME) const qelem_##QNAME##_params_t* params = (const qelem_##QNAME##_params_t*) qelem_params

#define QZMALLOC(VAR, TYPE, EXTRA_SIZE) \
do { \
  VAR = (TYPE*) rte_zmalloc(#TYPE, sizeof(TYPE) + EXTRA_SIZE, RTE_CACHE_LINE_SIZE); \
  if(!VAR) { rte_panic("cannot allocate memory for " #TYPE); } \
} while(0);

#define QCAST(QNAME) qelem_ ## QNAME ## _t* QNAME = (qelem_ ## QNAME ## _t*) qelem


typedef struct {
    tsc_cycles_t timestamp;
} timestamp_metadata_t;

#define MBUF2TS_PTR(PKT) ((timestamp_metadata_t*)(PKT->buf_addr))
