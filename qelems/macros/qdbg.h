#ifndef	QELEM_QDBG
#define	QELEM_QDBG

#define LOG(...) RTE_LOG(DEBUG, USER5, __VA_ARGS__);
#define LOG_VAR(var, pfarg) RTE_LOG(DEBUG, USER1, __FILE__ ": %d - " #var " = " pfarg "\n" , __LINE__, var)
#define LOG_FUNC() RTE_LOG(DEBUG, USER1, "%s\n", __func__)
#define LOG_LINE() RTE_LOG(DEBUG, USER1, "in %s at line %d\n", __func__, __LINE__)

#endif 