codel pseudocode with estimator/setpoint/control loop blocks


1. on packet dequeue: 

estimate (packet) -> is_in_persistent_queue / is_not_in_persistent_queue ; set persistent_queue_since
    10: if queue is empty -> we are not in persistent queue state
    20: calculate packet sojourn time
    30: if sojourn time > target -> if unset - set state variable a) first time above = now  , or b) persistent_queue_since = now + interval
    40: if now + interval > first_time_above => set is_in_persistent_queue!
    
    
if the queue is in is_in_persistent_queue then activate controller    
if just changed, reset the controller
    
control loop (packet, is_in_persistent_queue) : send or drop packet
    10: was there a packet that was dropped in current cinterval? 
    20: yes: send packet
    30: no: drop pack
    
goto next packet


TODO: do we reset first_time_above/persistent_queue_since after first indication of sojourn_time < target


