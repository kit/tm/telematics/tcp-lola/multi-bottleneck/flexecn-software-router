#ifndef	PORT_QOS_ETHDEV_WRITER
#define	PORT_QOS_ETHDEV_WRITER

#include "../rte_include.h"
#include "qelem.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */
  
struct rte_port_qos_ethdev_writer_params {
    qelem_t *qelem;
    uint32_t tx_burst_size;
    unsigned hw_queue_index;
};

extern struct rte_port_out_ops rte_port_qos_ethdev_writer_ops;  
  
  
#ifdef __cplusplus
}
#endif /* __cplusplus */  

#endif