#ifndef	__QDT_TSQ__
#define	__QDT_TSQ__

#include "../../rte_include.h"
#include "../qelem_types.h"

#ifdef IN_IDE_PARSER
#define MAX_BURST 32
#endif

#ifndef MAX_BURST
#error please define MAX_BURST
#endif

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

typedef struct
{
  uint32_t tail; // last empty location (this is fifo -> write to tail, read from head)
  uint32_t head; // first filled location
  uint32_t peek_head;

  uint32_t stick_index;

  uint32_t qlen_bytes;
  uint32_t ring_mask;

  struct rte_mbuf** lollipop_stick;
  struct rte_mbuf** ring;

  tsc_cycles_t* timestamps;
} tsqueue_t;

static size_t tsq_entries_sizeof ( unsigned num_entries );
static void tsq_init ( tsqueue_t* tsq, unsigned queue_len );

static unsigned tsq_qlen_bytes ( tsqueue_t* tsq );
static unsigned tsq_count ( tsqueue_t* tsq );
// static unsigned tsq_is_burst_enqueued ( tsqueue_t* tsq );
static uint32_t tsq_free_count ( tsqueue_t* tsq );

static unsigned int tsq_enqueue_pkt ( tsqueue_t* tsq, struct rte_mbuf* pkt, tsc_cycles_t timestamp);
static unsigned int tsq_enqueue_burst ( tsqueue_t* tsq, struct rte_mbuf** pkts, uint32_t num_pkts, tsc_cycles_t timestamp);
static unsigned tsq_peek_burst ( tsqueue_t* tsq, struct rte_mbuf*** pkts );
static void tsq_dequeue_peeked_burst ( tsqueue_t* tsq, struct rte_mbuf** pkts, unsigned num_pkts );
static tsc_cycles_t tsq_get_timestamp(tsqueue_t* tsq, unsigned offset_from_head);

static struct rte_mbuf* tsq_head ( tsqueue_t* tsq );
static void tsq_drop_head ( tsqueue_t* tsq );

//---------------------------------------------------------------------------------------------------------------------------

#define RING_IDX(index) ((index) & tsq->ring_mask)

size_t tsq_entries_sizeof ( unsigned int num_entries )
{
  return ( num_entries + MAX_BURST ) *  sizeof ( struct rte_mbuf* ) + num_entries * sizeof ( tsc_cycles_t ) ;
}

void tsq_init ( tsqueue_t* tsq, unsigned int queue_len )
{
  // assume that tsq is zmalloced
  tsq->tail = 0;
  tsq->head = queue_len - 1;
  tsq->ring_mask = queue_len - 1;

//     tsq->burst_size = burst_size;
  tsq->stick_index = 0;

  tsq->lollipop_stick = ( struct rte_mbuf** ) &tsq[1];
  tsq->ring = &tsq->lollipop_stick[MAX_BURST];

  tsq->timestamps = ( tsc_cycles_t* ) &tsq->lollipop_stick[MAX_BURST + queue_len];

};

unsigned int tsq_qlen_bytes ( tsqueue_t* tsq )
{
  return tsq->qlen_bytes;
}

unsigned int tsq_count ( tsqueue_t* tsq )
{
  return RING_IDX ( tsq->tail - ( tsq->head + 1 ) );
}

uint32_t tsq_free_count ( tsqueue_t* tsq )
{
  return  RING_IDX ( tsq->head - tsq->tail );
}

unsigned int tsq_enqueue_pkt ( tsqueue_t* tsq, struct rte_mbuf* pkt, tsc_cycles_t timestamp )
{
  if ( tsq_free_count ( tsq ) == 0 )
  {
    return 0;
  }

  // append packet to the tail
  tsq->ring[tsq->tail] = pkt;
  tsq->timestamps[tsq->tail] = timestamp;

  // update bytes-count
#ifndef NO_BYTES_COUNTER
  tsq->qlen_bytes += pkt->data_len;
#endif

  // update tail
  tsq->tail = RING_IDX ( tsq->tail + 1 );
  return 1;
}

static unsigned tsq_enqueue_burst ( tsqueue_t* tsq, struct rte_mbuf** pkts, uint32_t num_pkts,  tsc_cycles_t timestamp)
{
  // calculate number of packets to insert

  unsigned free_space = tsq_free_count ( tsq );

  if ( unlikely ( free_space < num_pkts ) )
  {
    if ( unlikely ( free_space == 0 ) )
    {
      return 0;
    }
    else
    {
      num_pkts = free_space;
    }
  }

  // enqueue packets starting from tail

  uint32_t ring_size = tsq->ring_mask + 1;
  uint32_t ring_idx =  tsq->tail;

  unsigned pkt_num;
  if ( likely ( ring_idx + num_pkts < ring_size ) )
  {
    // gcc doesn't verctorize this, and the macro in rte_ring also ... :(
    // so do memcpy instead
    memcpy ( &tsq->ring[ring_idx], pkts, num_pkts * sizeof ( void* ) );
    // we can't memset with 64 byte values :()
    for ( pkt_num = 0; pkt_num < num_pkts; pkt_num++, ring_idx++ )
    {
      tsq->timestamps[ring_idx] = timestamp;
    }

  }
  else
  {
    for ( pkt_num = 0; ring_idx < ring_size; pkt_num++, ring_idx++ )
    {
      tsq->ring[ring_idx] = pkts[pkt_num];
      tsq->timestamps[ring_idx] = timestamp;
    }
    for ( ring_idx = 0; pkt_num < num_pkts; pkt_num++,ring_idx++ )
    {
      tsq->ring[ring_idx] = pkts[pkt_num];
      tsq->timestamps[ring_idx] = timestamp;
    }
  }

#ifndef NO_BYTES_COUNTER
  for ( pkt_num = 0; pkt_num < num_pkts; pkt_num++ )
  {
    tsq->qlen_bytes += pkts[pkt_num]->data_len;
  }
#endif


  tsq->tail = RING_IDX ( tsq->tail + num_pkts );
  return num_pkts;

  // hope this works :)
}

unsigned int tsq_peek_burst ( tsqueue_t* tsq, struct rte_mbuf*** pkts )
{
  // dequeue

  uint32_t num_entries = tsq_count ( tsq );

//     LOG_VAR ( num_entries, "%u" );
//     LOG_VAR ( tsq->head, "%u" );
//     LOG_VAR ( tsq->tail, "%u" );

  uint32_t num_pkts = MAX_BURST;

  if ( unlikely ( num_pkts > num_entries ) )
  {
    if ( unlikely ( num_entries == 0 ) )
    {
      return 0;
    }
    num_pkts = num_entries;
  }

  const uint32_t ring_size = tsq->ring_mask + 1;
  uint32_t ring_idx = RING_IDX ( tsq->head + 1 );

  if ( likely ( ring_idx + num_pkts < ring_size ) )
  {
    *pkts = &tsq->ring[ring_idx];
  }
  else
  {
//         LOG_LINE();

    // copy packets from head to start to the lollipop_stick
    // and return pointer to the lollipop_stick

    unsigned pkts_to_copy = ring_size - ring_idx;
    // assert(pkts_to_copy < tsq->burst_size);

//         LOG_VAR ( pkts_to_copy, "%u" );
//         LOG_VAR ( ring_size, "%u" );
//         LOG_VAR ( ring_idx, "%u" );

    // if packets are not on lollipop_stick, copy them there
    if ( tsq->stick_index == 0 )
    {

//             LOG_LINE();
//             LOG_VAR ( tsq->burst_size - pkts_to_copy, "%u" );

      unsigned pkt_num;
      for ( pkt_num = MAX_BURST - pkts_to_copy; ring_idx < ring_size; pkt_num++,ring_idx++ )
      {
        tsq->lollipop_stick[pkt_num] = tsq->ring[ring_idx];
      }
      tsq->stick_index = pkts_to_copy;
    } /*else {
            if ( tsq->stick_index != pkts_to_copy ) {
                LOG_VAR ( tsq->stick_index, "%u" );
                LOG_VAR ( pkts_to_copy, "%u" );
            }
        }*/



//         LOG_VAR ( tsq->burst_size - tsq->stick_index, "%u" );
    *pkts = &tsq->lollipop_stick[MAX_BURST - tsq->stick_index];
  }

  return num_pkts;
}

/// pkts are to update qlen_bytes; it seems easier to do so, then to traverse ring again
void tsq_dequeue_peeked_burst ( tsqueue_t* tsq, struct rte_mbuf** pkts, unsigned int num_sent_pkts )
{
  // update head
  tsq->head = RING_IDX ( tsq->head + num_sent_pkts );

  // update stick if present
  if ( unlikely ( tsq->stick_index != 0 ) )
  {
    if ( tsq->stick_index > num_sent_pkts )
    {
      tsq->stick_index -= num_sent_pkts;
    }
    else
    {
      tsq->stick_index = 0;
    }
  }

  // update byte queue counter
#ifndef NO_BYTES_COUNTER
  // update byte counter from already dequeued burst
  unsigned pkt_num;
  for ( pkt_num = 0; pkt_num < num_sent_pkts; pkt_num++ )
  {
    tsq->qlen_bytes -= pkts[pkt_num]->data_len;
  }
#endif
}

tsc_cycles_t tsq_get_timestamp(tsqueue_t* tsq, unsigned int offset_from_head)
{
  // TODO: check for element
  uint32_t ring_idx = RING_IDX ( tsq->head + 1 + offset_from_head);
  return tsq->timestamps[ring_idx];
}



struct rte_mbuf* tsq_head ( tsqueue_t* tsq )
{
  uint32_t ring_idx = RING_IDX ( tsq->head + 1 );

  if ( unlikely ( ring_idx == tsq->tail ) )
  {
    return 0;
  }
  else
  {
    return tsq->ring[ring_idx];
  }
}

void tsq_drop_head ( tsqueue_t* tsq )
{
  // assert head is not null (tsq_head was called and returned a packet)
  tsq->head = RING_IDX ( tsq->head + 1 );

  // if there is a peeked version decrease it too
  if ( tsq->stick_index > 0 )
  {
    tsq->stick_index--;
  }

  struct rte_mbuf* pkt = tsq->ring[tsq->head];
#ifndef NO_BYTES_COUNTER
  tsq->qlen_bytes -= pkt->data_len;
#endif
  rte_pktmbuf_free ( pkt );
}



#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
