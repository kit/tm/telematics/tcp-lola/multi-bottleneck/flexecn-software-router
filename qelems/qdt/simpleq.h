#ifndef	__QDT_SIMPLE_QUEUE__
#define	__QDT_SIMPLE_QUEUE__

#include "../../rte_include.h"
#include "../qelem_types.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

/**
 * @file
 *
 * rte_ring + bytes in queue counter
 */

typedef struct {
    struct rte_mbuf* peeked_packets[32];
    unsigned peek_first;
    unsigned peek_last;

    struct rte_ring* ring;
    unsigned qlen_bytes;
} simpleq_t;

static qres_t sq_init ( simpleq_t* sq, unsigned queue_len );
static void sq_destroy ( simpleq_t* sq );

static unsigned sq_len ( simpleq_t* sq );
static unsigned sq_count ( simpleq_t* sq );
static unsigned sq_free_count ( simpleq_t* sq );
static qres_t sq_enqueue ( simpleq_t* sq, struct rte_mbuf* pkt );
static unsigned sq_enqueue_burst ( simpleq_t* sq, struct rte_mbuf** pkts, unsigned num_pkts );

static struct rte_mbuf* sq_head ( simpleq_t* sq );
static void sq_drop_head ( simpleq_t* sq );

static unsigned sq_peek ( simpleq_t* sq, struct rte_mbuf*** burst_ptr );
static void sq_dequeue_peeked ( simpleq_t* sq, unsigned num_pkts );

//---------------------------------------------------------------------------------------------------------------------------
//     extern int instance_counter = 0;

qres_t sq_init ( simpleq_t* sq, unsigned queue_len )
{

#define RING_NAME "sq-ring%u  "
    char sq_ring_name[sizeof ( RING_NAME )];
    sprintf ( sq_ring_name, RING_NAME, (unsigned)(rte_rand() % 15));
//     instance_counter+=1;
#undef RING_NAME


    struct rte_ring* sq_ring  = rte_ring_create ( sq_ring_name, queue_len, rte_socket_id(), RING_F_SP_ENQ | RING_F_SC_DEQ );
    if ( !sq_ring ) {
        return FAILURE;
    }

    sq->ring = sq_ring;
    sq->qlen_bytes = 0;
    // assume memory is zalloced, so everyting else is set to 0

    return SUCCESS;
};

void sq_destroy ( simpleq_t* sq )
{
    unsigned idx;
    for ( idx = sq->peek_first; idx < sq->peek_last; idx++ ) {
        rte_pktmbuf_free ( sq->peeked_packets[idx] );
    }

    rte_free ( sq->ring );
}

qres_t sq_enqueue ( simpleq_t* sq, struct rte_mbuf* pkt )
{
    qres_t res = ( rte_ring_enqueue ( sq->ring, pkt ) == 0 );
    if ( res == SUCCESS ) {
        sq->qlen_bytes += pkt->data_len;
    }

    return res;
};

unsigned sq_enqueue_burst ( simpleq_t* sq, struct rte_mbuf** pkts, unsigned num_pkts )
{
    unsigned nb_enc = rte_ring_enqueue_burst ( sq->ring, ( void** ) pkts, num_pkts );
    
#ifndef NO_BYTES_COUNTER    
    unsigned pkt_num;
    for ( pkt_num = 0 ; pkt_num < nb_enc; pkt_num++ ) {
        sq->qlen_bytes += pkts[pkt_num]->data_len;
    };
#endif    

    return nb_enc;
};

struct rte_mbuf* sq_head ( simpleq_t* sq )
{
    // if there are no packets in burst, dequeue new one
    if ( sq->peek_last - sq->peek_first  == 0 ) {
        unsigned nb_dec = rte_ring_dequeue_burst ( sq->ring, ( void ** ) sq->peeked_packets, 32 );
        sq->peek_first = 0;
        sq->peek_last = nb_dec;
    }

    return sq->peeked_packets[sq->peek_first];
}

void sq_drop_head ( simpleq_t* sq )
{
    if ( sq->peek_last - sq->peek_first  == 0 ) {
        return;    // TODO this should never happen
    }

    rte_pktmbuf_free ( sq->peeked_packets[sq->peek_first] );
    sq->peek_first++;
}


unsigned sq_peek ( simpleq_t* sq, struct rte_mbuf*** burst_ptr )
{
    // if there are no packets in burst, dequeue new one
    if ( sq->peek_last - sq->peek_first  == 0 ) {
        unsigned nb_dec = rte_ring_dequeue_burst ( sq->ring, ( void ** ) sq->peeked_packets, 8 );
        sq->peek_first = 0;
        sq->peek_last = nb_dec;
    }

#ifndef NO_BYTES_COUNTER
    unsigned idx;
    for ( idx = sq->peek_first; idx < sq->peek_last; idx++ ) {
        sq->qlen_bytes -= sq->peeked_packets[idx]->data_len;
    }
#endif    

    *burst_ptr = & ( sq->peeked_packets[sq->peek_first] );
    return sq->peek_last - sq->peek_first;
};

void sq_dequeue_peeked ( simpleq_t* sq, unsigned num_pkts )
{
    sq->peek_first+= num_pkts;

#ifndef NO_BYTES_COUNTER    
    unsigned idx;
    for ( idx = sq->peek_first; idx < sq->peek_last; idx++ ) {
        sq->qlen_bytes += sq->peeked_packets[idx]->data_len;
    }
#endif    
};

unsigned sq_len ( simpleq_t* sq )
{
    return sq->qlen_bytes;
};

unsigned int sq_count ( simpleq_t* sq )
{
    return rte_ring_count ( sq->ring ) + sq->peek_last - sq->peek_first;;
}


unsigned int sq_free_count ( simpleq_t* sq )
{
    return rte_ring_free_count ( sq->ring );
}


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
