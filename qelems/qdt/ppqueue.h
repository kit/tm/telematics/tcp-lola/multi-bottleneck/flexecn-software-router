#ifndef	__QDT_PER_PKT_QUEUE__
#define	__QDT_PER_PKT_QUEUE__

#include "../../rte_include.h"
#include "../qelem_types.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

/**
 * @file
 * per-packet queue
 * contains a ring and one extra packet
 */

typedef struct
{
  struct rte_mbuf* pkt;
  struct rte_ring* ring;
  unsigned qlen_bytes;
  unsigned pkt_bytes;
} ppqueue_t;


static qres_t ppq_init ( ppqueue_t* ppq, unsigned queue_len );
static void ppq_destroy ( ppqueue_t* ppq );

static unsigned ppq_len ( ppqueue_t* ppq );
static unsigned ppq_count ( ppqueue_t* ppq );
static unsigned ppq_free_count ( ppqueue_t* ppq );
static qres_t ppq_enqueue ( ppqueue_t* ppq, struct rte_mbuf* pkt );
static struct rte_mbuf* ppq_peek ( ppqueue_t* ppq );
static void ppq_dequeue_peeked ( ppqueue_t* ppq );

//---------------------------------------------------------------------------------------------------------------------------

qres_t ppq_init ( ppqueue_t* ppq, unsigned queue_len )
{
  static int instance_counter = 0;

#define RING_NAME "ppq-ring%u  "
    char ppq_ring_name[sizeof ( RING_NAME )];
    sprintf ( ppq_ring_name, RING_NAME, (unsigned)(rte_rand() % 15));
//     instance_counter+=1;
#undef RING_NAME


  struct rte_ring* ppq_ring  = rte_ring_create ( ppq_ring_name, queue_len, rte_socket_id(), RING_F_SP_ENQ | RING_F_SC_DEQ );
  if ( !ppq_ring )
  {
    return FAILURE;
  }

  ppq->ring = ppq_ring;
  ppq->pkt = NULL;
  ppq->qlen_bytes = 0;
  ppq->pkt_bytes = 0;
  // assume memory is zalloced, so everyting else is set to 0
  
  return SUCCESS;
};

void ppq_destroy ( ppqueue_t* ppq )
{
  if ( ppq->pkt )
    rte_pktmbuf_free ( ppq->pkt );
  rte_free ( ppq->ring );
}

qres_t ppq_enqueue ( ppqueue_t* ppq, struct rte_mbuf* pkt )
{
  qres_t res = ( rte_ring_enqueue ( ppq->ring, pkt ) == 0 );
  if ( res == SUCCESS )
    ppq->qlen_bytes += pkt->data_len;

  return res;
};

struct rte_mbuf* ppq_peek ( ppqueue_t* ppq )
{
  if ( !ppq->pkt )
  {
    int res = rte_ring_dequeue ( ppq->ring, (void**) &ppq->pkt );
    if ( res != 0 )
    {
      ppq->pkt = 0;
      ppq->pkt_bytes = 0;
    }
    else
    {
      ppq->pkt_bytes = ppq->pkt->data_len;
    }
  }

  return ppq->pkt;
};

void ppq_dequeue_peeked ( ppqueue_t* ppq )
{
  ppq->qlen_bytes-=ppq->pkt_bytes;
  ppq->pkt = 0;
  ppq->pkt_bytes = 0;
};

unsigned ppq_len ( ppqueue_t* ppq )
{
  return ppq->qlen_bytes;
};

unsigned int ppq_count ( ppqueue_t* ppq )
{
  return rte_ring_count(ppq->ring) + (ppq->pkt != NULL);
}


unsigned int ppq_free_count ( ppqueue_t* ppq )
{
  return rte_ring_free_count(ppq->ring);
}




#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
