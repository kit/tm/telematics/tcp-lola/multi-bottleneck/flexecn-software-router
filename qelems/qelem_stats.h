#ifndef	__QELEM_STATS__
#define	__QELEM_STATS__

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

typedef struct
{
  uint64_t pkts_in;
  uint64_t tail_drops;
  uint64_t aqm_drops;
} qelem_stats_t;

#define QELEM_PKTS_IN(_qelem, num_pkts) _qelem->qelem.stats.pkts_in+= num_pkts


// #define QELEM_STAP

#if defined QELEM_STAP
#include <sys/sdt.h>

#define TAILDROP(number) \
  STAP_PROBE1(qelem_stats, qelem_tail_drop, number);

#define TAILDROP_TS(now,number) \
  STAP_PROBE2(qelem_stats, qelem_tail_drop_ts, now, number);


#else

#define TAILDROP(now)
#define TAILDROP_TS(now,number)
#endif

#define QELEM_TAIL_DROP(_qelem, pkts) do { _qelem->qelem.stats.tail_drops+=pkts; TAILDROP(pkts)} while(0)
#define QELEM_TAIL_DROP_TS(_qelem, now, pkts) do { _qelem->qelem.stats.tail_drops++; TAILDROP_TS(now,pkts)} while(0)

#define QELEM_AQM_DROP(_qelem) do { _qelem->qelem.stats.aqm_drops++; } while(0)

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
