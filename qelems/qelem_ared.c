#include "qelems.h"
#include "../rte_include.h"
#include "qlog.h"
#include "macros/common.h"

#define NO_BYTES_COUNTER
#include "qdt/lolliq.h"

// TODO selector

// #define ADAPTIVE // TODO: is this necessary
#define GENTLE

#include "qtrl/ared_floats.h"
// #include "qtrl/ared_32bit.h"


static qelem_ops_t qelem_ared_ops;

typedef struct __qelem_ared
{
    qelem_t qelem;

    ared_params_t ared_params;
    ared_state_t ared_state;

    lolliqueue_t queue;
} qelem_ared_t;

//-----------------------------------------------------------------------------------------------------------

static
qelem_t* qelem_ared_alloc ( const qelem_params_t* common_params, const void* qelem_params )
{
    LOG_INIT ( "allocating qelem_red for port %d \n", common_params->port_id );

    QPARAMSCAST ( ared );
    qelem_ared_t* ared = 0;

    ared_params_t loc_params;

    // need to calculate max_th before allocating queue
    ctrl_ared_init ( &ared->ared_state, &loc_params,
		     rte_get_tsc_hz(), params,
                     common_params->trace_queue);

    // need to calculate max_th before allocating queue
    ared_state_t tmp_ared_state;
    ared_params_t tmp_ared_params;

    unsigned qlen;

    if ( params->qlen_pkts == 0 )
    {
#ifdef GENTLE
        qlen = 2 * tmp_ared_params.max_thresh;
#else
        qlen = tmp_ared_params.max_thresh;
#endif
        // round to the next power of two:
        qlen = 1 << ( 32 - __builtin_clz ( qlen - 1 ) );
    }
    else
    {
        qlen = params->qlen_pkts;
    }

    LOG_PARAM(qlen, "%u");

    // TODO calculate queue size from parameters:

    size_t queue_mem_size = lolliq_entries_sizeof ( qlen );
    QZMALLOC ( ared, qelem_ared_t, queue_mem_size );
    lolliq_init ( &ared->queue, qlen );

    qelem_init ( ( qelem_t* ) ared, common_params, &qelem_ared_ops );

    memcpy(&ared->ared_state, &tmp_ared_state, sizeof(tmp_ared_state));
    memcpy(&ared->ared_params, &tmp_ared_params, sizeof(tmp_ared_params));

    return ( qelem_t* ) ared;
};


//-----------------------------------------------------------------------------------------------------------
// helpers opts
//-----------------------------------------------------------------------------------------------------------

static inline
unsigned ared_enqueue_burst ( qelem_ared_t* ared, struct rte_mbuf **pkts,  uint32_t num_pkts, uint64_t now )
{
    unsigned pkt_num, num_enq = 0, num_taildrop = 0,num_aqm_drop = 0;
    for ( pkt_num = 0; pkt_num < num_pkts; pkt_num++ )
    {
        struct rte_mbuf* pkt = pkts[pkt_num];

        // if there is no space we can't do anything :)
        if ( unlikely ( lolliq_free_count ( &ared->queue ) == 0 ) )
        {
            rte_pktmbuf_free ( pkt );
            num_taildrop++;
            continue;
        }

        boolean_t should_drop = ctrl_ared_on_pkt_enqueue ( &ared->ared_state, &ared->ared_params, now,
                                lolliq_count ( &ared->queue ) );
        if ( unlikely ( should_drop ) )
        {
            rte_pktmbuf_free ( pkt );
            num_aqm_drop++;
        }
        else
        {
            lolliq_enqueue_pkt ( &ared->queue, pkt );
            num_enq++;
        }
    }

    red_on_pkts_enqueue(ared->ared_state.trace_queue, now, num_aqm_drop, num_taildrop);
    return num_enq;
}

static inline
void ared_send_burst ( qelem_ared_t* ared, tsc_cycles_t now )
{
    struct rte_mbuf** burst;
    unsigned num_pkts = lolliq_peek_burst ( &ared->queue, &burst );

    if ( num_pkts > 0 )
    {
        unsigned num_tx = rte_eth_tx_burst ( ared->qelem.port_id, ared->qelem.queue_id, burst, num_pkts );

        if ( num_tx > 0 )
        {
            lolliq_dequeue_peeked_burst ( &ared->queue, burst, num_tx );
            ctrl_ared_on_pkt_dequeue ( &ared->ared_state, &ared->ared_params, now, lolliq_count ( &ared->queue ) );
        }
    }
}

//-----------------------------------------------------------------------------------------------------------
// default opts
//-----------------------------------------------------------------------------------------------------------

static
void qelem_ared_tx_flush ( qelem_t *qelem )
{
    QCAST ( ared );
    tsc_cycles_t now = rte_get_timer_cycles();

    // flush queue
    if ( lolliq_count ( &ared->queue ) > 0 )
    {
        ared_send_burst ( ared, now );
    }

    ctrl_ared_update_if_needed ( &ared->ared_state, &ared->ared_params, now);
}

static
void qelem_ared_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    QCAST ( ared );
    tsc_cycles_t now = rte_get_timer_cycles();

    // enqueu packets
    ared_enqueue_burst ( ared, pkts, num_pkts, now );

    // if there are enough packet to send -> send them
    if ( likely ( lolliq_count ( &ared->queue ) >= ared->qelem.min_send_burst ) )
    {
        ared_send_burst ( ared, now );
    }
}

static
void qelem_ared_free ( qelem_t* qelem )
{
    qelem_ared_t* ared = ( qelem_ared_t* ) qelem;
    rte_free ( ared );
};

//-----------------------------------------------------------------------------------------------------------
// trace log
//-----------------------------------------------------------------------------------------------------------

#ifdef WITH_TRACES
static
void* red_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
    return trace_event_queue_add ( "red", common_params->trace_queue_size, sizeof ( red_event_t ), red_event_log );
}
#endif

//---------------------------------------------------------------------------------------------------------------

qelem_factory_t qelem_ared_factory =
{
    .qelem_alloc = qelem_ared_alloc,
#ifdef WITH_TRACES
    .trace_queue_alloc = red_trace_queue_alloc,
#endif
};

static qelem_ops_t qelem_ared_ops =
{
    .qelem_tx_burst = qelem_ared_tx_burst,
    .qelem_tx_flush = qelem_ared_tx_flush,
    .qelem_free = qelem_ared_free,
};

