#ifndef	__TESTQS__
#define	__TESTQS__

#include "../qelem.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

/// for tesstqs there is a single list of params  
  
typedef struct
{
  unsigned qlen_pkts;
  unsigned qlen_thresh_pkts;
  double ewma_weight;
  double ecn_p;
} testq_params_t;

/// basic lolliq based qelem without byte coutner
extern qelem_factory_t testq_base_factory;

extern qelem_factory_t testq_noop_factory;

/// qelem with byte counter
extern qelem_factory_t testq_bc_factory;

/// qelem with hol blocking on tx
extern qelem_factory_t testq_block_factory;

/// qelem with linked list
extern qelem_factory_t testq_linkedq_factory;

/// qelem with timestamps saved in pkt metadata
extern qelem_factory_t testq_tspkt_factory;

/// qelem based on linked list with timestamps saved in pkt metadata
/// it is here because if we need to touch metadata anyway, saving timestamps may inquire less overhead
extern qelem_factory_t testq_lqts_factory;

/// qelem with timestamps saved in queueu
extern qelem_factory_t testq_tsq_factory;

/// qelem with pie-like drain rate estimation based on queue byte count
extern qelem_factory_t testq_drainq_factory;

/// qelem with pie-like drain rate estimation based on nic stats
extern qelem_factory_t testq_drainnic_factory;

/// qelem with pie-like drain rate estimation based on nic stats w/o byte counter in the queu
extern qelem_factory_t testq_drainnic2_factory;

/// qelem that ecn-marks packets assuming all traffic is ecn
extern qelem_factory_t testq_ecn_factory;

/// qelem that ecn-marks packets if it can and drops others
extern qelem_factory_t testq_ecn2_factory;

/// qelem that ecn-marks packets
extern qelem_factory_t testq_drop_factory;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
