#include "testqs.h"
#include "../../rte_include.h"
#include "../qlog.h"
#include "macros.h"

typedef struct
{
  uint64_t timestamp;
} pkt_metadata_t;


// add data to linkedq (LQE - li)
#define LQ_ENTRY_DATA pkt_metadata_t
#define LQ_PARAM ,tsc_cycles_t timestamp
#define LQ_SET(m_entry) m_entry->data.timestamp = timestamp
#include "../qdt/linkedq.h"


#include "testq_events.h"
#include "testq_log.h"

#ifdef IN_IDE_PARSER
#define MAX_BURST 32
#endif

static qelem_ops_t testq_lqts_ops;

typedef struct
{
  qelem_t qelem;
  
#ifdef WITH_TRACES  
  event_log_queue_t* trace_queue;
#endif 
  
  linkedq_t queue;
} testq_lqts_t __rte_cache_aligned;


//------------------------------------------------------------------------------------------------------

static
qelem_t* testq_lqts_alloc ( const  qelem_params_t* common_params, const void* qelem_params )
{
  LOG_INIT ( "allocating testq_lqts for port %d \n", common_params->port_id );

  TQPARAMSCAST();
  testq_lqts_t* testq = 0;

  TQZMALLOC ( testq, testq_lqts_t, 0 );
  qelem_init ( ( qelem_t* ) testq, common_params, &testq_lqts_ops );
  linkedq_init ( &testq->queue, params->qlen_pkts );
  
#ifdef WITH_TRACES  
  testq->trace_queue = common_params->trace_queue;
#endif
  
  return ( qelem_t* ) testq;
};

//------------------------------------------------------------------------------------------------------
// helper opts
//------------------------------------------------------------------------------------------------------

static inline unsigned testq_lqts_enqueue_burst ( testq_lqts_t *testq, struct rte_mbuf **pkts,  uint32_t num_pkts, tsc_cycles_t now )
{
  unsigned num_enqueued = linkedq_enqueue_burst ( &testq->queue, pkts, num_pkts, now );

  // drop packets which didn't fit in
  unsigned pkt_num;
  if ( unlikely ( num_enqueued < num_pkts ) )
  {
    for ( pkt_num = num_enqueued ; pkt_num < num_pkts; pkt_num++ )
    {
      rte_pktmbuf_free ( pkts[pkt_num] );
    };
  }

  return num_enqueued;
}


static inline void testq_lqts_send_burst ( testq_lqts_t *testq, tsc_cycles_t now )
{
  struct rte_mbuf** burst = 0;
  unsigned num_pkts = linkedq_peek_burst ( &testq->queue, &burst );

  unsigned num_tx = rte_eth_tx_burst ( testq->qelem.port_id, testq->qelem.queue_id, burst, num_pkts );
  if ( num_tx > 0 )
  {
    linkedq_dequeue_peeked_burst ( &testq->queue, burst, num_tx );
  }
}


//------------------------------------------------------------------------------------------------------
// default opts
//------------------------------------------------------------------------------------------------------

static
void testq_lqts_tx_flush ( qelem_t *qelem )
{
  TQCAST ( lqts );
  tsc_cycles_t now = rte_get_timer_cycles();

  if ( likely ( linkedq_count ( &testq->queue ) > 0 ) )
  {
    testq_lqts_send_burst ( testq, now );
  }

}

static
void testq_lqts_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
  TQCAST ( lqts );
  tsc_cycles_t now = rte_get_timer_cycles();

  // enqueue packets
  testq_lqts_enqueue_burst ( testq, pkts, num_pkts, now );

  // if there is at least burst packets in the queue, transmit packets
  if ( likely ( linkedq_count ( &testq->queue ) >= testq->qelem.min_send_burst ) )
  {
    testq_lqts_send_burst ( testq, now );
  }
}

static
void testq_lqts_free ( qelem_t* qelem )
{
  rte_free ( qelem );
};

//-----------------------------------------------------------------------------------------------------------
// trace log
//-----------------------------------------------------------------------------------------------------------

#ifdef WITH_TRACES
static
void* testq_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
  return trace_event_queue_add ( "testq", common_params->trace_queue_size, sizeof ( testq_event_t ), testq_event_log );
}
#endif

//--------------------------------------------------------------------------------------------------------

qelem_factory_t testq_lqts_factory =
{
  .qelem_alloc = testq_lqts_alloc,
#ifdef WITH_TRACES
  .trace_queue_alloc = testq_trace_queue_alloc,
#endif
};

static qelem_ops_t testq_lqts_ops =
{
  .qelem_tx_burst = testq_lqts_tx_burst,
  .qelem_tx_flush = testq_lqts_tx_flush,
  .qelem_free = testq_lqts_free,
};

