#include "testqs.h"
#include "../../rte_include.h"
#include "../qlog.h"
#include "macros.h"

#define NO_BYTES_COUNTER
#include "../qdt/lolliq.h"

#include "testq_events.h"
#include "testq_log.h"

static qelem_ops_t testq_drainnic2_ops;

typedef struct
{
  qelem_t qelem;

  /*const*/ unsigned qlen_thresh;
  /*const*/ double ewma_weight;

  boolean_t in_measurements;
  tsc_cycles_t measurement_cycle_begin;
  uint64_t tx_bytes_on_begin;
  unsigned sent_pkts_in_cycle;
  double avg_drain_rate;

#ifdef WITH_TRACES
  event_log_queue_t* trace_queue;
#endif
  lolliqueue_t queue;

} testq_drainnic2_t __rte_cache_aligned;

typedef struct
{
  uint64_t timestamp;
} pkt_metadata_t;

//------------------------------------------------------------------------------------------------------

static
qelem_t* testq_drainnic2_alloc ( const  qelem_params_t* common_params, const void* qelem_params )
{
  LOG_INIT ( "allocating testq_drainnic2 for port %d \n", common_params->port_id );

  TQPARAMSCAST ( );
  testq_drainnic2_t* testq = 0;

  size_t queue_mem_size = lolliq_entries_sizeof ( params->qlen_pkts );
  TQZMALLOC ( testq, testq_drainnic2_t, queue_mem_size );
  qelem_init ( ( qelem_t* ) testq, common_params, &testq_drainnic2_ops );
  lolliq_init ( &testq->queue, params->qlen_pkts );
  
  testq->qlen_thresh = params->qlen_thresh_pkts;
  testq->ewma_weight = params->ewma_weight;
  
  LOG_PARAM ( params->qlen_pkts ,"%u" );
  LOG_PARAM ( testq->qlen_thresh ,"%u" );
  LOG_PARAM ( testq->ewma_weight ,"%f" );  
  
#ifdef WITH_TRACES
  testq->trace_queue = common_params->trace_queue;
#endif

  return ( qelem_t* ) testq;
};

//------------------------------------------------------------------------------------------------------
// helper opts
//------------------------------------------------------------------------------------------------------

static inline unsigned testq_drainnic2_enqueue_burst ( testq_drainnic2_t *testq, struct rte_mbuf **pkts,  uint32_t num_pkts, tsc_cycles_t now )
{
  unsigned num_enqueued = lolliq_enqueue_burst ( &testq->queue, pkts, num_pkts );

  // drop packets which didn't fit in
  unsigned pkt_num;
  if ( unlikely ( num_enqueued < num_pkts ) )
  {
    for ( pkt_num = num_enqueued ; pkt_num < num_pkts; pkt_num++ )
    {
      rte_pktmbuf_free ( pkts[pkt_num] );
    };
  }

  return num_enqueued;
}

static inline
uint64_t testq_get_obytes ( testq_drainnic2_t *testq )
{
  // this is base version - get statistics per port
  // in real impl we need to set statistics per queue
  struct rte_eth_stats stats;
  int res = rte_eth_stats_get ( testq->qelem.port_id, &stats );

  if ( res != 0  )
  {
    return 0;
  }
  else
  {
    return stats.obytes;
  }

}

static inline
void testq_calculate_drain_rate ( testq_drainnic2_t *testq, tsc_cycles_t now, unsigned new_queue_len, unsigned num_sent_pkts )
{
  // start measurement cycle when there are at least 10 pkts still in the queue
  // this is done because if there are packets in the queue, tx ring is probably also full
  if ( !testq->in_measurements && new_queue_len > testq->qlen_thresh )
  {
    testq->tx_bytes_on_begin = testq_get_obytes ( testq );
    if ( testq->tx_bytes_on_begin == 0 )
    {
      // do not start measurement cycle
    }
    else
    {
      testq->in_measurements = TRUE;
      testq->sent_pkts_in_cycle = 0;
      testq->measurement_cycle_begin = now;

      draint_measurement_on ( testq->trace_queue, now );
    }
  }

  if ( testq->in_measurements )
  {
    testq->sent_pkts_in_cycle += num_sent_pkts;

    if ( testq->sent_pkts_in_cycle >= testq->qlen_thresh )
    {
      uint64_t tx_bytes_curr= testq_get_obytes ( testq );

      if ( !tx_bytes_curr )
      {
        // skip this measurement
      }
      else
      {
        tsc_cycles_t drain_interval = now - testq->measurement_cycle_begin;
        uint64_t drained_bytes = tx_bytes_curr - testq->tx_bytes_on_begin;
        double curr_drain_rate = ( ( double ) drained_bytes ) / ( double ) drain_interval;

        testq->avg_drain_rate = ( 1 - testq->ewma_weight ) * testq->avg_drain_rate + testq->ewma_weight * curr_drain_rate;

        draint_new_measurement ( testq->trace_queue, now, curr_drain_rate, testq->avg_drain_rate );
      }

      // cycle end! start new cycle if there are enough packets and we have link stat
      if ( new_queue_len > testq->qlen_thresh && tx_bytes_curr > 0 )
      {
        // start new cycle
        testq->tx_bytes_on_begin = tx_bytes_curr;
        testq->sent_pkts_in_cycle = 0;
        testq->measurement_cycle_begin = now;
      }
      else
      {
        testq->in_measurements = FALSE;
        draint_measurement_off ( testq->trace_queue, now );
      }
    }
  }

}

static inline
void testq_drainnic2_send_burst ( testq_drainnic2_t *testq, tsc_cycles_t now )
{
  struct rte_mbuf** burst = 0;
  unsigned num_pkts = lolliq_peek_burst ( &testq->queue, &burst );

  // TODO somehow read timestamps

  unsigned num_tx = rte_eth_tx_burst ( testq->qelem.port_id, testq->qelem.queue_id, burst, num_pkts );
  if ( num_tx > 0 )
  {
//     unsigned old_queue_len = lolliq_count ( &testq->queue );
    lolliq_dequeue_peeked_burst ( &testq->queue, burst, num_tx );
    unsigned new_queue_len = lolliq_count ( &testq->queue );

//     LOG_VAR(new_queue_len, "%u");
    testq_calculate_drain_rate ( testq, now, new_queue_len, num_tx );

    // TODO if we find out what to do with sojourn, do it with drain rate...
  }
}

//------------------------------------------------------------------------------------------------------
// default opts
//------------------------------------------------------------------------------------------------------

static
void testq_drainnic2_tx_flush ( qelem_t *qelem )
{
  TQCAST ( drainnic2 );
  tsc_cycles_t now = rte_get_timer_cycles();

  if ( likely ( lolliq_count ( &testq->queue ) > 0 ) )
  {
    testq_drainnic2_send_burst ( testq, now );
  }
}

static
void testq_drainnic2_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
  TQCAST ( drainnic2 );
  tsc_cycles_t now = rte_get_timer_cycles();

  // enqueue packets
  testq_drainnic2_enqueue_burst ( testq, pkts, num_pkts, now );

  // if there is at least burst packets in the queue, transmit packets
  if ( likely ( lolliq_count ( &testq->queue ) >= testq->qelem.min_send_burst ) )
  {
    testq_drainnic2_send_burst ( testq, now );
  }
}

static
void testq_drainnic2_free ( qelem_t* qelem )
{
  rte_free ( qelem );
};

//-----------------------------------------------------------------------------------------------------------
// trace log
//-----------------------------------------------------------------------------------------------------------

#ifdef WITH_TRACES
static
void* testq_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
  return trace_event_queue_add ( "testq", common_params->trace_queue_size, sizeof ( testq_event_t ), drainq_event_log );
}
#endif

//--------------------------------------------------------------------------------------------------------

qelem_factory_t testq_drainnic2_factory =
{
  .qelem_alloc = testq_drainnic2_alloc,
#ifdef WITH_TRACES
  .trace_queue_alloc = testq_trace_queue_alloc,
#endif
};

static qelem_ops_t testq_drainnic2_ops =
{
  .qelem_tx_burst = testq_drainnic2_tx_burst,
  .qelem_tx_flush = testq_drainnic2_tx_flush,
  .qelem_free = testq_drainnic2_free,
};

