#include "testqs.h"
#include "../../rte_include.h"
#include "../qlog.h"
#include "macros.h"

#include "../qdt/lolliq.h"

#include "testq_events.h"
#include "testq_log.h"
// #include "makeconf.h"

static qelem_ops_t testq_bc_ops;

typedef struct
{
  qelem_t qelem;
  
#ifdef WITH_TRACES  
  event_log_queue_t* trace_queue;
#endif    
  lolliqueue_t queue;
} testq_bc_t __rte_cache_aligned;


//------------------------------------------------------------------------------------------------------

static
qelem_t* testq_bc_alloc ( const  qelem_params_t* common_params, const void* qelem_params )
{
  LOG_INIT ( "allocating testq_bc for port %d \n", common_params->port_id );

  TQPARAMSCAST ( );
  testq_bc_t* testq = 0;

  // determine queue length
  unsigned qlen = params->qlen_pkts;
  LOG_PARAM ( qlen ,"%u" );

  size_t queue_mem_size = lolliq_entries_sizeof (qlen);
  TQZMALLOC ( testq, testq_bc_t, queue_mem_size );
  qelem_init ( ( qelem_t* ) testq, common_params, &testq_bc_ops );
  lolliq_init ( &testq->queue, qlen);

#ifdef WITH_TRACES  
  testq->trace_queue = common_params->trace_queue;
#endif
  
  return ( qelem_t* ) testq;
};

//------------------------------------------------------------------------------------------------------
// helper opts
//------------------------------------------------------------------------------------------------------

static inline unsigned testq_bc_enqueue_burst ( testq_bc_t *testq, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
  unsigned num_enqueued = lolliq_enqueue_burst ( &testq->queue, pkts, num_pkts );

  // drop packets which didn't fit in
  unsigned pkt_num;
  if ( unlikely ( num_enqueued < num_pkts ) )
  {
    for ( pkt_num = num_enqueued ; pkt_num < num_pkts; pkt_num++ )
    {
      rte_pktmbuf_free ( pkts[pkt_num] );
    };
  }

  return num_enqueued;
}

//------------------------------------------------------------------------------------------------------
// default opts
//------------------------------------------------------------------------------------------------------

static
void testq_bc_tx_flush ( qelem_t *qelem )
{
  TQCAST ( bc );

  struct rte_mbuf** burst;
  unsigned num_pkts = lolliq_peek_burst ( &testq->queue, &burst );

  if ( likely ( num_pkts > 0 ) )
  {
    unsigned num_tx = rte_eth_tx_burst ( qelem->port_id, qelem->queue_id, burst, num_pkts );
    if ( num_tx > 0 )
    {
      lolliq_dequeue_peeked_burst ( &testq->queue, burst, num_tx );
      //TODO events to show num of pkts enqueued to the ring of queue
    }
  }
}

static
void testq_bc_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
  TQCAST ( bc );

  // enqueue packets
  testq_bc_enqueue_burst ( testq, pkts, num_pkts );

  // if there is at least burst packets in the queue, transmit packets
  if ( likely ( lolliq_count ( &testq->queue ) >= testq->qelem.min_send_burst ) )
  {
    testq_bc_tx_flush ( qelem );
  }
}

static
void testq_bc_free ( qelem_t* qelem )
{
  rte_free ( qelem );
};

//-----------------------------------------------------------------------------------------------------------
// trace log
//-----------------------------------------------------------------------------------------------------------

#ifdef WITH_TRACES
static
void* testq_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
  return trace_event_queue_add ( "testq", common_params->trace_queue_size, sizeof ( testq_event_t ), testq_event_log);
}
#endif

//--------------------------------------------------------------------------------------------------------

qelem_factory_t testq_bc_factory =
{
  .qelem_alloc = testq_bc_alloc,
#ifdef WITH_TRACES
  .trace_queue_alloc = testq_trace_queue_alloc,
#endif
};

static qelem_ops_t testq_bc_ops =
{
  .qelem_tx_burst = testq_bc_tx_burst,
  .qelem_tx_flush = testq_bc_tx_flush,
  .qelem_free = testq_bc_free,
};

