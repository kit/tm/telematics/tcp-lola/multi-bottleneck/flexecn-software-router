#include "testqs.h"
#include "../../rte_include.h"
#include "../qlog.h"
#include "macros.h"

#define NO_BYTES_COUNTER
#include "../qdt/lolliq.h"

#include "testq_events.h"
#include "testq_log.h"
// #include "makeconf.h"

static qelem_ops_t testq_block_ops;

typedef struct
{
    qelem_t qelem;

#ifdef WITH_TRACES
    event_log_queue_t* trace_queue;
#endif

    double max_tokens;
    double tokens;
    double rate_tsc;
    tsc_cycles_t last_update;

    lolliqueue_t queue;
} testq_block_t __rte_cache_aligned;


//------------------------------------------------------------------------------------------------------

static
qelem_t* testq_block_alloc ( const  qelem_params_t* common_params, const void* qelem_params )
{
    LOG_INIT ( "allocating testq_block for port %d \n", common_params->port_id );

    TQPARAMSCAST ( );
    testq_block_t* testq = 0;

    // determine queue length
    unsigned qlen = params->qlen_pkts;
    LOG_PARAM ( qlen ,"%u" );

    size_t queue_mem_size = lolliq_entries_sizeof ( qlen );
    TQZMALLOC ( testq, testq_block_t, queue_mem_size );
    qelem_init ( ( qelem_t* ) testq, common_params, &testq_block_ops );
    lolliq_init ( &testq->queue, qlen );

#ifdef WITH_TRACES
    testq->trace_queue = common_params->trace_queue;
#endif

    testq->rate_tsc = 14.88e6 / rte_get_tsc_hz();
    testq->max_tokens = 32;
    testq->tokens = testq->max_tokens;
//     testq->min_send_burst = common_params->min_send_burst;

    return ( qelem_t* ) testq;
};

//------------------------------------------------------------------------------------------------------
// helper opts
//------------------------------------------------------------------------------------------------------

static inline unsigned testq_block_enqueue_burst ( testq_block_t *testq, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    unsigned num_enqueued = lolliq_enqueue_burst ( &testq->queue, pkts, num_pkts );

    // drop packets which didn't fit in
    unsigned pkt_num;
    if ( unlikely ( num_enqueued < num_pkts ) )
    {
        for ( pkt_num = num_enqueued ; pkt_num < num_pkts; pkt_num++ )
        {
            rte_pktmbuf_free ( pkts[pkt_num] );
        };
    }

    return num_enqueued;
}

//------------------------------------------------------------------------------------------------------
// default opts
//------------------------------------------------------------------------------------------------------

// static
// void testq_block_tx_flush ( qelem_t *qelem )
// {
//   TQCAST ( block );
//
//   struct rte_mbuf** burst;
//   unsigned num_pkts = lolliq_peek_burst ( &testq->queue, &burst );
//
//   if ( likely ( num_pkts > 0 ) )
//   {
//     struct rte_mbuf** burst_to_send = burst;
//     unsigned num_pkts_to_send = num_pkts;
//
// //     LOG_VAR(num_pkts, "%u");
// //     LOG_VAR(burst[0], "%p");
// //     LOG_VAR(burst1[0], "%p");
// //     LOG_VAR(burst[1], "%p");
// //     LOG_VAR(burst1[1], "%p");
//
// //     LOG_LINE();
// //     LOG_VAR(num_pkts, "%u");
//
//     do
//     {
//       unsigned num_tx = rte_eth_tx_burst ( qelem->port_id, qelem->queue_id, burst_to_send, num_pkts_to_send );
//       num_pkts_to_send -= num_tx;
//       burst_to_send += num_tx;
//
// //     LOG_VAR(burst[num_pkts-num_pkts1], "%p");
// //     LOG_VAR(burst1[0], "%p");
// //       LOG_VAR(num_tx, "%u");
// //       LOG_VAR(num_pkts, "%u");
//     }
//     while ( num_pkts_to_send > 0 );
//
// //     LOG_LINE();
//
//     lolliq_dequeue_peeked_burst ( &testq->queue, burst, num_pkts );
//   }
// }

static
void testq_block_tx_flush ( qelem_t *qelem )
{
    TQCAST ( block );

    // peek packets to send
    struct rte_mbuf** burst;
    unsigned max_num_pkts = lolliq_peek_burst ( &testq->queue, &burst );

    if ( max_num_pkts == 0 ) {
        return;
    }

    // update token number
    if ( testq->tokens < testq->max_tokens ) {
        // update tokens
        tsc_cycles_t now = rte_get_tsc_cycles();
        tsc_cycles_t delta = now - testq->last_update;

        testq->tokens += delta * testq->rate_tsc;
// 	LOG_VAR(testq->tokens, "%f");
        if ( testq->tokens > testq->max_tokens ) {
            testq->tokens = testq->max_tokens;
        }

        testq->last_update = now;
    }

    // send tok packets
    if ( testq->tokens > 0 ) {
      
	unsigned num_pkts = max_num_pkts > testq->tokens ? testq->tokens : max_num_pkts;

        struct rte_mbuf** burst_to_send = burst;
        unsigned num_pkts_to_send = num_pkts;

        do
        {
            unsigned num_tx = rte_eth_tx_burst ( qelem->port_id, qelem->queue_id, burst_to_send, num_pkts_to_send );
            num_pkts_to_send -= num_tx;
            burst_to_send += num_tx;

        }
        while ( num_pkts_to_send > 0 );

        lolliq_dequeue_peeked_burst ( &testq->queue, burst, num_pkts );
	testq->tokens -= num_pkts;
    }
}

static
void testq_block_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    TQCAST ( block );

    // enqueue packets
    testq_block_enqueue_burst ( testq, pkts, num_pkts );

    // if there is at least burst packets in the queue, transmit packets
    if ( likely ( lolliq_count ( &testq->queue ) >= testq->qelem.min_send_burst ) )
    {
        testq_block_tx_flush ( qelem );
    }
}

static
void testq_block_free ( qelem_t* qelem )
{
    rte_free ( qelem );
};

//-----------------------------------------------------------------------------------------------------------
// trace log
//-----------------------------------------------------------------------------------------------------------

#ifdef WITH_TRACES
static
void* testq_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
    return trace_event_queue_add ( "testq", common_params->trace_queue_size, sizeof ( testq_event_t ), testq_event_log);
}
#endif

//--------------------------------------------------------------------------------------------------------

qelem_factory_t testq_block_factory =
{
    .qelem_alloc = testq_block_alloc,
#ifdef WITH_TRACES
    .trace_queue_alloc = testq_trace_queue_alloc,
#endif
};

static qelem_ops_t testq_block_ops =
{
    .qelem_tx_burst = testq_block_tx_burst,
    .qelem_tx_flush = testq_block_tx_flush,
    .qelem_free = testq_block_free,
};

