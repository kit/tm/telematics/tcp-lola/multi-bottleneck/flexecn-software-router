#include "testqs.h"
#include "../../rte_include.h"
#include "../qlog.h"
#include "macros.h"

// #define NO_BYTES_COUNTER
// #include "../qdt/lolliq.h"

#include "testq_events.h"
#include "testq_log.h"
// #include "makeconf.h"

static qelem_ops_t testq_noop_ops;

typedef struct
{
  qelem_t qelem;
 
#ifdef WITH_TRACES  
  event_log_queue_t* trace_queue;
#endif  
  
//   lolliqueue_t queue;
} testq_noop_t __rte_cache_aligned;


//------------------------------------------------------------------------------------------------------

static
qelem_t* testq_noop_alloc ( const  qelem_params_t* common_params, const void* qelem_params )
{
  LOG_INIT ( "allocating testq_noop for port %d \n", common_params->port_id );

  TQPARAMSCAST ( );
  testq_noop_t* testq = 0;

  // determine queue length
  unsigned qlen = params->qlen_pkts;
  LOG_PARAM ( qlen ,"%u" );

//   size_t queue_mem_size = lolliq_entries_sizeof (qlen);
  TQZMALLOC ( testq, testq_noop_t, 0 );
  qelem_init ( ( qelem_t* ) testq, common_params, &testq_noop_ops );
//   lolliq_init ( &testq->queue, qlen);

#ifdef WITH_TRACES
  testq->trace_queue = common_params->trace_queue;
#endif
  
  return ( qelem_t* ) testq;
};

//------------------------------------------------------------------------------------------------------
// helper opts
//------------------------------------------------------------------------------------------------------

static inline
unsigned send_or_drop_burst ( uint8_t port_id, uint16_t queue_id, struct rte_mbuf** burst, uint16_t num_pkts )
{
    unsigned num_tx = rte_eth_tx_burst ( port_id, queue_id, burst, num_pkts );

    if ( unlikely ( num_tx < num_pkts ) ) {
        unsigned pkt_to_drop;
        for ( pkt_to_drop = num_tx ; pkt_to_drop < num_pkts; pkt_to_drop++ ) {
            rte_pktmbuf_free ( burst[pkt_to_drop] );
        };
    }

    return num_tx;
}
//------------------------------------------------------------------------------------------------------
// default opts
//------------------------------------------------------------------------------------------------------

static
void testq_noop_tx_flush ( qelem_t *qelem )
{
  TQCAST ( noop );

//   struct rte_mbuf** burst;
//   unsigned num_pkts = lolliq_peek_burst ( &testq->queue, &burst );
// 
//   if ( likely ( num_pkts > 0 ) )
//   {
//     unsigned num_tx = rte_eth_tx_burst ( qelem->port_id, qelem->queue_id, burst, num_pkts );
//     if ( num_tx > 0 )
//     {
//       lolliq_dequeue_peeked_burst ( &testq->queue, burst, num_tx );
//       //TODO events to show num of pkts enqueued to the ring of queue
//     }
//   }
}

static
void testq_noop_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
  TQCAST ( noop );
  send_or_drop_burst ( qelem->port_id, qelem->queue_id, pkts, num_pkts );
}

static
void testq_noop_free ( qelem_t* qelem )
{
  rte_free ( qelem );
};

//-----------------------------------------------------------------------------------------------------------
// trace log
//-----------------------------------------------------------------------------------------------------------

#ifdef WITH_TRACES
static
void* testq_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
  return trace_event_queue_add ( "testq", common_params->trace_queue_size, sizeof ( testq_event_t ), testq_event_log );
}
#endif

//--------------------------------------------------------------------------------------------------------

qelem_factory_t testq_noop_factory =
{
  .qelem_alloc = testq_noop_alloc,
#ifdef WITH_TRACES
  .trace_queue_alloc = testq_trace_queue_alloc,
#endif
};

static qelem_ops_t testq_noop_ops =
{
  .qelem_tx_burst = testq_noop_tx_burst,
  .qelem_tx_flush = testq_noop_tx_flush,
  .qelem_free = testq_noop_free,
};

