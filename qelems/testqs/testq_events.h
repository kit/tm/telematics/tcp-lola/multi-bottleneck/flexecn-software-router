#ifndef	__TESTQ_EVENT__
#define	__TESTQ_EVENT__

#ifdef IN_IDE_PARSER
#define WITH_TRACES 2
#endif

#ifdef WITH_TRACES
#include "../traces/trace_event_queue.h"
#include <stdint.h>

/**
 * possible events:
 *
 * base - num packets enqueued on the ring
 *
 * sojourn: 
 * 	pkt dequeue: <tsc,sj,num_sent>
 * 
 * drain*:
 * 	in/out_measurement <tsc,bool>
 *	new measurement <tsc,curr_drain_rate,avg_drain_rate>
 *
 */

enum TESTQ_EVENT
{
  SJ_SOJOURN,
  DRAINT_IN_OUT_MEASUREMENT,
  DRAINT_NEW_MEASUREMENT,
};

// enum TAILDROP_EVENT {TAILDROP_PKT_DROP, TAILDROP_PKT_DEQUEUE};
//
typedef struct testq_event
{
  uint64_t now;
  uint32_t event_type;
  uint32_t unsigned1;
  uint64_t lunsigned1;
  double double1;
  double double2;
//   uint32_t unsigned2;
//   uint32_t unsigned3;
} testq_event_t;

static inline void testq_sj_sojourn(event_log_queue_t* queue, uint64_t now, uint64_t sojourn, uint32_t num_sent){
  if ( ( queue->writer_index < queue->size ) )
  {
    testq_event_t* event = & ( ( ( testq_event_t* ) queue->events ) [queue->writer_index] );
    event->event_type = SJ_SOJOURN;
    event->now = now;
    event->unsigned1 = num_sent;
    event->lunsigned1 = sojourn;
    COMPILER_BARRIER();
    queue->writer_index++;
  }  
}

static inline void draint_measurement_on ( event_log_queue_t* queue, uint64_t now )
{
  if ( ( queue->writer_index < queue->size ) )
  {
    testq_event_t* event = & ( ( ( testq_event_t* ) queue->events ) [queue->writer_index] );
    event->event_type = DRAINT_IN_OUT_MEASUREMENT;
    event->now = now;
    event->unsigned1 = 1;
    COMPILER_BARRIER();
    queue->writer_index++;
  }
}

static inline void draint_measurement_off ( event_log_queue_t* queue, uint64_t now )
{
  if ( ( queue->writer_index < queue->size ) )
  {
    testq_event_t* event = & ( ( ( testq_event_t* ) queue->events ) [queue->writer_index] );
    event->event_type = DRAINT_IN_OUT_MEASUREMENT;
    event->now = now;
    event->unsigned1 = 0;
    COMPILER_BARRIER();
    queue->writer_index++;
  }
}

static inline void draint_new_measurement ( event_log_queue_t* queue, uint64_t now, double curr_drain_rate, double avg_drain_rate )
{
  if ( ( queue->writer_index < queue->size ) )
  {
    testq_event_t* event = & ( ( ( testq_event_t* ) queue->events ) [queue->writer_index] );
    event->event_type = DRAINT_NEW_MEASUREMENT;
    event->now = now;
    event->double1 = curr_drain_rate;
    event->double2 = avg_drain_rate;
    COMPILER_BARRIER();
    queue->writer_index++;
  }
}

// static inline void taildrop_on_pkts_drop ( event_log_queue_t* queue, uint64_t now, uint32_t num_tail_drops )
// {
//     if ( ( queue->writer_index < queue->size ) ) {
//         taildrop_event_t* event = & ( ( ( taildrop_event_t* ) queue->events ) [queue->writer_index] );
//         event->event_type = TAILDROP_PKT_DROP;
//         event->now = now;
//         event->taildrops = num_tail_drops;
//         COMPILER_BARRIER();
//         queue->writer_index++;
//     }
// }
//
// static inline void taildrop_on_pkts_dequeue ( event_log_queue_t* queue, uint64_t now, uint32_t qlen_pkts, uint32_t qlen_bytes, uint32_t num_deq )
// {
//     if (num_deq > 0 && ( queue->writer_index < queue->size ) ) {
//         taildrop_event_t* event = & ( ( ( taildrop_event_t* ) queue->events ) [queue->writer_index] );
//         event->event_type = TAILDROP_PKT_DEQUEUE;
//         event->now = now;
//         event->qlen_pkts = qlen_pkts;
//         event->qlen_bytes = qlen_bytes;
//         event->taildrops = 0;
//         COMPILER_BARRIER();
//         queue->writer_index++;
//     }
// }

#else

#define testq_sj_sojourn(queue,now,sojourn,num_sent)
#define draint_measurement_on(queue,now )
#define draint_measurement_off(queue,now) 
#define draint_new_measurement(queue,now,curr_drain_rate, avg_drain_rate )


#endif // ifdef WITH_TRACES


#endif // ifndef __QTRL_TAILDROP_LOG__



