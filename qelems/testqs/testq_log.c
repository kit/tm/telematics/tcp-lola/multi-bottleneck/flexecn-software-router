#include "testq_events.h"
#include "../traces/trace_event_queue.h"
#include "testq_log.h"
#include "../../rte_include.h"
#include "../qlog.h"

#include <stdio.h>
#include <stdlib.h>

#ifdef WITH_TRACES


void testq_event_log(trace_event_queue_t* queue)
{
  FILE* fd = queue->fd;
  testq_event_t* event = & ( ( ( testq_event_t* ) queue->queue->events ) [queue->reader_index] );
  switch ( event->event_type )
  {
  case SJ_SOJOURN:
    fprintf ( fd, "sj,%lu,%lu,%u\n", event->now,event->lunsigned1,event->unsigned1);
    break;
  }
}

/**
 * drainq-log:
 * 	iom: tsc,bool=on/off
 * 	mn: tsc,curr_drain_rate,avg_drain_rate
 * 
 */
void drainq_event_log( trace_event_queue_t* queue)
{
//   LOG_LINE();
  FILE* fd = queue->fd;
  testq_event_t* event = & ( ( ( testq_event_t* ) queue->queue->events ) [queue->reader_index] );
  switch ( event->event_type )
  {
  case DRAINT_IN_OUT_MEASUREMENT:
    fprintf ( fd, "iom,%lu,%u\n", event->now,event->unsigned1);
    break;
  case DRAINT_NEW_MEASUREMENT:
    fprintf ( fd, "nm,%lu,%f,%f\n", event->now, event->double1,  event->double2);
    break;
  }
}



#endif
