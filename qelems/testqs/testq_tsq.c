#include "testqs.h"
#include "../../rte_include.h"
#include "../qlog.h"
#include "macros.h"

#define NO_BYTES_COUNTER
#include "../qdt/tsq.h"

#include "testq_events.h"
#include "testq_log.h"

static qelem_ops_t testq_tsq_ops;

typedef struct
{
  qelem_t qelem;
#ifdef WITH_TRACES
  event_log_queue_t* trace_queue;
#endif
  uint64_t sojurn_smth;
  tsqueue_t queue;
  
} testq_tsq_t __rte_cache_aligned;

typedef struct
{
  uint64_t timestamp;
} pkt_metadata_t;

//------------------------------------------------------------------------------------------------------

static
qelem_t* testq_tsq_alloc ( const  qelem_params_t* common_params, const void* qelem_params )
{
  LOG_INIT ( "allocating testq_tsq for port %d \n", common_params->port_id );

  TQPARAMSCAST ( );
  testq_tsq_t* testq = 0;

  // determine queue length
  unsigned qlen = params->qlen_pkts;
  LOG_PARAM ( qlen ,"%u" );

  size_t queue_mem_size = tsq_entries_sizeof ( qlen );
  TQZMALLOC ( testq, testq_tsq_t, queue_mem_size );
  qelem_init ( ( qelem_t* ) testq, common_params, &testq_tsq_ops );
  tsq_init ( &testq->queue, qlen );

#ifdef WITH_TRACES
  testq->trace_queue = common_params->trace_queue;
#endif

  return ( qelem_t* ) testq;
};

//------------------------------------------------------------------------------------------------------
// helper opts
//------------------------------------------------------------------------------------------------------

static inline unsigned testq_tsq_enqueue_burst ( testq_tsq_t *testq, struct rte_mbuf **pkts,  uint32_t num_pkts, tsc_cycles_t now )
{
  unsigned num_enqueued = tsq_enqueue_burst ( &testq->queue, pkts, num_pkts, now);

  // drop packets which didn't fit in
  unsigned pkt_num;
  if ( unlikely ( num_enqueued < num_pkts ) )
  {
    for ( pkt_num = num_enqueued ; pkt_num < num_pkts; pkt_num++ )
    {
      rte_pktmbuf_free ( pkts[pkt_num] );
    };
  }

  return num_enqueued;
}

static inline
void testq_tsq_send_burst ( testq_tsq_t *testq, tsc_cycles_t now )
{
  struct rte_mbuf** burst = 0;
  unsigned num_pkts = tsq_peek_burst ( &testq->queue, &burst );

  // TODO somehow read timestamps

  unsigned num_tx = rte_eth_tx_burst ( testq->qelem.port_id, testq->qelem.queue_id, burst, num_pkts );
  if ( num_tx > 0 )
  {
//     now = rte_rdtsc();
    tsc_cycles_t sojourn = now - tsq_get_timestamp(&testq->queue, 0);
    testq->sojurn_smth += sojourn;
    tsq_dequeue_peeked_burst ( &testq->queue, burst, num_tx );
    testq_sj_sojourn(testq->trace_queue, now, sojourn, num_tx);
  }
}

//------------------------------------------------------------------------------------------------------
// default opts
//------------------------------------------------------------------------------------------------------

static
void testq_tsq_tx_flush ( qelem_t *qelem )
{
  TQCAST ( tsq );
  tsc_cycles_t now = rte_get_timer_cycles();

  if ( likely ( tsq_count ( &testq->queue ) > 0 ) )
  {
    testq_tsq_send_burst ( testq, now );
  }
}

static
void testq_tsq_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
  TQCAST ( tsq );
  tsc_cycles_t now = rte_get_timer_cycles();

  // enqueue packets
  testq_tsq_enqueue_burst ( testq, pkts, num_pkts, now );

  // if there is at least burst packets in the queue, transmit packets
  if ( likely ( tsq_count ( &testq->queue ) >= testq->qelem.min_send_burst ) )
  {
    testq_tsq_send_burst ( testq, now );
  }
}

static
void testq_tsq_free ( qelem_t* qelem )
{
  rte_free ( qelem );
};

//-----------------------------------------------------------------------------------------------------------
// trace log
//-----------------------------------------------------------------------------------------------------------

#ifdef WITH_TRACES
static
void* testq_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
  return trace_event_queue_add ( "testq", common_params->trace_queue_size, sizeof ( testq_event_t ), testq_event_log );
}
#endif

//--------------------------------------------------------------------------------------------------------

qelem_factory_t testq_tsq_factory =
{
  .qelem_alloc = testq_tsq_alloc,
#ifdef WITH_TRACES
  .trace_queue_alloc = testq_trace_queue_alloc,
#endif
};

static qelem_ops_t testq_tsq_ops =
{
  .qelem_tx_burst = testq_tsq_tx_burst,
  .qelem_tx_flush = testq_tsq_tx_flush,
  .qelem_free = testq_tsq_free,
};

