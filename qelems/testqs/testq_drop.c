#include "testqs.h"
#include "../../rte_include.h"
#include "../qlog.h"
#include "../qmath/qm_types.h"
#include "../qmath/fastrand.h"
#include "macros.h"

#define NO_BYTES_COUNTER
#include "../qdt/lolliq.h"

#include "testq_events.h"
#include "testq_log.h"
// #include "makeconf.h"

static qelem_ops_t testq_drop_ops;

typedef struct
{
  qelem_t qelem;

  /* const */ Q_0_32 prob;
  rand_state_t rand_state;

#ifdef WITH_TRACES
  event_log_queue_t* trace_queue;
#endif

  lolliqueue_t queue;
} testq_drop_t __rte_cache_aligned;


//------------------------------------------------------------------------------------------------------

static
qelem_t* testq_drop_alloc ( const  qelem_params_t* common_params, const void* qelem_params )
{
  LOG_INIT ( "allocating testq_drop for port %d \n", common_params->port_id );

  TQPARAMSCAST ( );
  testq_drop_t* testq = 0;

  // determine queue length
  unsigned qlen = params->qlen_pkts;


  LOG_PARAM ( qlen ,"%u" );

  size_t queue_mem_size = lolliq_entries_sizeof ( qlen );
  TQZMALLOC ( testq, testq_drop_t, queue_mem_size );
  qelem_init ( ( qelem_t* ) testq, common_params, &testq_drop_ops );
  lolliq_init ( &testq->queue, qlen );
  fast_srand ( &testq->rand_state, rte_get_timer_cycles() );
  testq->prob = q_0_32_from_double ( params->ecn_p );

  LOG_PARAM ( params->qlen_pkts ,"%u" );
  LOG_PARAM ( params->ecn_p ,"%f" );
  LOG_PARAM ( testq->prob ,"%u" );  
  
#ifdef WITH_TRACES  
  testq->trace_queue = common_params->trace_queue;
#endif 

  return ( qelem_t* ) testq;
};

//------------------------------------------------------------------------------------------------------
// helper opts
//------------------------------------------------------------------------------------------------------

static inline unsigned testq_drop_enqueue_burst ( testq_drop_t *testq, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
  unsigned pkt_num;
  unsigned num_enqueued;
  // for each enqueued packet mark it with probability p
  for ( pkt_num = 0; pkt_num < num_pkts; pkt_num++ )
  {
    Q_0_32 rand = fast_rand ( &testq->rand_state );
    if ( rand < testq->prob )
    {
      // "aqm" drop
      rte_pktmbuf_free ( pkts[pkt_num] );
    }
    else if ( !lolliq_enqueue_pkt ( &testq->queue, pkts[pkt_num] ) )
    {
      // taildrop
      rte_pktmbuf_free ( pkts[pkt_num] );
    }
    else
    {
      num_enqueued++;
    }
  }

  return num_enqueued;
}

//------------------------------------------------------------------------------------------------------
// default opts
//------------------------------------------------------------------------------------------------------

static
void testq_drop_tx_flush ( qelem_t *qelem )
{
  TQCAST ( drop );

  struct rte_mbuf** burst;
  unsigned num_pkts = lolliq_peek_burst ( &testq->queue, &burst );

  if ( likely ( num_pkts > 0 ) )
  {
    unsigned num_tx = rte_eth_tx_burst ( qelem->port_id, qelem->queue_id, burst, num_pkts );
    if ( num_tx > 0 )
    {
      lolliq_dequeue_peeked_burst ( &testq->queue, burst, num_tx );
      //TODO events to show num of pkts enqueued to the ring of queue
    }
  }
}

static
void testq_drop_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
  TQCAST ( drop );

  // enqueue packets
  testq_drop_enqueue_burst ( testq, pkts, num_pkts );

  // if there is at least burst packets in the queue, transmit packets
  if ( likely ( lolliq_count ( &testq->queue ) >= testq->qelem.min_send_burst ) )
  {
    testq_drop_tx_flush ( qelem );
  }
}

static
void testq_drop_free ( qelem_t* qelem )
{
  rte_free ( qelem );
};

//-----------------------------------------------------------------------------------------------------------
// trace log
//-----------------------------------------------------------------------------------------------------------

#ifdef WITH_TRACES
static
void* testq_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
  return trace_event_queue_add ( "testq", common_params->trace_queue_size, sizeof ( testq_event_t ), testq_event_log);
}
#endif

//--------------------------------------------------------------------------------------------------------

qelem_factory_t testq_drop_factory =
{
  .qelem_alloc = testq_drop_alloc,
#ifdef WITH_TRACES
  .trace_queue_alloc = testq_trace_queue_alloc,
#endif
};

static qelem_ops_t testq_drop_ops =
{
  .qelem_tx_burst = testq_drop_tx_burst,
  .qelem_tx_flush = testq_drop_tx_flush,
  .qelem_free = testq_drop_free,
};

