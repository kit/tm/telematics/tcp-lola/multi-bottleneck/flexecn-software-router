#ifndef	__IP4_REWRITE__
#define	__IP4_REWRITE__

#include "../rte_include.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

/**
 * rewrite IP address and update tcp/udp checksum
 *
 * see https://tools.ietf.org/html/rfc1141 for incremental checksum updates
 *
 * src1 - dst1 --> src2 - dst2
 */

static const unsigned match_src_ip = IPv4 ( 1,0,168,192 );
static const unsigned match_dst_ip = IPv4 ( 2,0,168,192 );
static const unsigned rewrite_src_ip = IPv4 ( 1,1,168,192 );
static const unsigned rewrite_dst_ip = IPv4 ( 2,1,168,192 );

static const unsigned checksum_diff;

static inline uint16_t incr_check_s ( uint16_t old_check, uint16_t old, uint16_t new )
{
    /* see RFC's 1624, 1141 and 1071 for incremental checksum updates
    */
    uint32_t l;
    old_check = ~ntohs ( old_check );
    old = ~old;
    l = ( uint32_t ) old_check + ntohs ( old ) + ntohs ( new );
    return htons ( ~ ( ( uint16_t ) ( l>>16 ) + ( l&0xffff ) ) );
}

static void ip4_rewrite ( struct rte_mbuf* pkt )
{

    const uint16_t swapped_ETHER_TYPE_IPv4 = rte_constant_bswap16 ( ETHER_TYPE_IPv4 );

    // get eth header and check that the packet is IP

    struct ether_hdr* eth_hdr = rte_pktmbuf_mtod ( pkt, struct ether_hdr* );
    if ( eth_hdr->ether_type != swapped_ETHER_TYPE_IPv4 ) {
        return;
    }

    struct ipv4_hdr* ip_hdr = ( void* ) &eth_hdr[1];

    if ( ( ip_hdr->src_addr == match_src_ip ) && ( ip_hdr->dst_addr == match_dst_ip ) ) {

//         LOG_LINE();

        ip_hdr->src_addr = rewrite_src_ip;
        ip_hdr->dst_addr = rewrite_dst_ip;

        ip_hdr->hdr_checksum = incr_check_s ( ip_hdr->hdr_checksum, ( uint16_t ) ( match_src_ip>>16 ), ( uint16_t ) ( rewrite_src_ip>>16 ) );
        ip_hdr->hdr_checksum = incr_check_s ( ip_hdr->hdr_checksum, ( uint16_t ) ( match_dst_ip>>16 ), ( uint16_t ) ( rewrite_dst_ip>>16 ) );

        switch ( ip_hdr->next_proto_id ) {
        case IPPROTO_TCP: {
            struct tcp_hdr* tcp_hdr = ( struct tcp_hdr* ) &ip_hdr[1];
            tcp_hdr->cksum=incr_check_s ( tcp_hdr->cksum, ( uint16_t ) ( match_src_ip>>16 ), ( uint16_t ) ( rewrite_src_ip>>16 ) );
            tcp_hdr->cksum=incr_check_s ( tcp_hdr->cksum, ( uint16_t ) ( match_dst_ip>>16 ), ( uint16_t ) ( rewrite_dst_ip>>16 ) );
            break;
        }
        case IPPROTO_UDP: {
            struct udp_hdr* udp_hdr = ( struct udp_hdr* ) &ip_hdr[1];
            udp_hdr->dgram_cksum=incr_check_s ( udp_hdr->dgram_cksum, ( uint16_t ) ( match_src_ip>>16 ), ( uint16_t ) ( rewrite_src_ip>>16 ) );
            udp_hdr->dgram_cksum=incr_check_s ( udp_hdr->dgram_cksum, ( uint16_t ) ( match_dst_ip>>16 ), ( uint16_t ) ( rewrite_dst_ip>>16 ) );
        }
        }
    } else if ( ( ip_hdr->src_addr == rewrite_dst_ip ) && ( ip_hdr->dst_addr == rewrite_src_ip ) ) {

        ip_hdr->src_addr = match_dst_ip;
        ip_hdr->dst_addr = match_src_ip;

        ip_hdr->hdr_checksum = incr_check_s ( ip_hdr->hdr_checksum, ( uint16_t ) ( rewrite_src_ip>>16 ) , ( uint16_t ) ( match_src_ip>>16 ) );
        ip_hdr->hdr_checksum = incr_check_s ( ip_hdr->hdr_checksum, ( uint16_t ) ( rewrite_dst_ip>>16 ), ( uint16_t ) ( match_dst_ip>>16 ) );

        switch ( ip_hdr->next_proto_id ) {
        case IPPROTO_TCP: {
            struct tcp_hdr* tcp_hdr = ( struct tcp_hdr* ) &ip_hdr[1];
            tcp_hdr->cksum=incr_check_s ( tcp_hdr->cksum, ( uint16_t ) ( rewrite_dst_ip>>16 ), ( uint16_t ) ( match_dst_ip>>16 ) );
            tcp_hdr->cksum=incr_check_s ( tcp_hdr->cksum, ( uint16_t ) ( rewrite_src_ip>>16 ), ( uint16_t ) ( match_src_ip>>16 ) );
            break;
        }
        case IPPROTO_UDP: {
            struct udp_hdr* udp_hdr = ( struct udp_hdr* ) &ip_hdr[1];
            udp_hdr->dgram_cksum=incr_check_s ( udp_hdr->dgram_cksum, ( uint16_t ) ( match_dst_ip>>16 ), ( uint16_t ) ( rewrite_dst_ip>>16 ) );
            udp_hdr->dgram_cksum=incr_check_s ( udp_hdr->dgram_cksum, ( uint16_t ) ( match_src_ip>>16 ), ( uint16_t ) ( rewrite_src_ip>>16 ) );
        }
        }
    }

    // set offload flags
//     pkt->l2_len = sizeof(eth_hdr);
//     pkt->l3_len = (ip_hdr->version_ihl & IPV4_HDR_IHL_MASK) * IPV4_IHL_MULTIPLIER;
// //     pkt->l3_len = sizeof(ip_hdr);
//     pkt->ol_flags |= PKT_TX_IPV4 | PKT_TX_IP_CKSUM;



}



#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
