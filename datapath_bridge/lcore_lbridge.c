// #include "../src/lcores.h"
// #include "../src/globals.h"
// #include "../rte_include.h"
// #include "../config/config_lcores.h"
// #include "../config/log_macros.h"
// #include "../datapath/switch_stats.h"
// #include "ip4_rewrite.h"
// 
// #define RCU_MB
// #include <urcu.h>
// #include <urcu/rculfhash.h>
// #include <urcu/compiler.h>
// 
// static const uint8_t bridge_ports[] = {0, 1};
// // static const uint8_t input_2_output[] = {1, 0};
// 
// typedef struct {
//     struct ether_addr addr;
//     int output_port_index;
//     // TODO timestamp
//     struct cds_lfht_node node;
// } node_t;
// 
// static inline
// unsigned send_or_drop_burst ( uint8_t port_id, uint16_t queue_id, struct rte_mbuf** burst, uint16_t burst_count )
// {
//     unsigned nb_tx,pkt_num;
// 
//     nb_tx = rte_eth_tx_burst ( port_id, queue_id, burst, burst_count );
//     for ( pkt_num = nb_tx; pkt_num < burst_count; pkt_num++ ) {
//         rte_pktmbuf_free ( burst[pkt_num] );
//     }
// 
//     return nb_tx;
// }
// 
// static
// int match ( struct cds_lfht_node *ht_node, const void *_key )
// {
//     node_t*node = caa_container_of ( ht_node, node_t, node );
// 
//     const struct ether_addr* key = _key;
// 
//     return memcmp ( key, &node->addr, ETHER_ADDR_LEN ) == 0;
// }
// 
// int lcore_lbridge ( void* arg )
// {
//     LOG_INIT ( "lcore-lbridge started \n" );
// 
//     unsigned port_num;
//     unsigned output_port_num;
//     const unsigned num_ports = 2;
// 
//     unsigned nb_rx,nb_tx,pkt_num;
// 
//     static const unsigned burst_size = 32;
//     struct rte_mbuf* input_burst[burst_size];
// 
//     unsigned output_indices[num_ports];
//     memset ( &output_indices, 0, sizeof ( unsigned ) * num_ports );
//     struct rte_mbuf* output_bursts[num_ports][burst_size];
// 
//     rcu_register_thread();
//     struct cds_lfht* fib_table;
// 
//     LOG_LINE();
// 
//     struct rte_mempool* fib_enrty_pool;
//     fib_enrty_pool = rte_mempool_create ( "fib_pool125",
//                                           1024, sizeof ( node_t ), 0, 0,
//                                           NULL, NULL, NULL, NULL,
//                                           rte_socket_id(), 0 );
// 
//     if ( !fib_enrty_pool ) {
//         rte_panic ( "fib_enrty_pool creation failed \n" );
//     }
// 
//     fib_table = cds_lfht_new ( 1024, 1024, 0, 0, NULL );
//     if ( !fib_table ) {
//         rte_panic ( "Error allocating hash table\n" );
//         return -1;
//     }
// 
// 
//     while ( 1 ) {
// 
//         for ( port_num = 0; port_num < num_ports; port_num++ ) {
// 
//             // receive packets
// 
//             nb_rx = rte_eth_rx_burst ( bridge_ports[port_num], 0, input_burst, burst_size );
// //             LOG_VAR ( nb_rx, "%u" );
// 
//             // lookup packets
// 
//             for ( pkt_num = 0; pkt_num < nb_rx; pkt_num++ ) {
//                 struct rte_mbuf* pkt = input_burst[pkt_num];
//                 struct ether_hdr* eth_hdr = rte_pktmbuf_mtod ( pkt, struct ether_hdr* );
// 
//                 // process source address
//                 if ( is_valid_assigned_ether_addr ( &eth_hdr->s_addr ) ) {
// 
// //                     LOG_LINE();
// //                     LOG_VAR ( port_num, "%u" );
// //                     LOG_VAR ( eth_hdr->s_addr.addr_bytes[5], "%.2x" );
// 
//                     node_t* new_node;
//                     unsigned ret = rte_mempool_get ( fib_enrty_pool, ( void** ) &new_node );
//                     if ( likely ( ret == 0 ) ) {
// 
// //                         LOG_LINE();
// 
//                         uint32_t hash = rte_jhash ( &eth_hdr->s_addr, ETHER_ADDR_LEN  , 0 );
//                         ether_addr_copy ( &eth_hdr->s_addr, &new_node->addr );
//                         new_node->output_port_index = port_num;
// 
//                         rcu_read_lock();
//                         struct cds_lfht_node* ht_node = cds_lfht_add_unique ( fib_table, hash, match, &eth_hdr->s_addr, &new_node->node );
// 
//                         if ( ht_node != &new_node->node ) {
//                             rte_mempool_put ( fib_enrty_pool, ht_node );
// //                             LOG_LINE();
//                             // TODO update time
//                         }
//                         rcu_read_unlock();
//                     }
//                 }
// 
//                 // nat :)
//                 ip4_rewrite ( pkt );
// 
// //                 LOG_VAR ( eth_hdr->s_addr.addr_bytes[5], "%.2x" );
// //                 LOG_VAR ( eth_hdr->d_addr.addr_bytes[5], "%.2x" );
//  
//                 // check if dst is broadcast
//                 if ( is_broadcast_ether_addr ( &eth_hdr->d_addr ) ||  is_multicast_ether_addr ( &eth_hdr->d_addr ) ) {
// 
// //                     LOG_LINE();
// 
//                     for ( output_port_num = 0; output_port_num < num_ports; output_port_num++ ) {
//                         output_bursts[output_port_num][output_indices[output_port_num]++] = pkt;
//                     }
//                     rte_mbuf_refcnt_update ( pkt, num_ports-1 );
// 
//                 } else  {
// 
// //                     LOG_LINE();
// 
//                     // find packet
//                     uint32_t hash = rte_jhash ( &eth_hdr->d_addr, ETHER_ADDR_LEN  , 0 );
//                     struct cds_lfht_iter iter;
//                     int op = -1;
// 
//                     rcu_read_lock();
//                     cds_lfht_lookup ( fib_table, hash, match, &eth_hdr->d_addr , &iter );
//                     struct cds_lfht_node* ht_node = cds_lfht_iter_get_node ( &iter );
//                     if ( ht_node == 0 ) {
// //                         LOG_LINE();
//                     } else {
// //                         LOG_LINE();
// 
//                         node_t*node = caa_container_of ( ht_node, node_t, node );
//                         op = node->output_port_index;
//                     }
//                     rcu_read_unlock();
// 
//                     if ( op>=0 ) {
// //                         LOG_VAR ( op, "%u" );
// //                         LOG_VAR ( output_indices[op], "%u" );
//                         output_bursts[op][output_indices[op]++] = pkt;
//                     } else {
//                         for ( output_port_num = 0; output_port_num < num_ports; output_port_num++ ) {
//                             output_bursts[output_port_num][output_indices[output_port_num]++] = pkt;
//                         }
//                         rte_mbuf_refcnt_update ( pkt, num_ports-1 );
//                     }
//                 }
//             }
// 
//             // send/enqueue packets
// 
//             for ( output_port_num = 0; output_port_num < num_ports; output_port_num++ ) {
//                 unsigned num_pkts_in_burst = output_indices[output_port_num];
// 
// 
// // 		LOG_VAR(output_port_num, "%u");
// // 		LOG_VAR(num_pkts_in_burst, "%u");
// //                 if ( !num_pkts_in_burst ) {
// //                     continue;
// //                 }
// 
//                 if ( !use_qelems ) {
// //                     unsigned out_port =  bridge_ports[output_port_num];
//                     qelem_t* qelem = instance.qelems[output_port_num];
//                     if ( num_pkts_in_burst ) {
//                         nb_tx = qelem_enqueue_burst ( qelem, output_bursts[output_port_num], num_pkts_in_burst/*, 0 */);
//                         output_indices[output_port_num] = 0;
//                     } else {
//                         nb_tx = 0;
//                     }
//                     qelem_xmit ( qelem, 0 );
//                 } else {
//                     if ( num_pkts_in_burst ) {
//                         nb_tx = send_or_drop_burst ( bridge_ports[output_port_num], 0, output_bursts[output_port_num], num_pkts_in_burst );
//                         output_indices[output_port_num] = 0;
//                     } else {
//                         nb_tx = 0;
//                     }
//                 }
// 
//                 stats.rxlb_stats[0].rx_pkts[port_num] += nb_rx;
//                 stats.tx_stats[0].tx_pkts[output_port_num] += nb_tx;
// 
//             }
//         }
//     }
// 
//     return 0;
// }
