#ifndef	__CONFIG_BRIDGE__
#define	__CONFIG_BRIDGE__

#error "this file is deprecated"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */


#define USE_QELEMS 0
#define SEPARATE_TX 0

// bridge modes
enum bridge_mode {
    BRIDGE_PORT_MAP, BRIDGE_FIXED_MAC, BRDIGE_MAC_LEARNING
};

//------------------------------------------------------------------------
// common configuration
//------------------------------------------------------------------------

static const unsigned burst_size = 32;

// what ports will be read
// static const uint8_t num_bridge_input_ports = 5;
// static const uint8_t bridge_input_ports[] = {0, 1, 2, 3, 4};

// what ports will be flushed
// static const uint8_t num_bridge_output_ports = 5;
// static const uint8_t bridge_output_ports[] = {0, 1, 2, 3, 4};
// static const uint8_t num_bridge_output_ports = 1;
// static const uint8_t bridge_output_ports[] = {3};


// latapp1
static const uint8_t num_bridge_input_ports = 3;
static const uint8_t bridge_input_ports[] = {0, 1, 2, 3, 4};
static const uint8_t num_bridge_output_ports = 1;
static const uint8_t bridge_output_ports[] = {3, 3, 2, 3, 4};

//------------------------------------------------------------------------
// bridge port map configuration
//------------------------------------------------------------------------

static const uint8_t bridge_port_map[] = {3, 3, 3, 3};

//------------------------------------------------------------------------
// bridge fixed mac configuration
//------------------------------------------------------------------------

// for srv0 - srv1 - tb10gbe1

// srv0: 68:05:ca:2e:5f:70 68:05:ca:2e:5f:71 68:05:ca:2e:5f:72
// srv2: 68:05:ca:36:f1:e0 68:05:ca:36:f1:e1 68:05:ca:36:f1:e2
// srv3: 68:05:ca:39:a5:7c
// srv3 -eno2 : 0c:c4:7a:33:27:fb
// tb10gbe1: 90:e2:ba:72:6b:a0

// static const unsigned mac_table_size = 4;
// static const struct ether_addr mac_addrs[] = {
//     { .addr_bytes = { 0x68, 0x05, 0xca, 0x39, 0xa5, 0x7c}},
//     { .addr_bytes = { 0x68, 0x05, 0xca, 0x36, 0xf1, 0xe1}},
//     { .addr_bytes = { 0x68, 0x05, 0xca, 0x36, 0xf1, 0xe2}},
//     { .addr_bytes = { 0x68, 0x05, 0xca, 0x36, 0xf1, 0xe3}},
// };
// 
// static const uint8_t mac_addr_output_ports[] = {0, 1, 2, 3};

// WITH IGB 
static const unsigned mac_table_size = 8;
static const struct ether_addr mac_addrs[] = {
    { .addr_bytes = { 0x68, 0x05, 0xca, 0x36, 0xf1, 0xe0}},
    { .addr_bytes = { 0x68, 0x05, 0xca, 0x36, 0xf1, 0xe1}},
    { .addr_bytes = { 0x68, 0x05, 0xca, 0x36, 0xf1, 0xe2}},
    { .addr_bytes = { 0x68, 0x05, 0xca, 0x36, 0xf1, 0xe3}},
    { .addr_bytes = { 0x68, 0x05, 0xca, 0x39, 0xa5, 0x7c}},
    { .addr_bytes = { 0x68, 0x05, 0xca, 0x39, 0xa5, 0x7d}},
    { .addr_bytes = { 0x68, 0x05, 0xca, 0x39, 0xa3, 0xa4}},
    { .addr_bytes = { 0x68, 0x05, 0xca, 0x39, 0xa3, 0xa5}},
};

static const uint8_t mac_addr_output_ports[] = {1, 2, 3, 4, 0, 0, 0, 0};


static const enum bridge_mode bridge_mode = BRIDGE_PORT_MAP;
// static const enum bridge_mode bridge_mode = BRIDGE_FIXED_MAC;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CONFIG__PIPELINE__ */

