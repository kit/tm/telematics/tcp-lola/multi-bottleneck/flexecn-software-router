// #include "../rte_include.h"
// #include "config_bridge.h"
// #include "../bridgie/bridgie.h"
// #include "../bridgie/log_macros.h"
// #include "../qelems/qdev.h"
// #include "../bridgie/benchmark/benchmark.h"
// 
// 
// static inline
// unsigned send_or_drop_burst ( uint8_t port_id, uint16_t queue_id, struct rte_mbuf** burst, uint16_t burst_count )
// {
//     unsigned nb_tx,pkt_num;
// 
//     nb_tx = rte_eth_tx_burst ( port_id, queue_id, burst, burst_count );
//     for ( pkt_num = nb_tx; pkt_num < burst_count; pkt_num++ ) {
//         rte_pktmbuf_free ( burst[pkt_num] );
//     }
// 
//     return nb_tx;
// }
// 
// //-----------------------------------------------------------------------------------------------------------
// 
// int lcore_bridge_tx ( void* arg )
// {
//     LOG_INIT ( "lcore_bridge_tx started \n" );
// 
//     unsigned port_num;
//     unsigned output_port_num,entry_num;
// 
//     unsigned nb_deq,nb_tx,pkt_num;
// 
//     static const unsigned burst_size = 32;
//     struct rte_mbuf* burst[burst_size];
// 
//     struct rte_ring* rings[num_bridge_output_ports];
//     rings[0] = rte_ring_lookup ( "ring0" );
//     rings[1] = rte_ring_lookup ( "ring1" );
//     rings[2] = rte_ring_lookup ( "ring2" );
//     rings[3] = rte_ring_lookup ( "ring3" );
// 
// 
//     while ( 1 ) {
// 
//          for ( port_num = 0; port_num < num_bridge_output_ports; port_num++ ) {
// 
//             nb_deq = rte_ring_dequeue_burst ( rings[port_num], (void**) burst, burst_size );
//             if(nb_deq == 0) {
//                 stats.tx_stats[1].tx_pkts[0] += 1;
//                 continue;
//             }
//             
//             if ( USE_QELEMS ) {
//                 qdev_tx_burst ( bridge_port_map[port_num], 0, burst, nb_deq );
//             } else {
//                 send_or_drop_burst ( bridge_port_map[port_num], 0, burst, nb_deq );
//             }
// 
//             stats.rxlb_stats[1].rx_pkts	[0] += nb_deq;
//         }
//     }
// 
//     return 0;
// }
