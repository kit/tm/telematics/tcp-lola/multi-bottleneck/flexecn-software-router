#ifndef	CONFIG_ETHDEV
#define	CONFIG_ETHDEV

#include "def.h"

/**
 * @file port configuration for bw100gb testbed
 * 
 * must use the same name CONFIG_ETHDEV in include guard as other ethedev configurations
 */

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

//DPDK STRUCTURES DEFINITIONS ----------------------------------------------------------------------------------------------------  

// port configuration for 10gb port
static const struct rte_eth_conf eth_conf_10g = {
    .rxmode = {
        .max_rx_pkt_len = ETHER_MAX_LEN,
        .split_hdr_size = 0,
        .header_split   = 0, /**< Header Split disabled */
        .hw_ip_checksum = 0, /**< IP checksum offload disabled */
        .hw_vlan_filter = 0, /**< VLAN filtering disabled */
        .jumbo_frame    = 0, /**< Jumbo Frame Support disabled */
        .hw_strip_crc   = 0, /**< CRC stripped by hardware */
    },
    .txmode = {
        .mq_mode = ETH_MQ_TX_NONE,
    },
};

// port configuration for 1gb port
static const struct rte_eth_conf eth_conf_1g = {
//     .link_speed = ETH_LINK_SPEED_100,
    .rxmode = {
        .max_rx_pkt_len = ETHER_MAX_LEN,
        .split_hdr_size = 0,
        .header_split   = 0, /**< Header Split disabled */
        .hw_ip_checksum = 0, /**< IP checksum offload disabled */
        .hw_vlan_filter = 0, /**< VLAN filtering disabled */
        .jumbo_frame    = 0, /**< Jumbo Frame Support disabled */
        .hw_strip_crc   = 0, /**< CRC stripped by hardware */
    },
    .txmode = {
        .mq_mode = ETH_MQ_TX_NONE,
    },
};

static const struct rte_eth_rxconf rx_conf = {
    .rx_thresh = {
        .pthresh = 1,
        .hthresh = 1,
        .wthresh = 1,
    },
//     .rx_free_thresh = 32,
};

static const struct rte_eth_txconf tx_conf_10g_low_lat = {
    .tx_thresh = {
        .pthresh = 8,
        .hthresh = 8,
        .wthresh = 0,
    },
    .tx_rs_thresh  = 8,
    .tx_free_thresh  = 16,
};

static const struct rte_eth_txconf tx_conf_eno = {
    .tx_thresh = {
        .pthresh = 1,
        .hthresh = 0,
        .wthresh = 0,
    },
//     .tx_rs_thresh  = 32,
};

#define NUM_RX_DESCRIPTORS 128
#define NUM_TX_DESCRIPTORS 128 

// ETH PORT CONFIG-----------------------------------------------------------------------------------------------------------

// ethernet ports configuration; to be read in the order of (port_type_t - 1)
static const ethport_conf_t ethport_confs[max_port_types-1] = {
    // port port_type_normal
    {
        .eth_conf = &eth_conf_10g,
        .num_rx_queues = 1,
        .num_tx_queues = 1,
        .num_rx_descriptors = NUM_RX_DESCRIPTORS,
        .rx_conf = NULL,
        .num_tx_descriptors = NUM_TX_DESCRIPTORS,
        .tx_conf=NULL,
    },
    // port_type_aqm
    {
        .eth_conf = &eth_conf_10g, 
        .num_rx_queues = 1,
        .num_tx_queues = 1,
        .num_rx_descriptors = NUM_RX_DESCRIPTORS,
        .rx_conf = NULL,
        .num_tx_descriptors = 64,
        .tx_conf= &tx_conf_10g_low_lat,
    },
    // port_type_lowspeed
    {
        .eth_conf = &eth_conf_1g, 
        .num_rx_queues = 1,
        .num_tx_queues = 1,
        .num_rx_descriptors = NUM_RX_DESCRIPTORS,
        .rx_conf = NULL,
        .num_tx_descriptors = NUM_TX_DESCRIPTORS,
        .tx_conf=NULL, //&tx_conf_eno, 
    },
    // port_type_lowspeed_aqm
    {
        .eth_conf = &eth_conf_1g, 
        .num_rx_queues = 1,
        .num_tx_queues = 1,
        .num_rx_descriptors = NUM_RX_DESCRIPTORS,
        .rx_conf = NULL,
        .num_tx_descriptors = 32,
        .tx_conf=NULL, //&tx_conf_eno, 
    }
};


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
