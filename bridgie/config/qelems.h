#ifndef	__CONFIG__QELEMS__
#define	__CONFIG__QELEMS__

#include "def.h"
#include "../../qelems/qelems.h"

/**
 * @file  
 * configuration settings for each implemented qelem
 *
 * assignment to ports will be done in qdev_*setting*.h 
 */

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

#ifndef MAX_PORTS
#define MAX_PORTS 1
#endif

// noop ------------------------------------------------------------------------

static qelem_noop_params_t qnoop_params = {};

// delay ----------------------------------------------------------------------
// for separate delay switch, half/delay in both directions
static qelem_delay_params_t qdelay_params =
{
    .delay_sec = DELAYx2/2,
    .min_pkt_size = MINPKT,
    .link_speed_bit_s= BANDWIDTH,
};

// for non-delay switch, whole delay in one direction
static qelem_delay_params_t qdelay_params2 =
{
    .delay_sec = DELAYx2,
    .min_pkt_size = MINPKT,
    .link_speed_bit_s= BANDWIDTH,
};

// taildrop ----------------------------------------------------------------------
static const qelem_taildrop_params_t qtailpkt_params = {.pkt_limit = QLEN_PKTS };
static const qelem_taildrop_params_t qtail_bdp_params = {.byte_limit = 1 * BDP };
static const qelem_taildrop_params_t qtail_2bdp_params = {.byte_limit = 2 * BDP };
static const qelem_taildrop_params_t qtail_04bdp_params = {.byte_limit = 0.43 * BDP };
static const qelem_taildrop_params_t qtail_06bdp_params = {.byte_limit = 0.6 * BDP };
static const qelem_taildrop_params_t qtail_005bdp_params = {.byte_limit = 0.05 * BDP };
static const qelem_taildrop_params_t qtail_006bdp_params = {.byte_limit = 0.06 * BDP };

// codel -------------------------------------------------------------------------

static qelem_codel_params_t codel_params_005 =
{
    .qlen_pkts = QLEN_PKTS,
    .interval_sec = RTT,
    .target_sec = 0.05 * RTT,
    .mtu = AVGPKT,
};

static qelem_codel_params_t codel_params_01 =
{
    .qlen_pkts = QLEN_PKTS,
    .interval_sec = RTT,
    .target_sec = 0.1 * RTT,
    .mtu = AVGPKT,
};


// pie -------------------------------------------------------------------------

// static qelem_pie_params_t pie_params =
// {
//     .qlen_pkts = QLEN_PKTS,
//     .ref_delay = 15 * 1e-3, // 20ms
//     .t_update = 16 * 1e-3, // 32ms - linux
//     .alpha = 0.125, .beta = 1.25,
// };
// static qelem_pie_params_t pie_params =
// {
//     .qlen_pkts = QLEN_PKTS,
//     .ref_delay = 1.5*  1e-3, // 20ms
//     .t_update = 2 * 1e-3, // 32ms - linux
//     .alpha = 0.015625, .beta = 1.3046875/*1.46875*/,
// };

// new params for 10G paper

// step 1: reduce Tupd by 10 and update alpha so that formula in paper is the same
//      alpha = alpha/10 = 1/80
//      beta = beta + 9alpha/10 = 1 + 1/4 + 9/160
// step 2: since ref_delay is reduced by one order of magniture, increase alpha and beta by one order of magniture
//      alpha = 1/8
//      beta = 10 + 10/4 + 9/16 = 12.5 + 9/16

static qelem_pie_params_t pie_params =
{
    .qlen_pkts = QLEN_PKTS,
    .ref_delay = 0.05 * RTT , //2.5 *  1e-3, // 20ms
    .t_update = 1.5 * 1e-3, // 1.5ms 
   .alpha = 0.125, .beta = 13.0625,
//     .alpha = 0.0125, .beta = 1.30625,
};

// static qelem_pie_params_t pie_params =
// {
//     .qlen_pkts = 2048,
//     .ref_delay = 20e-6, // 20ms
//     .t_update = 10e-6, // 32ms - linux
//     .alpha = 0.015625, .beta =  1.3046875/*1.46875*/,
// };

// ared -------------------------------------------------------------------------

static qelem_ared_params_t ared_params = {
    .qlen_pkts = QLEN_PKTS,
    .target_delay = 10 * 1e-3, // 10ms
    .link_rate = BANDWIDTH,
    .avg_pkt_size = AVGPKT,
};

// gsp -------------------------------------------------------------------------

static qelem_gsp_params_t gsp_params = {
    .qlen_bytes = BDP,
    .threshold_ratio = 0.1,
    .interval_s = 2 * RTT
};

static qelem_gsp_params_t dgsp_params = {
  .qlen_bytes = BDP,
  .threshold_ratio = 0.05 * RTT,
  .interval_s = 2 * RTT
};

static qelem_gsp_params_t dgsp_params2 = {
  .qlen_bytes = BDP,
  .threshold_ratio = 0.43 * RTT,
  .interval_s = 2 * RTT
};


static qelem_gsp_params_t dgsp_params3 = {
  .qlen_bytes = BDP,
  .threshold_ratio = 0.024 * RTT,
  .interval_s = 2 * RTT
};



// tb -------------------------------------------------------------------------

// static qelem_tb_params_t qtb_params =
// {
//     .rate_bits_s = 100e6,
//     .max_burst = 1514 * 2,
// //   .min_send_burst = 1,
// 
//     .qelem_child_params = &codel_params,
//     .qelem_child_factory = &qelem_codel_factory,
// 
// //   .qelem_child_params = &codel_params,
// //   .qelem_child_factory = &qelem_codel_factory,
// 
// //     .qelem_child_params = &fq_codel_params,
// //     .qelem_child_factory = &qelem_fq_codel_factory,
// 
// //   .qelem_child_params = &pie_params,
// //   .qelem_child_factory = &qelem_pie_factory,
// };
// 
// static const struct qport_conf qtb_conf[] =
// {
//     { .qelem_params = &qtb_params, .qelem_factory = &qelem_tb_factory, },
//     { .qelem_params = &qnoop_params, .qelem_factory = &qelem_noop_factory, },
//     { .qelem_params = &qnoop_params, .qelem_factory = &qelem_noop_factory, },
//     { .qelem_params = &qnoop_params, .qelem_factory = &qelem_noop_factory, },
//     { .qelem_params = &qnoop_params, .qelem_factory = &qelem_noop_factory, },    
// //     { .qelem_params = &qtail_params, .qelem_factory = &qelem_taildrop_factory, },
// //     { .qelem_params = &qdelay_params, .qelem_factory = &qelem_delay_factory, },
// //     { .qelem_params = &qnoop_params, .qelem_factory = &qelem_noop_factory, }
// };



// fq_codel -------------------------------------------------------------------------
// // #include "../qelems/qelem_fq_codel.h"
// #include "../../qelems/qls_flow_tuple.h"
// 
// static qelem_fq_codel_params_t fq_codel_params =
// {
//     .num_flow_queues = 1024,
//     .quantum = 1518,
//     .pkt_hard_limit = 10240,
//     .interval_sec = RTT,
//     .target_sec = 0.1 * RTT,
//     .mtu = AVGPKT,
//     .qls_create = qls_flow_tuple_hash_create,
// };
// 
// static const struct qport_conf fq_codel_conf[] =
// {
// //     { .qelem_params = &qdelay_params, .qelem_factory = &qelem_delay_factory, },
//     { .qelem_params = &fq_codel_params, .qelem_factory = &qelem_fq_codel_factory, },
//     { .qelem_params = &qnoop_params, .qelem_factory = &qelem_noop_factory, },
//     { .qelem_params = &qnoop_params, .qelem_factory = &qelem_noop_factory, },
//     { .qelem_params = &qnoop_params, .qelem_factory = &qelem_noop_factory, },
//     { .qelem_params = &qnoop_params, .qelem_factory = &qelem_noop_factory, },
// };


// selector ---------------------------------------------------------------------

// static const struct qport_conf* qports_conf = qnoop_conf;
// static const struct qport_conf* qports_conf = qdrop_conf;
// static const struct qport_conf* qports_conf = qdelay_conf;
// static const struct qport_conf* qports_conf = qtb_conf;
// static const struct qport_conf* qports_conf = qtail_conf;
// static const struct qport_conf* qports_conf = codel_conf;
// static const struct qport_conf* qports_conf = fq_codel_conf;
// static const struct qport_conf* qports_conf = pie_conf;
// static const struct qport_conf* qports_conf = ared_conf;
// static const struct qport_conf* qports_conf = gsp_conf;
// static const struct qport_conf* qports_conf = dgsp_conf;

// 2599997443


// benchmark qelems ---------------------------------------------------------------------

// #include "../../qelems/testqs/testqs.h"

//# 262144 is buffer size for over 10ms delay (10ms delay: 195312 ; curr buffer: 16777216=16.7Mb

// static testq_params_t testq_params = {
//     .qlen_pkts = 16384/*QLEN_PKTS*/ /*16384*/,
//     .qlen_thresh_pkts = 64,
//     .ewma_weight = 0.125 /*0.125*/,
//     .ecn_p = 0.2,
// };
// 
// static const qelem_taildrop_params_t qtailt_params = {.pkt_limit = 1024 };
// 
// static const struct qport_conf testq_conf[] =
// {
//     { .qelem_params = &testq_params, .qelem_factory = &testq_drainq_factory, },
//     { .qelem_params = &qnoop_params, .qelem_factory = &qelem_noop_factory, },
//     { .qelem_params = &qnoop_params, .qelem_factory = &qelem_noop_factory, },
//     { .qelem_params = &qnoop_params, .qelem_factory = &qelem_noop_factory, },
//     { .qelem_params = &qnoop_params, .qelem_factory = &qelem_noop_factory, },
//   { .qelem_params = &qdelay_params, .qelem_factory = &qelem_delay_factory, },
//     { .qelem_params = &qtailt_params, .qelem_factory = &qelem_taildrop_factory, },
//     { .qelem_params = &qtailt_params, .qelem_factory = &qelem_taildrop_factory, },
//     { .qelem_params = &qtailt_params, .qelem_factory = &qelem_taildrop_factory, },
//     { .qelem_params = &qtailt_params, .qelem_factory = &qelem_taildrop_factory, },

//     { .qelem_params = &testq_params, .qelem_factory = &testq_base_factory, }
//     { .qelem_params = &testq_params, .qelem_factory = &testq_noop_factory, }
//   { .qelem_params = &testq_params, .qelem_factory = &testq_bc_factory, }
//   { .qelem_params = &testq_params, .qelem_factory = &testq_block_factory, }
//   { .qelem_params = &testq_params, .qelem_factory = &testq_linkedq_factory, }
//   { .qelem_params = &testq_params, .qelem_factory = &testq_lqts_factory, }
//   { .qelem_params = &testq_params, .qelem_factory = &testq_tspkt_factory, }
//   { .qelem_params = &testq_params, .qelem_factory = &testq_tsq_factory, }
//   { .qelem_params = &testq_params, .qelem_factory = &testq_drainq_factory, }
//   { .qelem_params = &testq_params, .qelem_factory = &testq_drainnic_factory, }
//   { .qelem_params = &testq_params, .qelem_factory = &testq_drainnic2_factory, }
//   { .qelem_params = &testq_params, .qelem_factory = &testq_ecn_factory, }
//   { .qelem_params = &testq_params, .qelem_factory = &testq_ecn2_factory, }
//   { .qelem_params = &testq_params, .qelem_factory = &testq_drop_factory, }
// };

// static const struct qport_conf* qports_conf = testq_conf;



#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CONFIG__QELEMS__ */
