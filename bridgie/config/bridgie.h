#ifndef	CONFIG_BRIDGIE
#define	CONFIG_BRIDGIE

#include "def.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

//------------------------------------------------------------------------
// ethdev and qelem initialization
//------------------------------------------------------------------------

// these ports will be traversed in init_qports
static const uint8_t bconf_num_avail_eth_ports = 4;
// static const int bconf_eth_ports[] = {port_type_normal, port_type_normal, port_type_normal, port_type_down, port_type_lowspeed_aqm};
static const int bconf_eth_ports[] = {port_type_aqm, port_type_normal, port_type_normal, port_type_normal};
// static const int bconf_eth_ports[] = {port_type_normal, port_type_normal, port_type_normal, port_type_aqm};

// configuration for ports (write one file for each testbed)
#include "ethdev_bw100g.h"

// number of packets in mbuf pool
static const unsigned bconf_pktpoolsize = 2048*8*4*16-1;

// common AQM configuration parameters
#define DELAYx2 (30 * 1e-3)
#define BANDWIDTH (10 * 1e9)
#include "qdev_bw100g.h"

//------------------------------------------------------------------------
// bridgie configuration
//------------------------------------------------------------------------

#define USE_QELEMS 1

//------------------------------------------------------------------------
// common configuration
//------------------------------------------------------------------------

static const unsigned burst_size = 32;

// what ports will be read
static const uint8_t num_bridge_input_ports = 4;
static const uint8_t bridge_input_ports[] = {0, 1, 2, 3};

// what ports will be flushed
static const uint8_t num_bridge_output_ports = 4;
static const uint8_t bridge_output_ports[] = {0, 1, 2, 3};
// static const uint8_t num_bridge_output_ports = 1;
// static const uint8_t bridge_output_ports[] = {3};


// latapp1
// static const uint8_t num_bridge_input_ports = 3;
// static const uint8_t bridge_input_ports[] = {0, 1, 2, 3, 4};
// static const uint8_t num_bridge_output_ports = 1;
// static const uint8_t bridge_output_ports[] = {3, 3, 2, 3, 4};

//------------------------------------------------------------------------
// bridge port map configuration
//------------------------------------------------------------------------

static const uint8_t bridge_port_map[] = {1, 0, 0, 0};

//------------------------------------------------------------------------
// bridge fixed mac configuration
//------------------------------------------------------------------------

// for srv0 - srv1 - tb10gbe1

// srv0: 68:05:ca:2e:5f:70 68:05:ca:2e:5f:71 68:05:ca:2e:5f:72
// srv2: 68:05:ca:36:f1:e0 68:05:ca:36:f1:e1 68:05:ca:36:f1:e2
// srv3: 68:05:ca:39:a5:7c
// srv3 -eno2 : 0c:c4:7a:33:27:fb
// tb10gbe1: 90:e2:ba:72:6b:a0

// static const unsigned mac_table_size = 4;
// static const struct ether_addr mac_addrs[] = {
//     { .addr_bytes = { 0x68, 0x05, 0xca, 0x39, 0xa5, 0x7c}},
//     { .addr_bytes = { 0x68, 0x05, 0xca, 0x36, 0xf1, 0xe1}},
//     { .addr_bytes = { 0x68, 0x05, 0xca, 0x36, 0xf1, 0xe2}},
//     { .addr_bytes = { 0x68, 0x05, 0xca, 0x36, 0xf1, 0xe3}},
// };
//
// static const uint8_t mac_addr_output_ports[] = {0, 1, 2, 3};

// WITH IGB
static const unsigned mac_table_size = 4;
static const struct ether_addr mac_addrs[] = {
    { .addr_bytes = { 0x68, 0x05, 0xca, 0x36, 0xf1, 0xe0}},
    { .addr_bytes = { 0x68, 0x05, 0xca, 0x2e, 0x5f, 0x71}},
    { .addr_bytes = { 0x68, 0x05, 0xca, 0x2e, 0x5f, 0x72}},
    { .addr_bytes = { 0x68, 0x05, 0xca, 0x2e, 0x5f, 0x73}}
    // { .addr_bytes = { 0x00, 0x15, 0x17, 0x1c, 0xfa, 0xc6}}
};

// static const uint8_t mac_addr_output_ports[] = {0, 1, 2, 40, 3, 3, 3, 3};
// index of the output port; port_id = bridge_output_ports[value]
static const uint8_t mac_addr_output_ports[] = {0, 1, 2, 3};


// static const enum bridge_mode bridge_mode = BRIDGE_PORT_MAP;
static const enum bridge_mode bridge_mode = BRIDGE_FIXED_MAC;



#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
