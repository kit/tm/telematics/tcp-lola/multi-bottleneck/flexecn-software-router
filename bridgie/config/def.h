#ifndef	CONFIG_DEF
#define	CONFIG_DEF

/**
 * @file
 * enums and structures defenition for configuration
 */

#include <rte_ethdev.h>
#include "../../qelems/qelems.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */
  
typedef enum {
  port_type_down = 0,
  port_type_normal,
  port_type_aqm,
  port_type_lowspeed,
  port_type_lowspeed_aqm,
  max_port_types,
} port_type_t;  

typedef struct {
    // port config (currently unused)
    const struct rte_eth_conf* eth_conf;
    unsigned num_rx_queues;
    unsigned num_tx_queues;
    // rx-queues-config
    unsigned num_rx_descriptors;
    const struct rte_eth_rxconf* rx_conf;
    // tx-queues-config
    unsigned num_tx_descriptors;
    const struct rte_eth_txconf* tx_conf;
} ethport_conf_t;
  
typedef struct 
{
    unsigned min_send_burst;
    const void* qelem_params;
    qelem_factory_t* qelem_factory;
    
} qport_conf_t;

// bridge modes
enum bridge_mode {
    BRIDGE_PORT_MAP, BRIDGE_FIXED_MAC, //BRDIGE_MAC_LEARNING
};  

#ifdef __cplusplus
}
#endif /* __cplusplus */  

#endif