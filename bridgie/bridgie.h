#ifndef	GLOBALS_H
#define	GLOBALS_H

// #include "datatypes.h"
#include "../rte_include.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */
  
#define MAX_PORTS 4  
#define MAX_RX_PORTS_PER_CORE 4

#define NUM_RXLB_CORES 0
#define NUM_LOOKUP_CORES 1
#define NUM_TX_CORES 1

// max cores - for not to comment out structs below
#define MAX_NUM_RXLB_CORES 8
#define MAX_NUM_LOOKUP_CORES 8
#define MAX_NUM_TX_CORES 8

#define NUM_CORES_TOTAL (NUM_RXLB_CORES + NUM_LOOKUP_CORES + NUM_TX_CORES)

#if NUM_RXLB_CORES == 0
#define NUM_RX_STATS (NUM_LOOKUP_CORES)
#else
#define NUM_RX_STATS NUM_RXLB_CORES
#endif

#if NUM_TX_CORES == 0
#define NUM_TX_STATS (NUM_LOOKUP_CORES)
#else
#define NUM_TX_STATS (NUM_LOOKUP_CORES)
#endif  
  
  
typedef struct __instance
{
//   struct rte_mempool* pkt_pool;
} instance_t;  
  
typedef struct {
    uint64_t rx_pkts[MAX_PORTS];
    uint8_t pad[RTE_CACHE_LINE_SIZE - MAX_PORTS * sizeof ( uint64_t )];
} rxlb_stats_t __rte_cache_aligned;

typedef struct {
    uint64_t incomming_pkts;
    uint64_t incomming_missed_pkts;
    uint64_t lookup_misses;
    uint64_t reinstalled_keys;
    uint64_t outgoing_pkts;
    uint8_t pad[RTE_CACHE_LINE_SIZE - 5 * sizeof ( uint64_t )];
} lookup_stats_t __rte_cache_aligned;

typedef struct {
    uint64_t tx_pkts[MAX_PORTS];
    uint8_t pad[RTE_CACHE_LINE_SIZE - MAX_PORTS * sizeof ( uint64_t )];
} tx_stats_t __rte_cache_aligned;

typedef struct {
    rxlb_stats_t rxlb_stats[NUM_RX_STATS];
    lookup_stats_t lookup_stats[NUM_LOOKUP_CORES];
    tx_stats_t tx_stats[NUM_TX_STATS];
} switch_stats_t __rte_cache_aligned;


extern switch_stats_t stats;  
extern instance_t instance;

int lcore_bridge (void* arg);
int lcore_lbridge (void* arg);
int lcore_mbridge (void* arg);
int lcore_bridge_tx (void* arg);
int lcore_master (void* arg);
int lcore_master_sighandler (void);

// extern struct rte_ring* pipeline_ring[2];

  
#ifdef __cplusplus
}
#endif /* __cplusplus */  

#endif