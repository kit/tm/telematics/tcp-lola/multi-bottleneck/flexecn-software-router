#include "init_qports.h"
#include "../rte_include.h"
#include "config/bridgie.h"
#include "log_macros.h"
#include "../qelems/qdev.h"
#include "../qelems/traces/trace_event_queue.h"
// #include "../datapath_bridge/config_bridge.h"
// #include "config/ethdev.h"
// #include "config/qelems.h"

static void init_pktpool ( void );
static void init_ports ( void );

static struct rte_mempool* pkt_pool;

void init_qports ( void )
{
  // init trace event queue
  trace_event_init();

  // create pkt pool(s)
  init_pktpool();
  
  // init ports
  init_ports();
  
  // TODO: init benchmark if required
}

void init_pktpool ( void )
{
  pkt_pool = rte_mempool_create ( "pkt_pool", bconf_pktpoolsize ,
                                  2048, 0,
                                  sizeof ( struct rte_pktmbuf_pool_private ),
                                  rte_pktmbuf_pool_init, NULL,
                                  rte_pktmbuf_init, NULL,
                                  rte_socket_id(), /*MEMPOOL_F_NO_SPREAD*/ 0 );

  if ( pkt_pool == NULL )
  {
    rte_exit ( EXIT_FAILURE, "cannot init mbuf pool: %d - %s\n", rte_errno, rte_strerror ( rte_errno ) );
  }
}

void init_ports ( void )
{
  int ret;

  // initialize ports in bridge configuration

  unsigned num_avail_ports = rte_eth_dev_count();
  if ( num_avail_ports < bconf_num_avail_eth_ports )
  {
    rte_exit ( EXIT_FAILURE, "not enough ports: available %d, required %d\n", num_avail_ports, bconf_num_avail_eth_ports );
  }

  unsigned port_index;
  for ( port_index = 0; port_index < bconf_num_avail_eth_ports; port_index++ )
  {
    port_type_t port_type = bconf_eth_ports[port_index];
    
    // skip down ports
    if(port_type == port_type_down)
      continue;
    
    
    const ethport_conf_t* ethport_conf = &ethport_confs[port_type-1]; 
    unsigned port_id = port_index;

    if ( port_id >= num_avail_ports )
    {
      rte_exit ( EXIT_FAILURE, "port %d is not available, available ports: 0-%d\n", port_id, num_avail_ports - 1 );
    }

    LOG_INIT ( "initializing port %u... \n", port_id );

    // init port
    
    ret = rte_eth_dev_configure ( port_id, ethport_conf->num_rx_queues, ethport_conf->num_tx_queues, ethport_conf->eth_conf );
    if ( ret < 0 )
    {
      rte_exit ( EXIT_FAILURE, "Cannot configure device: err=%d (%s), port=%u\n", ret, rte_strerror ( -ret ), port_id );
    }

    qdev_port_init ( port_id, ethport_conf->num_tx_queues, rte_eth_dev_socket_id ( port_id ) );

    // init rx queue(s)
    
    unsigned rx_queue;
    for ( rx_queue = 0; rx_queue < ethport_conf->num_rx_queues; rx_queue++ )
    {
      ret = rte_eth_rx_queue_setup ( port_id, rx_queue, ethport_conf->num_rx_descriptors,
                                     rte_eth_dev_socket_id ( port_id ), ethport_conf->rx_conf, 
                                     pkt_pool );
      if ( ret < 0 )
      {
        rte_exit ( EXIT_FAILURE, "rte_eth_rx_queue_setup:err=%d, port=%u\n", ret,  port_id );
      }

    }

    // init tx queue(s)
    
    unsigned tx_queue;
    for ( tx_queue = 0; tx_queue < ethport_conf->num_tx_queues; tx_queue++ )
    {
      const qport_conf_t* qport_conf = &qport_confs[port_type-1];

      qdev_queue_setup ( port_id, tx_queue, ethport_conf->num_tx_descriptors,
			  rte_eth_dev_socket_id ( port_id ), ethport_conf->tx_conf,
			  qport_conf->min_send_burst, qport_conf->qelem_factory, qport_conf->qelem_params
			);
    }

    ret = rte_eth_dev_start ( port_id );
    if ( ret < 0 )
      rte_exit ( EXIT_FAILURE, "rte_eth_dev_start:err=%d, port=%u\n",
                 ret, ( unsigned ) port_id );

    rte_eth_promiscuous_enable ( port_id );
//     rte_eth_allmulticast_enable ( port_id );

//     struct rte_eth_link  link;
//     rte_eth_link_get ( port_id, &link );
//     RTE_LOG ( DEBUG, USER1, "link status: %u, link speed: %u, link duplex: %u\n",  link.link_status, link.link_speed, link.link_duplex );

    struct ether_addr  mac_addr;
    rte_eth_macaddr_get ( port_id, &mac_addr );
    RTE_LOG ( DEBUG, USER1, "port %u mac_addr is %02X:%02X:%02X:%02X:%02X:%02X\n",  port_id,
              mac_addr.addr_bytes[0], mac_addr.addr_bytes[1], mac_addr.addr_bytes[2],
              mac_addr.addr_bytes[3], mac_addr.addr_bytes[4], mac_addr.addr_bytes[5] );
    
    uint16_t mtu;
    rte_eth_dev_get_mtu(port_id, &mtu);
    RTE_LOG ( DEBUG, USER1, "port %u MTU is %u\n", ( unsigned ) port_id , mtu);
    

    RTE_LOG ( DEBUG, USER1, "port %u is up and running\n", ( unsigned ) port_id );

  }
}


