#include "../rte_include.h"
#include "bridgie.h"
#include "config/bridgie.h"
#include "../qelems/qdev.h"
#include "log_macros.h"
#include "benchmark/benchmark.h"

//-----------------------------------------------------------------------------------------------------

static inline
unsigned send_or_drop_burst ( uint8_t port_id, uint16_t queue_id, struct rte_mbuf** burst, uint16_t burst_count )
{
    unsigned nb_tx,pkt_num;

    nb_tx = rte_eth_tx_burst ( port_id, queue_id, burst, burst_count );
    for ( pkt_num = nb_tx; pkt_num < burst_count; pkt_num++ ) {
        rte_pktmbuf_free ( burst[pkt_num] );
    }
 
    return nb_tx;
}

static inline
unsigned enqueue_bulk ( uint8_t port_id, uint16_t queue_id, struct rte_mbuf** burst, uint16_t burst_count )
{
    unsigned nb_tx,pkt_num;

    nb_tx = rte_eth_tx_burst ( port_id, queue_id, burst, burst_count );
    for ( pkt_num = nb_tx; pkt_num < burst_count; pkt_num++ ) {
        rte_pktmbuf_free ( burst[pkt_num] );
    }

    return nb_tx;
}

//-----------------------------------------------------------------------------------------------------
 
int lcore_bridge ( void* arg )
{
    LOG_INIT ( "lcore-port bridge started \n" );

    unsigned port_num;
    unsigned num_rx;
    struct rte_mbuf* burst[burst_size];

    int iter;
    const int flush_iter = 128-1;

    BENCHMARK_DECLARE_VARS;

    for ( iter = 0; ; iter++ ) {

        for ( port_num = 0; port_num < num_bridge_input_ports; port_num++ ) {

            num_rx = rte_eth_rx_burst ( bridge_input_ports[port_num], 0, burst, burst_size );

            if(num_rx == 0) {
                stats.tx_stats[0].tx_pkts[bridge_port_map[port_num]] += 1;
//                 stats.tx_stats[0].tx_pkts[bridge_port_map[port_num]] += burst_size;
                continue;
            }
//             stats.tx_stats[0].tx_pkts[bridge_port_map[port_num]] += (burst_size-num_rx);

            // rewrite IP
//             unsigned pkt_num;
//             for ( pkt_num = 0; pkt_num < nb_rx; pkt_num++ ) {
//                 ip4_rewrite ( burst[pkt_num] );
//             }
     
	    if ( USE_QELEMS ) {
                qdev_tx_burst ( bridge_port_map[port_num], 0, burst, num_rx );
            } else {
                send_or_drop_burst ( bridge_port_map[port_num], 0, burst, num_rx );
            }

            stats.rxlb_stats[0].rx_pkts[0] += num_rx;
        }


        if ( USE_QELEMS && ( (iter & flush_iter) == 0 )) {
            for ( port_num = 0; port_num < num_bridge_output_ports; port_num++ ) {
                qdev_tx_flush ( bridge_output_ports[port_num], 0 );
            }
        }
        BENCHMARK_MP ( 1, stats.rxlb_stats[0].rx_pkts[0], 1 );
    }

    return 0;
}
