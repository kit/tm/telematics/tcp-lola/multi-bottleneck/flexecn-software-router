// #include "../src/lcores.h"
#include "../bridgie/bridgie.h"
#include "../rte_include.h"
#include "config/bridgie.h"
#include "log_macros.h"
#include "../qelems/qdev.h"
// #include "ip4_rewrite.h"

static inline
unsigned send_or_drop_burst ( uint8_t port_id, uint16_t queue_id, struct rte_mbuf** burst, uint16_t burst_count )
{
    unsigned nb_tx;

    nb_tx = rte_eth_tx_burst ( port_id, queue_id, burst, burst_count );

//     LOG_VAR(burst[0]->data_len, "%u");
    if(unlikely(nb_tx != burst_count)) {
	
	unsigned pkt_num;
        for ( pkt_num = nb_tx; pkt_num < burst_count; pkt_num++ ) {
            rte_pktmbuf_free ( burst[pkt_num] );
        }
    }

    return nb_tx;
}

static inline
unsigned enqueue_or_drop_burst ( struct rte_ring* ring, void* const* burst, unsigned nb_elements )
{
    unsigned nb_enc,pkt_num;

    nb_enc = rte_ring_enqueue_burst ( ring,  burst, nb_elements );
    for ( pkt_num = 0; pkt_num < nb_enc; pkt_num++ ) {
        rte_pktmbuf_free ( burst[pkt_num] );
    }

    return nb_enc;
}

static inline
unsigned enqueue_bulk ( struct rte_ring* ring, void* const* burst, unsigned nb_elements )
{
    while ( rte_ring_enqueue_bulk ( ring,  burst, nb_elements ) != 0 );

    return nb_elements;
}

//-----------------------------------------------------------------------------------------------------------

int lcore_mbridge ( void* arg )
{
    LOG_INIT ( "lcore-mbridge started on lcore %u (nr %u) \n", rte_lcore_id(), rte_lcore_index(rte_lcore_id()) );

    unsigned port_num;
    unsigned output_port_num,entry_num;

    unsigned nb_rx,nb_tx,pkt_num;

    static const unsigned burst_size = 32;
    struct rte_mbuf* input_burst[burst_size];

    unsigned output_indices[num_bridge_output_ports];
    memset ( &output_indices, 0, sizeof ( unsigned ) * num_bridge_output_ports );
    struct rte_mbuf* output_bursts[num_bridge_output_ports][burst_size];

//     struct rte_ring* rings[num_ports];
//     rings[0] = rte_ring_lookup ( "lookup0-tx0" );
//     rings[1] = rte_ring_lookup ( "lookup0-tx1" );
//
//     if(!rings[0]) rte_panic(" failed to find ring[0] \n");
//     if(!rings[1]) rte_panic(" failed to find ring[1] \n");

    int iter;
    const int flush_iter = (64-1);

    for ( iter = 0; ; iter++ ) {

        for ( port_num = 0; port_num < num_bridge_input_ports; port_num++ ) {

            // receive packets

            nb_rx = rte_eth_rx_burst ( bridge_input_ports[port_num], 0, input_burst, burst_size );
// 	    LOG_VAR(nb_rx, "%u");

            if(nb_rx == 0) {
//                 stats.tx_stats[0].tx_pkts[bridge_port_map[port_num]] += 1;
                continue;
            }

// 	    LOG_VAR ( nb_rx, "%u" );:

            // lookup packets
            for ( pkt_num = 0; pkt_num < nb_rx; pkt_num++ ) {
                struct rte_mbuf* pkt = input_burst[pkt_num];
                struct ether_hdr* eth_hdr = rte_pktmbuf_mtod ( pkt, struct ether_hdr* );

                // nat :)
//                 ip4_rewrite ( pkt );

//                 LOG_VAR ( eth_hdr->s_addr.addr_bytes[5], "%.2x" );
//                 LOG_VAR ( eth_hdr->d_addr.addr_bytes[5], "%.2x" );

                // check if dst is broadcast
                if ( is_broadcast_ether_addr ( &eth_hdr->d_addr ) ||  is_multicast_ether_addr ( &eth_hdr->d_addr ) ) {
			rte_pktmbuf_free(pkt);
//                     for ( output_port_num = 0; output_port_num < num_bridge_output_ports; output_port_num++ ) {
//                         output_bursts[output_port_num][output_indices[output_port_num]++] = pkt;
//                     }
//                     rte_mbuf_refcnt_update ( pkt, num_bridge_output_ports-2 );
                } else  {
                    // search in mac-table
                    int found = -1;

                    for ( entry_num = 0; entry_num < mac_table_size; entry_num++ ) {
//                         LOG_VAR ( mac_addrs[entry_num].addr_bytes[5], "%.2x" );

                        if ( memcmp ( &mac_addrs[entry_num], &eth_hdr->d_addr, ETHER_ADDR_LEN ) == 0 ) {

//                             LOG_LINE();

                            uint8_t output_port_num = mac_addr_output_ports[entry_num];
                            output_bursts[output_port_num][output_indices[output_port_num]++] = pkt;
                            found = 1;
// 			    LOG_VAR(pkt->data_len, "%u");
//                             LOG_LINE();
                            break;
                        }
                    }

                    if ( found < 0 ) {

                        LOG_LINE();
			rte_pktmbuf_free(pkt);

//                         for ( output_port_num = 0; output_port_num < num_bridge_output_ports; output_port_num++ ) {
//                             // do not send packet on incomming interface
//                             if(bridge_output_ports[output_port_num] == bridge_input_ports[port_num]) {
//                                 continue;
//                             }
//                             output_bursts[output_port_num][output_indices[output_port_num]++] = pkt;
//                         }
//                         rte_mbuf_refcnt_update ( pkt, num_bridge_output_ports-1 );
                    }
                }
            }

            // send/enqueue packets

            for ( output_port_num = 0; output_port_num < num_bridge_output_ports; output_port_num++ ) {
                unsigned num_pkts_in_burst = output_indices[output_port_num];
                if(!num_pkts_in_burst) continue;

                if ( USE_QELEMS ) {
                    qdev_tx_burst ( bridge_output_ports[output_port_num], 0, output_bursts[output_port_num], num_pkts_in_burst );
                    output_indices[output_port_num] = 0;
                    nb_tx = num_pkts_in_burst;
// 		    LOG_VAR(bridge_output_ports[output_port_num], "%u");
                } else {
                    nb_tx = send_or_drop_burst ( bridge_output_ports[output_port_num], 0, output_bursts[output_port_num], num_pkts_in_burst );
                    output_indices[output_port_num] = 0;
                    stats.tx_stats[0].tx_pkts[port_num] += nb_tx;
                }
            }

            stats.rxlb_stats[0].rx_pkts[port_num] += nb_rx;
        }

        if ( ( (iter & flush_iter) == 0 ) && USE_QELEMS ) {

// 	  LOG_LINE();

            for ( port_num = 0; port_num < num_bridge_output_ports; port_num++ ) {
                qdev_tx_flush ( bridge_output_ports[port_num], 0 );
            }
        }

    }

    return 0;
}
