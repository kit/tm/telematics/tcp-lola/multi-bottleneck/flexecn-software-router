#include "bridgie.h"
// #include "../src/lcores.h"
#include "init_qports.h"
#include "../bridgie/log_macros.h"
// #include "../datapath_bridge/config_bridge.h"
#include "config/bridgie.h"
#include "benchmark/benchmark.h"
#include "../qelems/traces/trace_event_queue.h"

#include "../qelems/qelems.h"

#include <signal.h>


switch_stats_t stats;
instance_t instance;

static void
handle_signal ( int sig )
{
  lcore_master_sighandler();
  rte_exit ( sig, "exiting after receiving signal %d\n", sig );
}

static void
setup_signal_handlers ( void )
{
  struct sigaction new_action;
  struct sigaction old_action;

  /* create a new action handler */
  new_action.sa_handler = handle_signal;
  sigemptyset ( &new_action.sa_mask );
// 	sigfillset(&new_action.sa_mask);
  new_action.sa_flags = 0;

  /* but don't use if someone previously asked for signal to be ignored */
  sigaction ( SIGINT, NULL, &old_action );
  if ( old_action.sa_handler != SIG_IGN )
    sigaction ( SIGINT, &new_action, NULL );
  sigaction ( SIGHUP, NULL, &old_action );
  if ( old_action.sa_handler != SIG_IGN )
    sigaction ( SIGHUP, &new_action, NULL );
  sigaction ( SIGTERM, NULL, &old_action );
  if ( old_action.sa_handler != SIG_IGN )
    sigaction ( SIGTERM, &new_action, NULL );
}

int main ( int argc, char *argv[] )
{
  int ret;

  // init dpdk
  ret = rte_eal_init ( argc, argv );
  if ( ret < 0 )
  {
    rte_exit ( EXIT_FAILURE, "Invalid EAL arguments\n" );
  }
  argc -= ret;
  argv += ret;
  
  //---------------------------------------------------------------------------------------------

  // TODO: parse command-line apps

  init_qports();
  setup_signal_handlers();
  BENCHMARK_INIT();

  //---------------------------------------------------------------------------------------------

  unsigned lcore_id;

  RTE_LCORE_FOREACH_SLAVE ( lcore_id )
  {
    if ( rte_lcore_index ( lcore_id ) == 1 )
    {
      switch(bridge_mode){
	case BRIDGE_PORT_MAP:
	  rte_eal_remote_launch ( lcore_bridge, 0, lcore_id );
	  break;
	case BRIDGE_FIXED_MAC:
	  rte_eal_remote_launch ( lcore_mbridge, 0, lcore_id );
	  break;
	default:
	  rte_panic("unspecified bridge mode, failed to start...");
	  
      }
//       rte_eal_remote_launch ( lcore_bridge, 0, lcore_id );
//       rte_eal_remote_launch ( lcore_lbridge, 0, lcore_id );
//       rte_eal_remote_launch ( lcore_mbridge, 0, lcore_id );
    }
//     if ( rte_lcore_index ( lcore_id ) == 2 )
//     {
//             rte_eal_remote_launch ( lcore_bridge_tx, 0, lcore_id );
//     }
  }

  // lcoremaster_stats();
  lcore_master ( 0 );

  // join all threads
  rte_eal_mp_wait_lcore();

  return 0;
}
