/*
 *
 */

#include "benchmark.h"
#include "benchmark_header.h"
#include <inttypes.h>

#include <rte_atomic.h>
#include <rte_cycles.h>
#include <rte_malloc.h>
#include <rte_log.h>
#include <rte_common.h>

//------------------------------------------------------------------------------

const char* stat_dump_filename = "stats.dump";  

#define stat_header_format  "#lcore,tsc_val,counter[0],counter[1],...\n"
#define stat_format  "%u,%lu,%lu,%lu\n"

typedef struct __stat_measure_point
{
    unsigned lcore_id;
    uint64_t tsc_val; 
    uint64_t counter1;
    uint64_t counter2;
} measure_point_t;

//-----------------------------------------------------------------------------
static void benchmark_dump(void);

static measure_point_t* points;
static rte_atomic32_t index =   RTE_ATOMIC32_INIT ( 0 );
static rte_atomic32_t dumping = RTE_ATOMIC32_INIT ( 0 );

void benchmark_init ( void )
{
    RTE_LOG (INFO, USER7, "initialized benchmark with %d points of %d counters\n", BJ_NUM_MEASURE_POINTS, 2);

    points = ( measure_point_t* ) rte_calloc ( "bj", BJ_NUM_MEASURE_POINTS, sizeof ( measure_point_t ), 0 );

    if(!points)
        rte_exit(EXIT_FAILURE, "cannot allocate memory for benchmark\n");
}

void benchmark_mp(unsigned lcore_id, uint64_t counter1, uint64_t counter2)
{
    if(index.cnt >= BJ_NUM_MEASURE_POINTS)
    {
        benchmark_dump();
        rte_exit ( EXIT_SUCCESS, "no more stats to collect\n" );
    }

    measure_point_t* mp = points + (index.cnt++);
    mp->lcore_id = lcore_id;
    mp->tsc_val = rte_rdtsc();
    mp->counter1 = counter1;
    mp->counter2 = counter2;
}


void benchmark_mp_atomic ( unsigned int lcore_id, uint64_t counter1, uint64_t counter2 )
{
    int32_t curr = rte_atomic32_add_return ( &index, 1 );
    if ( curr > BJ_NUM_MEASURE_POINTS )
    {
        benchmark_dump();
        rte_exit ( EXIT_SUCCESS, "no more stats to collect\n" );
    }
    measure_point_t* mp = points + (curr - 1);

    mp->lcore_id = lcore_id;
    mp->tsc_val = rte_rdtsc();
    mp->counter1 = counter1;
    mp->counter2 = counter2;
}

static void benchmark_dump(void)
{
    if ( rte_atomic32_test_and_set ( &dumping ) == 0 ) return;

    printf ( "dumping stat file... \n" );
    fflush ( stdout );

    FILE* fd = fopen ( stat_dump_filename, "w" );
    fprintf ( fd, stat_header_format );

    measure_point_t mp;
    int j;
    for ( j = 0; j < BJ_NUM_MEASURE_POINTS; j++ )
    {
        mp = points[j];
        fprintf ( fd, stat_format, mp.lcore_id, mp.tsc_val, mp.counter1, mp.counter1 );
    }

    fflush ( fd );
    fclose ( fd );
}


