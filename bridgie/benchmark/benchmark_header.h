/*
 * benchmarking engine similar to benchmark_journal in BAs
 * (e.g. https://projekte.tm.uni-karlsruhe.de/trac/RELOAD/browser/trunk/include/benchmark/benchmark_journal.h)
 */

#ifndef BJ__BENCHMARK_H
#define BJ__BENCHMARK_H
#include <stddef.h>
#include <stdint.h>

void benchmark_init(void);

void benchmark_mp(unsigned int lcore_id, uint64_t counter1, uint64_t counter2);
void benchmark_mp_atomic(unsigned lcore_id, uint64_t counter1, uint64_t counter2);

#endif // STAT_H

 