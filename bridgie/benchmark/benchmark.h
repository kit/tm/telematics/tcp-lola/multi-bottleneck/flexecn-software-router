#ifndef BJ__BENCHMARK_CONFIG_H
#define BJ__BENCHMARK_CONFIG_H

// #define DO_BENCHMARK 1
#define BJ_NUM_MEASURE_POINTS 4096*2


#ifdef DO_BENCHMARK

#include "benchmark_header.h"

#define MP_EVERY_NUM_PKTS 1024*512
// #define MP_EVERY_NUM_PKTS 1024*32

#define BENCHMARK_INIT() benchmark_init()
#define BENCHMARK_DECLARE_VARS uint64_t last_mp = 0
#define BENCHMARK_DECLARE_COUNTER(var_name) uint64_t var_name = 0
#define BENCHMARK_UPDATE_COUNTER(var_name, value) var_name += value
#define BENCHMARK_MP(lcore_id,main_counter,second_counter) \
        if (main_counter - last_mp > MP_EVERY_NUM_PKTS ) \
        {					  	 \
            benchmark_mp_atomic ( lcore_id, main_counter, second_counter); \
            last_mp += MP_EVERY_NUM_PKTS; \
        } 

#else

#define BENCHMARK_INIT() 
#define BENCHMARK_DECLARE_VARS
#define BENCHMARK_DECLARE_COUNTER(var_name)
#define BENCHMARK_UPDATE_COUNTER(var_name, value)
#define BENCHMARK_MP(lcore_id,main_counter,second_counter)

#endif




# endif